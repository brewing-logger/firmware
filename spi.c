/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

/* References: ATmega644A manual, Section 18. */

#include <avr/io.h>

#include "config.h"
#include "spi.h"

/**
 * \file
 * \brief SPI bus control.
 *
 * Utility functions to initialise and control the SPI bus. This includes enabling and disabling chip selects on different bus slaves, as well as
 * transmitting bytes from the master (the microcontroller) to the slaves.
 *
 * The system uses a single shared SPI bus, which the microcontroller is always the master of. Chip selection is done using standard GPIO ports on the
 * microcontroller.
 */

/**
 * \brief Initialise SPI bus.
 *
 * Initialise the SPI bus with the microcontroller as the bus master. Set the chip selects of all slaves to initially be deselected.
 */
void spi_init (void)
{
	/* Set the data direction of the SPI port. MOSI, SCK, nSS and the CSs are outputs, everything else is an input. */
	SPI_DDR = (1 << DD_MOSI) | (1 << DD_SCK) | (1 << DD_nSS) | (1 << SPI_CS_SD_CARD) | (1 << SPI_CS_FLASH);

	/* Enable SPI, MSB first, master mode, CPOL 0 (SCK low on idle), CPHA 0 (leading sample/trailing setup), set clock rate fck/4 = 250kHz */
	SPCR = (1 << SPE) | (1 << MSTR);

	/* Set the chip selects to 1 (deselected). (Note that the chip selects are inverted. */
	SPI_PORT |= (1 << SPI_CS_SD_CARD) | (1 << SPI_CS_FLASH);
}

void spi_select (uint8_t chip_select)
{
	/* Select one of the chips and disable the other. */
	if (chip_select == SPI_CS_SD_CARD) {
		SPI_PORT |= (1 << SPI_CS_FLASH);
		SPI_PORT &= ~(1 << SPI_CS_SD_CARD);
	} else {
		SPI_PORT |= (1 << SPI_CS_SD_CARD);
		SPI_PORT &= ~(1 << SPI_CS_FLASH);
	}
}

void spi_deselect (void)
{
	/* Deselect all chips. */
	SPI_PORT |= (1 << SPI_CS_SD_CARD) | (1 << SPI_CS_FLASH);
}

void spi_enable (void)
{
	/* Power up and re-initialise SPI. */
	PRR0 &= ~(1 << PRSPI);
	spi_init ();
}

void spi_disable (void)
{
	/* Disable SPI. */
	SPCR &= ~(1 << SPE);

	/* Power it down. */
	PRR0 |= (1 << PRSPI);
}

uint8_t spi_transmit_byte (uint8_t data)
{
	/* Push the data into the transmit buffer. */
	SPDR = data;

	/* Poll until transmission completes. */
	while (!(SPSR & (1 << SPIF)));

	/* Read the response byte. */
	return SPDR;
}

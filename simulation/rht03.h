/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _RHT03_H
#define _RHT03_H

/**
 * \brief IRQs for the RHT03.
 *
 * Identifiers for the RHT03's IRQs.
 */
enum {
	IRQ_RHT03_DATA = 0,	/**< Bi-directional DATA port. Serial; normally high. */
};

/**
 * \brief States for the RHT03 peripheral.
 *
 * Possible states the state machine in the RHT03 can be in.
 */
typedef enum {
	RHT03_STANDBY,	/**< Idle/Standby state. */
	RHT03_START0,	/**< Received a falling edge on DATA; expecting a rising edge. */
	RHT03_START1,	/**< Received a rising edge on DATA; about to transmit START ACK. */
	RHT03_DATA,	/**< Transmitted START ACK; about to transmit a data bit. */
} rht03_state_t;

/**
 * \brief RHT03 peripheral.
 *
 * Data structure holding the state of a simulated RHT03 humidity/temperature sensor peripheral. Only \c irq, \c rh_int, \c rh_frac, \c t_int and
 * \c t_frac should be accessed by client code.
 */
typedef struct rht03_t {
	avr_irq_t *irq;
	struct avr_t *avr;

	/* "Measured" humidity and temperature values. */
	union {
		struct {
			uint8_t rh_int;
			uint8_t rh_frac;
			uint8_t t_int;
			uint8_t t_frac;
		};
		uint32_t data;
	};
	uint8_t checksum; /* has to be stored in case RH and T change during transmission of a measurement */

	/* Internal state. */
	rht03_state_t state;
	avr_cycle_count_t previous_event;
	uint8_t data_index : 6;
	uint8_t data_pin_state : 1;
	uint8_t reentrant : 1;
} rht03_t;

/**
 * \brief Initialise RHT03 peripheral.
 *
 * Initialise the passed-in \c rht03_t structure, assuming it's already been allocated. This creates IRQs, which may be accessed using
 * \c rht03_t->irq after this call returns.
 */
void rht03_init (struct avr_t *avr, rht03_t *p);

#endif /* !_RHT03_H */

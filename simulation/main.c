/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Various #defines needed for simavr. */
#define AVR_STACK_WATCH 0
#define __AVR__ 0

#include <avr_ioport.h>
#include <avr_spi.h>
#include <avr_twi.h>
#include <sim_avr.h>
#include <sim_elf.h>
#include <sim_gdb.h>
#include <sim_vcd_file.h>

#include "../config.h"
#include "ds3231.h"
#include "m25p.h"
#include "rht03.h"
#include "sd_card.h"
#include "serial_lcd.h"

/* main() status codes. */
enum {
	STATUS_SUCCESS = 0,
	STATUS_INVALID_FIRMWARE_FILE,
	STATUS_INVALID_MCU,
	STATUS_INVALID_GDB_PORT,
	STATUS_SD_CARD_UNMOUNTABLE,
	STATUS_FLASH_ROM_UNMOUNTABLE,
	STATUS_FORCED_EXIT,
};

static void _usage (const char *program_name)
{
	puts ("Usage:\n");
	printf ("  %s [FIRMWARE ELF FILE] [GDB PORT] [SD CARD IMAGE FILE] [FLASH ROM IMAGE FILE]\n", program_name);
}

static int simulation_state;

static void _sigint_cb (int signal_number)
{
	/* Gracefully exit the simulation loop. */
	if (simulation_state != cpu_Done) {
		puts ("Exiting…\n");
		simulation_state = cpu_Done;
	} else {
		puts ("Forcing exit…\n");
		exit (STATUS_FORCED_EXIT);
	}
}

int main (int argc, char *argv[])
{
	const char *firmware_filename, *sd_card_filename = NULL, *flash_rom_filename = NULL;
	int gdb_port = -1, mount_error;
	elf_firmware_t firmware;
	avr_t *avr = NULL;
	serial_lcd_t serial_lcd;
	ds3231_t ds3231;
	rht03_t rht03;
	sd_card_t sd_card;
	m25p_t m25p;
#ifdef VCD_SPI
	avr_vcd_t vcd_file;
#endif VCD_SPI

	/* Parse arguments. */
	if (argc < 2) {
		_usage (argv[0]);
		return STATUS_INVALID_FIRMWARE_FILE;
	}

	firmware_filename = argv[1];

	if (argc > 2) {
		char *end_ptr;

		gdb_port = strtoul (argv[2], &end_ptr, 10);

		if (*(argv[2]) == '\0' || *end_ptr != '\0') {
			fprintf (stderr, "Invalid gdb port ‘%s’.\n\n", argv[2]);
			_usage (argv[0]);
			return STATUS_INVALID_GDB_PORT;
		}
	}

	if (argc > 3) {
		sd_card_filename = argv[3];
	}

	if (argc > 4) {
		flash_rom_filename = argv[4];
	} else {
		flash_rom_filename = "flash.img";
	}

	/* Read in the firmware. */
	elf_read_firmware (firmware_filename, &firmware);

	/* Set the MCU properties. */
	firmware.frequency = F_CPU;
	strncpy (firmware.mmcu, MMCU, sizeof (firmware.mmcu) / sizeof (*firmware.mmcu));

	/* Initialise the simulator. */
	avr = avr_make_mcu_by_name (firmware.mmcu);
	if (!avr) {
		fprintf (stderr, "MCU ‘%s’ not known.\n", firmware.mmcu);
		exit (STATUS_INVALID_MCU);
	}

	avr_init (avr);
	avr_load_firmware (avr, &firmware);

	/* Set up gdb if requested. */
	if (gdb_port > 0) {
		avr->gdb_port = gdb_port;
		avr_gdb_init (avr);
	}

	/* Initialise the dummy hardware. */
	serial_lcd_init (avr, &serial_lcd);
	avr_connect_irq (avr_io_getirq (avr, AVR_IOCTL_IOPORT_GETIRQ ('D'), 6), serial_lcd.irq + IRQ_SERIAL_LCD_DATA);
	avr_connect_irq (avr_io_getirq (avr, AVR_IOCTL_IOPORT_GETIRQ ('D'), 7), serial_lcd.irq + IRQ_SERIAL_LCD_CLK);

	ds3231_init (avr, &ds3231);
	ds3231_attach (avr, &ds3231, AVR_IOCTL_TWI_GETIRQ (0));

	rht03_init (avr, &rht03);
	avr_connect_irq (avr_io_getirq (avr, AVR_IOCTL_IOPORT_GETIRQ ('D'), 3), rht03.irq + IRQ_RHT03_DATA);
	avr_connect_irq (rht03.irq + IRQ_RHT03_DATA, avr_io_getirq (avr, AVR_IOCTL_IOPORT_GETIRQ ('D'), 3));

	sd_card_init (avr, &sd_card);
	sd_card_attach (avr, &sd_card, AVR_IOCTL_SPI_GETIRQ (0), avr_io_getirq (avr, AVR_IOCTL_IOPORT_GETIRQ ('B'), 4));

	if (sd_card_filename != NULL) {
		mount_error = sd_card_mount_file (avr, &sd_card, sd_card_filename, 16 * 1024 * 1024 /* 16 MiB */);

		if (mount_error != 0) {
			fprintf (stderr, "SD card image ‘%s’ could not be mounted (error %i).\n", sd_card_filename, mount_error);
			exit (STATUS_SD_CARD_UNMOUNTABLE);
		}
	}

	mount_error = m25p_init (avr, &m25p, M25P_16M, flash_rom_filename);

	if (mount_error != 0) {
		fprintf (stderr, "Flash ROM image ‘%s’ could not be mounted (error %i).\n", flash_rom_filename, mount_error);
		exit (STATUS_FLASH_ROM_UNMOUNTABLE);
	}

	m25p_attach (avr, &m25p, AVR_IOCTL_SPI_GETIRQ (0), avr_io_getirq (avr, AVR_IOCTL_IOPORT_GETIRQ ('B'), 3));

	/* Initially raise the SDA input of the I2C bus since it's normally high. */
	avr_raise_irq (avr_io_getirq (avr, AVR_IOCTL_IOPORT_GETIRQ ('C'), 1), 1);

#ifdef VCD_SPI
	avr_vcd_init (avr, "dump.vcd", &vcd_file, 100);
	avr_vcd_add_signal (&vcd_file, avr_io_getirq (avr, AVR_IOCTL_IOPORT_GETIRQ ('B'), 3), 1, "PB3");
	avr_vcd_add_signal (&vcd_file, avr_io_getirq (avr, AVR_IOCTL_IOPORT_GETIRQ ('B'), 4), 1, "PB4");
	avr_vcd_add_signal (&vcd_file, avr_io_getirq (avr, AVR_IOCTL_SPI_GETIRQ (0), SPI_IRQ_INPUT), 1, "MISO");
	avr_vcd_add_signal (&vcd_file, avr_io_getirq (avr, AVR_IOCTL_SPI_GETIRQ (0), SPI_IRQ_OUTPUT), 1, "MOSI");
	avr_vcd_add_signal (&vcd_file, sd_card.irq + IRQ_SD_CARD_MISO, 1, "SDC.MISO");
	avr_vcd_add_signal (&vcd_file, sd_card.irq + IRQ_SD_CARD_MOSI, 1, "SDC.MOSI");
	avr_vcd_add_signal (&vcd_file, sd_card.irq + IRQ_SD_CARD_nSS, 1, "SDC.nSS");
	avr_vcd_add_signal (&vcd_file, m25p.irq + IRQ_M25P_MISO, 1, "M25P.MISO");
	avr_vcd_add_signal (&vcd_file, m25p.irq + IRQ_M25P_MOSI, 1, "M25P.MOSI");
	avr_vcd_add_signal (&vcd_file, m25p.irq + IRQ_M25P_nSS, 1, "M25P.nSS");
#endif

	/* Set up a signal handler. */
	signal (SIGINT, _sigint_cb);

	/* Run the simulation. */
#ifdef VCD_SPI
	avr_vcd_start (&vcd_file);
#endif

	simulation_state = cpu_Running;
	while (simulation_state != cpu_Done && simulation_state != cpu_Crashed) {
		simulation_state = avr_run (avr);
	}

#ifdef VCD_SPI
	avr_vcd_stop (&vcd_file);
	avr_vcd_close (&vcd_file);
#endif

	m25p_deinit (avr, &m25p);
	sd_card_unmount_file (avr, &sd_card);
	avr_terminate (avr);

	return (simulation_state == cpu_Done) ? 0 : 1;
}

/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _DS3231_H
#define _DS3231_H

/**
 * \brief DS3231 register addresses.
 *
 * The addresses of all the registers supported by the DS3231.
 *
 * Reference: DS3231 data sheet, Figure 1.
 */
typedef enum {
	DS3231_SECONDS = 0x00,
	DS3231_MINUTES = 0x01,
	DS3231_HOURS = 0x02,
	DS3231_DAY = 0x03,
	DS3231_DATE = 0x04,
	DS3231_MONTH = 0x05,
	DS3231_YEAR = 0x06,
	DS3231_ALARM1_SECONDS = 0x07,
	DS3231_ALARM1_MINUTES = 0x08,
	DS3231_ALARM1_HOURS = 0x09,
	DS3231_ALARM1_DAY = 0x0A,
	DS3231_ALARM2_MINUTES = 0x0B,
	DS3231_ALARM2_HOURS = 0x0C,
	DS3231_ALARM2_DAY = 0x0D,
	DS3231_CONTROL = 0x0E,
	DS3231_CONTROL_STATUS = 0x0F,
	DS3231_AGING_OFFSET = 0x10,
	DS3231_TEMP_MSB = 0x11,
	DS3231_TEMP_LSB = 0x12,
} ds3231_reg_t;

#define DS3231_NUM_REGS (DS3231_TEMP_LSB + 1)

/**
 * \brief RTC peripheral.
 *
 * Data structure holding the state of a simulated real-time clock peripheral. Only \c irq should be accessed by client code.
 */
typedef struct ds3231_t {
	avr_irq_t *irq;
	struct avr_t *avr;

	uint8_t selected; /* currently selected address, or 0 if the slave isn't selected */
	ds3231_reg_t reg_addr; /* currently selected register */
	uint8_t regs[DS3231_NUM_REGS]; /* register data values */
} ds3231_t;

/**
 * \brief Initialise RTC peripheral.
 *
 * Initialise the passed-in \c ds3231_t structure, assuming it's already been allocated. This creates IRQs, which may be accessed using
 * \c ds3231_t->irq after this call returns.
 */
void ds3231_init (struct avr_t *avr, ds3231_t *p);

/**
 * \brief Attach RTC peripheral to I2C bus.
 *
 * Attach the IRQs from the passed-in \c ds3231_t structure to the MCU's I2C bus present at the given \c i2c_irq_base. This will typically be passed
 * as \c AVR_IOCTL_TWI_GETIRQ(0).
 */
void ds3231_attach (struct avr_t *avr, ds3231_t *p, uint32_t i2c_irq_base);

#endif /* !_DS3231_H */

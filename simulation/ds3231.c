/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Various #defines needed for simavr. */
#define AVR_STACK_WATCH 0
#define __AVR__ 0

#include <avr_twi.h>
#include <sim_avr.h>

#include "../config.h"
#include "ds3231.h"

/* Various hardware parameters.
 * Reference: DS3231 data sheet. */
#define DS3231_SLAVE_ADDRESS 0xD0

/* Called when the MOSI IRQ is fired. */
static void _ds3231_mosi_cb (struct avr_irq_t *irq, uint32_t value, void *param)
{
	ds3231_t *self = (ds3231_t *) param;
	avr_twi_msg_irq_t v;

	v.u.v = value;

	/* If we receive a STOP, check it was addressed to us, and reset the transaction. */
	if (v.u.twi.msg & TWI_COND_STOP) {
		self->selected = 0;
		self->reg_addr = 0;
	}

	/* If we receive a START, reset our status, check if it's addressed to us and reply with an ACK if so. */
	if (v.u.twi.msg & TWI_COND_START) {
		self->selected = 0;

		if ((v.u.twi.addr & 0xfe) == (DS3231_SLAVE_ADDRESS & 0xfe)) {
			/* We've been selected. */
			self->selected = v.u.twi.addr;
			avr_raise_irq (self->irq + TWI_IRQ_MISO, avr_twi_irq_msg (TWI_COND_ACK, self->selected, 1));
		}
	}

	/* Are we selected for this data transaction (read or write)? */
	if (self->selected) {
		/* For a write transaction, receive and ACK a single address byte. For the moment, values written to registers are ignored. */
		if (v.u.twi.msg & TWI_COND_WRITE) {
			self->reg_addr = v.u.twi.data;
			avr_raise_irq (self->irq + TWI_IRQ_MISO, avr_twi_irq_msg (TWI_COND_ACK, self->selected, 1));
		}

		/* For a read transaction, send the next addressed byte back to the master and increment the register address for sequential reads. */
		if (v.u.twi.msg & TWI_COND_READ) {
			uint8_t data;

			data = self->regs[self->reg_addr++];
			self->reg_addr = (self->reg_addr + 1) % DS3231_NUM_REGS;

			avr_raise_irq (self->irq + TWI_IRQ_MISO, avr_twi_irq_msg (TWI_COND_READ, self->selected, data));
		}
	}
}

static const char *_irq_names[2] = {
	[TWI_IRQ_MISO] = "8>ds3231.out",
	[TWI_IRQ_MOSI] = "32<ds3231.in",
};

void ds3231_init (struct avr_t *avr, ds3231_t *p)
{
	memset (p, 0, sizeof (*p));
	p->avr = avr;

	/* Allocate and connect to IRQs. */
	p->irq = avr_alloc_irq (&avr->irq_pool, 0, 2, _irq_names);
	avr_irq_register_notify (p->irq + TWI_IRQ_MOSI, _ds3231_mosi_cb, p);
}

/* Connect the IRQs of the DS3231 to the TWI master of the microcontroller. */
void ds3231_attach (struct avr_t *avr, ds3231_t *p, uint32_t i2c_irq_base)
{
	avr_connect_irq (p->irq + TWI_IRQ_MISO, avr_io_getirq (avr, i2c_irq_base, TWI_IRQ_MISO));
	avr_connect_irq (avr_io_getirq (avr, i2c_irq_base, TWI_IRQ_MOSI), p->irq + TWI_IRQ_MOSI);

	/* Raise the MISO IRQ because it's normally high. */
	avr_raise_irq (p->irq + TWI_IRQ_MISO, 1);
}

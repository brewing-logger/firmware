/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Various #defines needed for simavr. */
#define AVR_STACK_WATCH 0
#define __AVR__ 0

#include <sim_avr.h>

#include "../config.h"
#include "serial_lcd.h"

/* Called after a complete packet (9 bits) is received. */
static void _serial_lcd_process_packet (struct avr_t *avr, avr_cycle_count_t when, serial_lcd_t *self)
{
	/* TODO: It would be nice to handle these better, perhaps using ncurses. */
	if (self->rs) {
		/* Display data. */
		putc (self->bits, stdout);
	} else if (self->bits == 0x01) {
		/* Home command. */
		putc ('\n', stdout);
	} else if (self->bits == 0x02) {
		/* Clear command. */
		puts ("\n\n");
	} else if (self->bits == 0x18) {
		/* Scroll left command. */
		puts ("Scroll left\n");
	} else if (self->bits == 0x1C) {
		/* Scroll right command. */
		puts ("Scroll right\n");
	} else if (self->bits == 0xC0) {
		/* Row2 command. */
		putc ('\n', stdout);
	} else {
		/* Address command. The row/column are encoded in self->bits as ((row & 0x01) * 0x40) + column + 0x80. */
		/* TODO */
	}

	/* Flush the output. */
	fflush (NULL);
}

/* Called on every rising edge of CLK. */
static avr_cycle_count_t _serial_lcd_process_data_bit (struct avr_t *avr, avr_cycle_count_t when, void *param)
{
	serial_lcd_t *self = (serial_lcd_t *) param;
	uint8_t data_bit;

	data_bit = (self->pin_state & (1 << IRQ_SERIAL_LCD_DATA)) >> IRQ_SERIAL_LCD_DATA;

	switch (self->state) {
		case SERIAL_LCD_IDLE:
			if (data_bit) {
				/* Start of a command. */
				self->state = SERIAL_LCD_RS_BIT;
			}

			break;
		case SERIAL_LCD_RS_BIT:
			self->rs = data_bit;
			self->bits = 0;
			self->state = SERIAL_LCD_D7_BIT;
			break;
		case SERIAL_LCD_D7_BIT:
		case SERIAL_LCD_D6_BIT:
		case SERIAL_LCD_D5_BIT:
		case SERIAL_LCD_D4_BIT:
		case SERIAL_LCD_D3_BIT:
		case SERIAL_LCD_D2_BIT:
		case SERIAL_LCD_D1_BIT:
		case SERIAL_LCD_D0_BIT:
		default:
			/* Process the bits from a command packet. */
			self->bits = (self->bits << 1) | data_bit;

			if (self->state == SERIAL_LCD_D0_BIT) {
				/* At the end of a command packet. */
				self->state = SERIAL_LCD_IDLE;
				_serial_lcd_process_packet (avr, when, self);
			} else {
				/* In the middle of a command packet. */
				self->state++;
			}

			break;
	}

	return 0; /* delta cycle */
}

/* Called when the DATA or CLK IRQs are fired. */
static void _serial_lcd_pin_changed_cb (struct avr_irq_t *irq, uint32_t value, void *param)
{
	serial_lcd_t *self = (serial_lcd_t *) param;
	uint8_t old_pin_state;

	/* Update the pin state. */
	old_pin_state = self->pin_state;
	self->pin_state = (old_pin_state & ~(1 << irq->irq)) | (value << irq->irq);

	/* If this is the rising edge of CLK, process the new DATA bit. */
	if (!(old_pin_state & (1 << IRQ_SERIAL_LCD_CLK)) && (self->pin_state & (1 << IRQ_SERIAL_LCD_CLK))) {
		avr_cycle_timer_register (self->avr, 1, _serial_lcd_process_data_bit, self);
	}
}

static const char *_irq_names[2] = {
	[IRQ_SERIAL_LCD_DATA] = "lcd.data",
	[IRQ_SERIAL_LCD_CLK] = "lcd.clk",
};

void serial_lcd_init (struct avr_t *avr, serial_lcd_t *p)
{
	memset (p, 0, sizeof (*p));
	p->avr = avr;
	p->state = SERIAL_LCD_IDLE;

	/* Allocate and connect to IRQs. */
	p->irq = avr_alloc_irq (&avr->irq_pool, 0, 2, _irq_names);
	avr_irq_register_notify (p->irq + IRQ_SERIAL_LCD_DATA, _serial_lcd_pin_changed_cb, p);
	avr_irq_register_notify (p->irq + IRQ_SERIAL_LCD_CLK, _serial_lcd_pin_changed_cb, p);
}

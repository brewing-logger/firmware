/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _LCD_H
#define _LCD_H

/**
 * \brief IRQs for the serial LCD.
 *
 * Identifiers for the serial LCD's IRQs. Both are inputs.
 */
enum {
	IRQ_SERIAL_LCD_DATA = 0,	/**< DATA port. Serial; normally high. */
	IRQ_SERIAL_LCD_CLK,		/**< CLK port. Triggers on rising edges; normally low. */
};

/**
 * \brief States for the serial LCD receiver.
 *
 * Possible states the receiver state machine in the serial LCD can be in.
 */
typedef enum {
	SERIAL_LCD_IDLE,	/**< Idle state. */
	SERIAL_LCD_RS_BIT,	/**< Received a start bit (falling edge on DATA); expecting the RS bit. */
	SERIAL_LCD_D7_BIT,	/**< Received the RS bit; expecting the most significant data bit. */
	SERIAL_LCD_D6_BIT,	/**< Received the most significant data bit; expecting data bit 6. */
	SERIAL_LCD_D5_BIT,	/**< Received data bit 6; expecting data bit 5. */
	SERIAL_LCD_D4_BIT,	/**< Received data bit 5; expecting data bit 4. */
	SERIAL_LCD_D3_BIT,	/**< Received data bit 4; expecting data bit 3. */
	SERIAL_LCD_D2_BIT,	/**< Received data bit 3; expecting data bit 2. */
	SERIAL_LCD_D1_BIT,	/**< Received data bit 2; expecting data bit 1. */
	SERIAL_LCD_D0_BIT,	/**< Received data bit 1; expecting the least significant data bit. */
} serial_lcd_state_t;

/**
 * \brief Serial LCD peripheral.
 *
 * Data structure holding the state of a simulated serial LCD peripheral. Only \c irq should be accessed by client code.
 */
typedef struct serial_lcd_t {
	avr_irq_t *irq;
	struct avr_t *avr;

	uint8_t pin_state;
	serial_lcd_state_t state;
	uint8_t bits;
	uint8_t rs;
} serial_lcd_t;

/**
 * \brief Initialise serial LCD peripheral.
 *
 * Initialise the passed-in \c serial_lcd_t structure, assuming it's already been allocated. This creates IRQs, which may be accessed using
 * \c serial_lcd_t->irq after this call returns.
 */
void serial_lcd_init (struct avr_t *avr, serial_lcd_t *p);

#endif /* !_LCD_H */

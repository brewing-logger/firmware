/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _M25P_H
#define _M25P_H

/**
 * \brief IRQs for the M25P.
 *
 * Identifiers for the M25P's IRQs.
 */
enum {
	IRQ_M25P_MOSI = 0,	/**< Byte input port. Serial; normally low. */
	IRQ_M25P_MISO,		/**< Byte output port. Serial; normally low. */
	IRQ_M25P_nSS,		/**< Slave select port. Input; active low. */
};

/**
 * \brief States for the M25P.
 *
 * Private, internal states used in the SPI state machine of the M25P.
 */
typedef enum {
	M25P_IDLE,
	M25P_READ_INSTRUCTION,
	M25P_WRITE_RESPONSE,
	M25P_PAGE_PROGRAM,
} m25p_state_t;

/**
 * \brief Part numbers for the M25P family.
 *
 * This simulation supports different members of the M25P component family, each with a different capacity. This enumerates the supported variants
 * of the M25P. One should be passed to m25p_init() to select the component to simulate.
 */
typedef enum {
	M25P_16M,	/**< M25P16, 16 Mb capacity. */
} m25p_type_t;

/**
 * \brief Component identification values.
 *
 * A tuple of three values which identify the manufacturer and device. This is the layout of the device's identification register, accessible using
 * the \c RDID instruction.
 *
 * Reference: M25P16 data sheet, Section 6.3.
 */
typedef struct {
	uint8_t manufacturer;
	uint8_t memory_type;
	uint8_t memory_capacity;
} m25p_identification_t;

/**
 * \brief Instruction details structure.
 *
 * Private, opaque structure used to encode instruction details.
 */
typedef struct _m25p_instruction_details_t m25p_instruction_details_t;

/**
 * \brief M25P Flash ROM peripheral.
 *
 * Data structure holding the state of a simulated Flash ROM peripheral, connected over SPI. Only \c irq should be accessed by client code.
 */
typedef struct m25p_t {
	avr_irq_t *irq;
	struct avr_t *avr;

	/* Internal state. */
	m25p_type_t type;
	m25p_state_t state;
	uint8_t selected : 1; /* 1 iff the chip is selected */

	struct {
		const m25p_instruction_details_t *instruction;
		uint8_t buffer[4];
		uint8_t length;
		uint8_t index;
	} request;

	struct {
		uint8_t *base;
		size_t index; /* relative to base */
		size_t length;
	} response; /* responses cycle over a contiguous block of memory, [base, base + length], until the chip is deselected */

	/* Internal registers. */
	uint8_t status; /* status register, reference: M25P16 data sheet, Section 6.4. */
	m25p_identification_t id; /* identification register */

	/* Stored data. */
	uint8_t *data; /* mmap()ed data */
	off_t data_length;
	int data_fd;
} m25p_t;

/**
 * \brief Initialise M25P peripheral.
 *
 * Initialise the passed-in \c m25p_t structure, assuming it's already been allocated. This creates IRQs, which may be accessed using
 * \c m25p_t->irq after this call returns.
 *
 * TODO: More documentation
 */
int m25p_init (struct avr_t *avr, m25p_t *p, m25p_type_t type, const char *filename);

/* TODO */
void m25p_attach (struct avr_t *avr, m25p_t *p, uint32_t spi_irq_base, struct avr_irq_t *nss_irq);

/* TODO */
int m25p_deinit (struct avr_t *avr, m25p_t *p);

#endif /* !_M25P16_H */

/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/file.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

/* Various #defines needed for simavr. */
#define AVR_STACK_WATCH 0
#define __AVR__ 0

#include <avr_spi.h>
#include <sim_avr.h>

#include "../config.h"
#include "m25p.h"

/*
 * TODO:
 *  - Add support for the Write Protect and Hold pins.
 *  - Implement realistic timeouts on operations (t_{PP}, t_{W}, t_{SE}, t_{BE}, etc.). See M25P16 data sheet, Table 17.
 *  - Implement write protection using Write Protect pin and SRWD status field.
 *  - Implement power-down support, including support for the DP and RES instructions.
 *  - Add support for other variants of the M25P than just the M25P16.
 */

/**
 * \brief Instruction codes for the M25P.
 *
 * All instruction codes supported by any member of the M25P family. Some variants of the M25P may not support all instruction codes.
 *
 * Reference: M25P16 data sheet, Table 4.
 */
typedef enum {
	WREN = 0x06,		/**< Write Enable. */
	WRDI = 0x04,		/**< Write Disable. */
	RDID = 0x9f,		/**< Read Identification. */
	RDSR = 0x05,		/**< Read Status Register. */
	WRSR = 0x01,		/**< Write Status Register. */
	READ = 0x03,		/**< Read Data Bytes. */
	FAST_READ = 0x0b,	/**< Read Data Bytes at Higher Speed. */
	PP = 0x02,		/**< Page Program. */
	SE = 0xd8,		/**< Sector Erase. */
	BE = 0xc7,		/**< Bulk Erase. */
	DP = 0xb9,		/**< Deep Power-down. */
	RES = 0xab,		/**< Release from Deep Power-down. */
} m25p_instruction_code_t;

/**
 * \brief Instruction details structure.
 *
 * Encode details about the supported instructions, for use during execution. These details are constant, static data.
 *
 * Reference: M25P16 data sheet, Table 4.
 */
typedef struct _m25p_instruction_details_t {
	m25p_instruction_code_t instruction_code;
	uint8_t num_address_bytes;
	uint8_t num_dummy_bytes;
	enum {
		REQUIRE_NOTHING = 0,
		REQUIRE_WIP_LOW = (1 << 0),
		REQUIRE_WEL_HIGH = (1 << 1),
	} requirements;
} m25p_instruction_details_t;

/* Fields in the status register. Reference: M25P16 data sheet, Section 6.4. */
#define M25P_STATUS_SRWD (1 << 7)
#define M25P_STATUS_BP2 (1 << 4)
#define M25P_STATUS_BP1 (1 << 3)
#define M25P_STATUS_BP0 (1 << 2)
#define M25P_STATUS_WEL (1 << 1)
#define M25P_STATUS_WIP (1 << 0)

#define REQUEST_BUFFER_TO_ADDRESS(b) ((((uint32_t) (b)[0]) << 16) | (((uint32_t) (b)[1]) << 8) | ((uint32_t) (b)[2]))
#define SECTOR_FOR_ADDRESS(a) ((a) & 0x3fff00)
#define PAGE_SIZE 256 /* bytes */
#define SECTOR_SIZE (256 * PAGE_SIZE) /* bytes */
#define MEMORY_SIZE (32 * SECTOR_SIZE) /* bytes */

/* Debug macros. */
#ifdef M25P_DEBUG
#define DEBUG(m, ...) fprintf (stderr, "%lu: m25p: " m "\n", avr->cycle, __VA_ARGS__);
#else
#define DEBUG(m, ...) do{}while(0)
#endif

/* M25P variant details. */
static const struct {
	off_t capacity; /* bytes */
	m25p_identification_t id;
} m25p_variants_table[] = {
	/* Variant,    Capacity,			Identification */
	[M25P_16M] = { 16 * 1024 * 1024 / 8 /* 16Mb */,	{ 0x20, 0x20, 0x15 } },
};

/* Instruction details. Reference: M25P16 data sheet, Table 4. */
static const m25p_instruction_details_t instruction_details_table[] = {
	/* Opcode,	# address,	# dummy,	requirements */
	{ WREN,		0,		0,		REQUIRE_NOTHING },
	{ WRDI,		0,		0,		REQUIRE_NOTHING },
	{ RDID,		0,		0,		REQUIRE_WIP_LOW },
	{ RDSR,		0,		0,		REQUIRE_NOTHING },
	{ WRSR,		0,		0,		REQUIRE_WEL_HIGH },
	{ READ,		3,		0,		REQUIRE_WIP_LOW },
	{ FAST_READ,	3,		1,		REQUIRE_WIP_LOW },
	{ PP,		3,		0,		REQUIRE_WEL_HIGH | REQUIRE_WIP_LOW },
	{ SE,		3,		0,		REQUIRE_WEL_HIGH | REQUIRE_WIP_LOW },
	{ BE,		0,		0,		REQUIRE_WEL_HIGH | REQUIRE_WIP_LOW },
	{ DP,		0,		0,		REQUIRE_WIP_LOW },
	{ RES,		0,		3,		REQUIRE_WIP_LOW },
};

/* Find the details of a given instruction by its instruction code. Return NULL for invalid instruction codes. */
static const m25p_instruction_details_t *_m25p_find_instruction_details (m25p_instruction_code_t instruction_code)
{
	unsigned int i;

	for (i = 0; i < sizeof (instruction_details_table) / sizeof (*instruction_details_table); i++) {
		if (instruction_details_table[i].instruction_code == instruction_code) {
			return &(instruction_details_table[i]);
		}
	}

	return NULL;
}

/* Check whether an address is protected by the Block Protect bits. Reference: M25P16 data sheet, Table 2. */
static uint8_t _m25p_address_is_protected (m25p_t *self, size_t addr)
{
	uint8_t bp;
	size_t num_protected_sectors, lowest_protected_address;

	/* Grab the Block Protect bits. */
	bp = (self->status >> 2) & 0x03;

	/* Work out which sectors are protected. Protection is always in the top half of the address space. */
	num_protected_sectors = (1 << bp) - 1;
	lowest_protected_address = (32 - num_protected_sectors) * SECTOR_SIZE;

	return (addr > lowest_protected_address);
}

/* Process the instruction and arguments currently in the request and emit a response. */
static m25p_state_t _m25p_process_instruction (m25p_t *self)
{
	m25p_state_t next_state = M25P_IDLE;
	struct avr_t *avr = self->avr;

	DEBUG ("Processing instruction code %x.", self->request.instruction->instruction_code);

	/* Check whether the instruction requires WIP or WREN bits to be set/unset. */
	if (self->request.instruction->requirements & REQUIRE_WIP_LOW && self->status & M25P_STATUS_WIP) {
		DEBUG ("Instruction requires WIP to be low, but it was not (status: %08x).", self->status);
		return M25P_IDLE;
	} else if (self->request.instruction->requirements & REQUIRE_WEL_HIGH && !(self->status & M25P_STATUS_WEL)) {
		DEBUG ("Instruction requires WREN to be high, but it was not (status: %08x).", self->status);
		return M25P_IDLE;
	}

	/* Process the instruction. */
	switch (self->request.instruction->instruction_code) {
		case WREN:
			/* Write Enable. Reference: M25P16 data sheet, Section 6.1. */
			next_state = M25P_IDLE;
			self->status |= M25P_STATUS_WEL;
			break;
		case WRDI:
			/* Write Disable. Reference: M25P16 data sheet, Section 6.2. */
			next_state = M25P_IDLE;
			self->status &= ~M25P_STATUS_WEL;
			break;
		case RDID:
			/* Read Identification. Reference: M25P16 data sheet, Section 6.3. */
			next_state = M25P_WRITE_RESPONSE;
			self->response.base = (uint8_t *) &(self->id);
			self->response.index = 0;
			self->response.length = 3;
			break;
		case RDSR:
			/* Read Status Register. Reference: M25P16 data sheet, Section 6.4. */
			next_state = M25P_WRITE_RESPONSE;
			self->response.base = (uint8_t *) &(self->status);
			self->response.index = 0;
			self->response.length = 1;
			break;
		case WRSR:
			/* Write Status Register. Reference: M25P16 data sheet, Section 6.5.
			 * TODO: WRSR shouldn't be executed if CS isn't taken high immediately after the data byte. */
			next_state = M25P_IDLE;

			/* Bits 5 and 6 are always 0. Bits 0 and 1 (WIP and WEL) must not be set by the WRSR instruction. However, by executing the
			 * instruction, the WEL bit is cleared, so we only have to preserve WIP. */
			self->status = (self->request.buffer[0] & 0x9c) | (self->status & 0x01);

			break;
		case READ:
		case FAST_READ:
			/* Read Data Bytes. Reference: M25P16 data sheet, Sections 6.6 and 6.7. */
			next_state = M25P_WRITE_RESPONSE;
			self->response.base = self->data;
			self->response.index = REQUEST_BUFFER_TO_ADDRESS (self->request.buffer);
			self->response.length = self->data_length;
			break;
		case PP: {
			/* Page Program. Reference: M25P16 data sheet, Section 6.8. */
			size_t addr = REQUEST_BUFFER_TO_ADDRESS (self->request.buffer);

			/* Check the block protection bits first. */
			if (_m25p_address_is_protected (self, addr)) {
				/* Bail. */
				next_state = M25P_IDLE;
			} else {
				/* Start the write. */
				next_state = M25P_PAGE_PROGRAM;
				self->response.index = addr;

				/* Set the WIP bit. */
				self->status |= M25P_STATUS_WIP;
			}

			break;
		}
		case SE: {
			/* Sector Erase. Reference: M25P16 data sheet, Section 6.9. */
			size_t addr = SECTOR_FOR_ADDRESS (REQUEST_BUFFER_TO_ADDRESS (self->request.buffer));

			if (_m25p_address_is_protected (self, addr)) {
				/* Bail. */
				next_state = M25P_IDLE;
			} else {
				size_t i;

				/* Start the erase. */
				next_state = M25P_IDLE;
				self->status |= M25P_STATUS_WIP;

				for (i = 0; i < SECTOR_SIZE; i++) {
					self->data[addr + i] = 0xff;
				}

				/* TODO: simulate delay t_{SE} */

				/* Clear the WIP and WEL bits. */
				self->status &= ~M25P_STATUS_WIP & ~M25P_STATUS_WEL;
			}

			break;
		}
		case BE: {
			/* Bulk Erase. Reference: M25P16 data sheet, Section 6.10. */
			if (((self->status >> 2) & 0x03) != 0) {
				/* The Block Protect bits are set to a non-zero value. Bail. */
				next_state = M25P_IDLE;
			} else {
				size_t i;

				/* Start the erase. */
				next_state = M25P_IDLE;
				self->status |= M25P_STATUS_WIP;

				for (i = 0; i < MEMORY_SIZE; i++) {
					self->data[i] = 0xff;
				}

				/* TODO: simulate delay t_{BE} */

				/* Clear the WIP and WEL bits. */
				self->status &= ~M25P_STATUS_WIP & ~M25P_STATUS_WEL;
			}

			break;
		}
		case DP:
			/* Deep Power-down. Reference: M25P16 data sheet, Section 6.11. */
			/* TODO: Not implemented */
			next_state = M25P_IDLE;
			break;
		case RES:
			/* Release from Deep Power-down. Reference: M25P16 data sheet, Section 6.12. */
			/* TODO: Not implemented */
			next_state = M25P_IDLE;
			break;
		default:
			/* Illegal instruction. */
			fprintf (stderr, "%lu: m25p: Unknown M25P instruction ‘%x’.\n", self->avr->cycle, self->request.instruction->instruction_code);
			break;
	}

	return next_state;
}

/* Send a byte on the MISO line. */
static void _m25p_miso_send_byte (m25p_t *self, uint8_t value)
{
	struct avr_t *avr = self->avr;

	avr_raise_irq (self->irq + IRQ_M25P_MISO, value);
	DEBUG ("Transmitted byte %x (in state %u).", value, self->state);
}

/* Called when the nSS IRQ is fired. */
static void _m25p_ss_pin_changed_cb (struct avr_irq_t *irq, uint32_t value, void *param)
{
	m25p_t *self = (m25p_t *) param;
	struct avr_t *avr = self->avr;

	self->selected = (value == 0) ? 1 : 0; /* invert the value, since nSS is active low */
	DEBUG ("Flash ROM selected: %u.", self->selected);

	/* Finish any ongoing read/write operations. */
	if (self->state == M25P_PAGE_PROGRAM) {
		/* Clear the Write Enable Latch and Write In Progress bits. Reference: M25P16 data sheet, Sections 6.2 and 6.8. */
		self->status &= ~M25P_STATUS_WEL & ~M25P_STATUS_WIP;
		/* TODO: Emulate a delay of t_{PP} before clearing WIP. */
	}

	self->state = M25P_IDLE;
}

/* Called when the MOSI IRQ is fired. */
static void _m25p_mosi_pin_changed_cb (struct avr_irq_t *irq, uint32_t value, void *param)
{
	m25p_t *self = (m25p_t *) param;
	struct avr_t *avr = self->avr;

	/* Bail if this chip isn't selected. */
	if (self->selected == 0) {
		return;
	}

	DEBUG ("Received byte %x (in state %u).", value, self->state);

	/* Handle the input. */
	switch (self->state) {
		case M25P_IDLE: {
			/* Received an instruction code byte. Check how many other bytes need to be read in before it can be processed. */
			const m25p_instruction_details_t *instr;

			instr = _m25p_find_instruction_details (value);
			_m25p_miso_send_byte (self, 0xff);

			if (instr == NULL) {
				/* Error. */
				DEBUG ("Invalid instruction code ‘%u’.", value);
			} else {
				/* Start receiving any address/dummy bytes it requires. */
				self->request.instruction = instr;
				self->request.length = instr->num_address_bytes + instr->num_dummy_bytes;
				self->request.index = 0;

				if (self->request.length > 0) {
					self->state = M25P_READ_INSTRUCTION;
				} else {
					self->state = _m25p_process_instruction (self);
				}
			}

			break;
		}
		case M25P_READ_INSTRUCTION:
			/* Read the address/dummy bytes of an instruction. */
			self->request.buffer[self->request.index] = value;
			_m25p_miso_send_byte (self, 0xff);

			if (++self->request.index == self->request.length) {
				/* Now that all input bytes for the instruction have been received, process the instruction. */
				self->state = _m25p_process_instruction (self);
			}

			break;
		case M25P_WRITE_RESPONSE:
			/* Write out any response bytes. Keep looping over them until the chip is deselected. */
			_m25p_miso_send_byte (self, self->response.base[self->response.index]);
			self->response.index = (self->response.index + 1) % self->response.length;
			break;
		case M25P_PAGE_PROGRAM:
			/* Special implementation for the Page Program (PP) instruction. Keep reading in bytes and writing them to the data array
			 * until the chip is deselected.
			 *
			 * Note: This doesn't implement the hardware-constrained buffering (of up to 256B) which real M25Ps do. They only guarantee
			 * to correctly store the most recent 256B written on the MOSI line. However, the data sheet does allow for more bytes to be
			 * stored correctly; we take that to the limit and store all of them correctly.
			 *
			 * Note also that we slightly abuse response.index to hold our current write index. */
			self->data[self->response.index] &= value; /* use bitwise AND here, as bits can only change from 1 → 0 */
			_m25p_miso_send_byte (self, 0xff);

			/* Update the address. We wrap at the top of the current page. */
			self->response.index = (self->response.index & 0x3fff00) | ((self->response.index + 1) & 0xff);

			break;
		default:
			/* Shouldn't be reached. */
			assert (0);
	}
}

static const char *_irq_names[3] = {
	[IRQ_M25P_MOSI] = "8<m25p.mosi",
	[IRQ_M25P_MISO] = "8>m25p.miso",
	[IRQ_M25P_nSS] = "<m25p.nss",
};

static int _m25p_mount_file (m25p_t *self, const char *filename, off_t image_size)
{
	int fd;
	void *mapped;
	int saved_errno;
	struct stat stat_buf;
	uint8_t locked = 0; /* boolean */
	off_t blocknr;
	uint8_t c_size_mult;
	uint16_t mult;
	uint16_t c_size;

	/* Open the specified disk image. */
	fd = open (filename, O_RDWR | O_CREAT | O_CLOEXEC, S_IRUSR | S_IWUSR);

	if (fd == -1) {
		/* Error. */
		return errno;
	}

	/* Lock it for exclusive access. */
	if (flock (fd, LOCK_EX) == -1) {
		/* Error. */
		saved_errno = errno;
		goto error;
	}

	locked = 1;

	/* Check its size. If it's smaller than the requested size, expand it. Otherwise, ignore any excess size. */
	if (fstat (fd, &stat_buf) == -1) {
		/* Error. */
		saved_errno = errno;
		goto error;
	}

	if (stat_buf.st_size < image_size) {
		/* Expand the file. This fills it with 0s. We'll overwrite the new region with 1s after memory mapping it, below. */
		if (ftruncate (fd, image_size) == -1) {
			/* Error. */
			saved_errno = errno;
			goto error;
		}
	}

	/* Map it into memory. */
	mapped = mmap (NULL, image_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

	if (mapped == MAP_FAILED) {
		/* Error. */
		saved_errno = errno;
		goto error;
	}

	/* If we expanded (or created) the file, set the expanded area to all 1s. This is the initial delivery state for the M25P.
	 * Reference: M25P16 data sheet, Section 8. */
	if (stat_buf.st_size < image_size) {
		memset (mapped + stat_buf.st_size, 0xff, image_size - stat_buf.st_size);
	}

	/* Success. */
	self->data = mapped;
	self->data_length = image_size;
	self->data_fd = fd;

	return 0;

error:
	/* Clean up after the error. */
	if (locked == 1) {
		flock (fd, LOCK_UN);
	}

	close (fd);

	return saved_errno;
}

static int _m25p_unmount_file (struct avr_t *avr, m25p_t *self)
{
	if (self->data == NULL) {
		/* No disk mounted. */
		return 0;
	}

	/* Synchronise changes. */
	msync (self->data, self->data_length, MS_SYNC | MS_INVALIDATE);

	/* Unlock the file. */
	flock (self->data_fd, LOCK_UN);

	/* munmap() and close. */
	munmap (self->data, self->data_length);
	close (self->data_fd);

	self->data = NULL;
	self->data_length = 0;
	self->data_fd = -1;

	return 0;
}

/* Get the capacity (in bytes) of a given type of M25P component. */
static off_t _m25p_capacity_for_type (m25p_type_t type)
{
	return m25p_variants_table[type].capacity;
}

int m25p_init (struct avr_t *avr, m25p_t *p, m25p_type_t type, const char *filename)
{
	int mount_status;

	memset (p, 0, sizeof (*p));

	p->avr = avr;
	p->type = type;
	p->state = M25P_IDLE;

	/* Initial state: all status bits low. Reference: M25P16 data sheet, Section 8. */
	p->status = 0;
	p->id = m25p_variants_table[type].id;

	/* Mount the image file. */
	mount_status = _m25p_mount_file (p, filename, _m25p_capacity_for_type (type));

	if (mount_status != 0) {
		return mount_status;
	}

	/* Allocate and connect to IRQs. */
	p->irq = avr_alloc_irq (&avr->irq_pool, 0, 3, _irq_names);
	avr_irq_register_notify (p->irq + IRQ_M25P_MOSI, _m25p_mosi_pin_changed_cb, p);
	avr_irq_register_notify (p->irq + IRQ_M25P_nSS, _m25p_ss_pin_changed_cb, p);

	return 0;
}

/* Tidy up. */
int m25p_deinit (struct avr_t *avr, m25p_t *p)
{
	return _m25p_unmount_file (avr, p);
}

/* Connect the IRQs of the Flash ROM to the SPI bus of the microcontroller. */
void m25p_attach (struct avr_t *avr, m25p_t *p, uint32_t spi_irq_base, struct avr_irq_t *nss_irq)
{
	/* Connect MISO/MOSI. */
	avr_connect_irq (p->irq + IRQ_M25P_MISO, avr_io_getirq (avr, spi_irq_base, SPI_IRQ_INPUT));
	avr_connect_irq (avr_io_getirq (avr, spi_irq_base, SPI_IRQ_OUTPUT), p->irq + IRQ_M25P_MOSI);

	/* Connect the chip select. */
	avr_connect_irq (nss_irq, p->irq + IRQ_M25P_nSS);
}

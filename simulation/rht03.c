/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Various #defines needed for simavr. */
#define AVR_STACK_WATCH 0
#define __AVR__ 0

#include <sim_avr.h>
#include <sim_time.h>

#include "../config.h"
#include "rht03.h"

/*
 * This simulates an RHT03 humidity/temperature sensor. The simulation isn't cycle accurate (i.e. it doesn't pay attention to the delays the actual
 * device requires between bus transitions), and currently doesn't support configuring the data sent back along the bus.
 *
 * Reference: RHT03 data sheet.
 */

/* The RHT03 transmits bits as high/low by modulating the duty cycle of the DATA line. High bits take the DATA line high for 70us; low bits take it
 * high for 27us. */
static avr_cycle_count_t _rht03_calculate_bit_high_time (struct avr_t *avr, uint8_t b)
{
	return b ? avr_usec_to_cycles (avr, 70) : avr_usec_to_cycles (avr, 27);
}

/* Called on every DATA line change, and also as a result of self-initiated timer callbacks. */
static avr_cycle_count_t _rht03_process_data_bit (struct avr_t *avr, avr_cycle_count_t when, void *param)
{
	rht03_t *self = (rht03_t *) param;
	avr_cycle_count_t period = 0, elapsed;
	rht03_state_t next_state = self->state;

	/* Prevent re-entrancy loops. */
	self->reentrant = 1;

	/* Work out how many cycles have passed since we last processed an event. */
	elapsed = when - self->previous_event;

	switch (self->state) {
		case RHT03_STANDBY:
			if (self->data_pin_state == 0) {
				/* Received a falling edge on DATA; this might be a start signal. */
				next_state = RHT03_START0;
				period = 0;
			}

			break;
		case RHT03_START0:
			if (self->data_pin_state == 1) {
				/* Received a rising edge on DATA; this definitely is a start signal. Lower the DATA line after 30us to begin our
				 * START ACK, and hold it there for 80us. */
				if (elapsed == 1) {
					period = avr_usec_to_cycles (avr, 30);
				} else {
					next_state = RHT03_START1;
					avr_raise_irq (self->irq + IRQ_RHT03_DATA, 0);
					period = avr_usec_to_cycles (avr, 80);
				}
			}

			break;
		case RHT03_START1:
			/* Take the DATA line high for 81us (chosen because it's not quite 80us) to finish our START ACK, then start transmitting
			 * data immediately. */
			if (elapsed == avr_usec_to_cycles (avr, 80)) {
				avr_raise_irq (self->irq + IRQ_RHT03_DATA, 1);
				period = avr_usec_to_cycles (avr, 81);
			} else {
				next_state = RHT03_DATA;
				self->data_index = 0;
				period = 1;
			}
			break;
		case RHT03_DATA:
			/* Have we finished yet? If so, pull DATA low to signal that we have, and return to the idle state. */
			if (self->data_index >= 40) {
				avr_raise_irq (self->irq + IRQ_RHT03_DATA, 0);
				next_state = RHT03_STANDBY;
				period = 0;
				break;
			}

			/* Transmit another bit of data. */
			if (elapsed == avr_usec_to_cycles (avr, 50)) {
				/* Just finished transmitting the low segment of the bit. Now transmit the high segment. */
				avr_raise_irq (self->irq + IRQ_RHT03_DATA, 1);

				if (self->data_index < 32) {
					/* Transmit a bit of a measurement, MSB first. */
					period = _rht03_calculate_bit_high_time (avr, (self->data >> (32 - self->data_index)) & 0x01);
				} else if (self->data_index < 40) {
					/* Transmit a bit of the checksum. */
					period = _rht03_calculate_bit_high_time (avr, (self->checksum >> (40 - self->data_index)) & 0x01);
				}

				self->data_index++;
			} else {
				/* Transmit the low segment of the bit for 50us. */
				avr_raise_irq (self->irq + IRQ_RHT03_DATA, 0);
				period = avr_usec_to_cycles (avr, 50);
			}

			break;
		default:
			/* Should never be reached. */
			assert (0);
	}

	/* Prevent re-entrancy loops. */
	self->reentrant = 0;

	self->state = next_state;
	self->previous_event = when;

	/* Re-schedule ourselves after a further period cycles. */
	return (period > 0) ? when + period : 0;
}

/* Called when the DATA IRQ is fired. */
static void _rht03_data_pin_changed_cb (struct avr_irq_t *irq, uint32_t value, void *param)
{
	rht03_t *self = (rht03_t *) param;

	/* Prevent re-entrancy loops. */
	if (self->reentrant == 1) {
		return;
	}

	/* Bail early if nothing's changed. */
	if (self->data_pin_state == value) {
		return;
	}

	self->reentrant = 1;

	/* Update the pin state. */
	self->data_pin_state = value;

	/* Process the changed bit. */
	avr_cycle_timer_register (self->avr, 1, _rht03_process_data_bit, self);

	/* Prevent re-entrancy loops. */
	self->reentrant = 0;
}

static const char *_irq_names[2] = {
	[IRQ_RHT03_DATA] = "=rht03.data",
};

void rht03_init (struct avr_t *avr, rht03_t *p)
{
	memset (p, 0, sizeof (*p));
	p->avr = avr;
	p->state = RHT03_STANDBY;

	/* Allocate and connect to IRQs. */
	p->irq = avr_alloc_irq (&avr->irq_pool, 0, 2, _irq_names);
	avr_irq_register_notify (p->irq + IRQ_RHT03_DATA, _rht03_data_pin_changed_cb, p);
}

/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _ADC_H
#define _ADC_H

#include "common.h"

/**
 * \file
 * \brief ADC control declarations.
 *
 * Declarations for symbols in adc.c.
 */

typedef enum {
	ADC0 = 0,
	ADC1,
	ADC2,
	ADC3,
	ADC4,
	ADC5,
	ADC6,
	ADC7
} AdcInput;

void adc_init (void);
AdcReading adc_take_measurement (unsigned int adc_mux);
void adc_power_up (void);
void adc_power_down (void);

/**
 * \brief Convert voltage to an ADC measurement.
 *
 * This converts a voltage, as would be seen at an ADC input pin, to the quantised binary representation which would be outputted by the ADC code.
 * This simply scales by the reference voltage and the ADC precision. It is assumed that this will be used for constant data, so that floating point
 * computation can be avoided at run time.
 *
 * Reference: ATmega644A manual, Section 23.8.
 */
#define ADC_READING_FROM_VOLTAGE(v) ((AdcReading) (((v) / AVCC) * 1024.0))

#endif /* !_ADC_H */

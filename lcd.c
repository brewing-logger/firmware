/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <avr/io.h>
#include <util/delay.h>

#include "common.h"
#include "lcd.h"

/**
 * \file
 * \brief LCD display output handling.
 *
 * Printing of messages to the LCD display, and control over rendering on the display (e.g. clearing it, moving the text cursor around, etc.).
 */

/**
 * \brief Initialise LCD display.
 *
 * Initialise the LCD display ports. This does not change the content of the display or clear it in any way.
 */
void lcd_init (void)
{
	/* Set up the LCD output ports. The data line is normally high, and the clock is normally low. */
	LCD_DDR |= (1 << LCD_DATA) | (1 << LCD_CLK);
	LCD_PORT = (LCD_PORT | (1 << LCD_DATA)) & ~(1 << LCD_CLK);

	/* Set up the port controlling the transistor which gates the LCD's power. Leave it on initially so that we can show boot messages. */
	LCD_POWER_DDR |= (1 << LCD_POWER_OUT);
	lcd_power_set_state (LCD_POWER_ON);
}

/**
 * \brief Turn LCD power on/off.
 *
 * Turn the power supply for the LCD on or off. This includes a delay to allow the LCD controller to boot. When turning the power supply off, the data
 * and clock lines are taken low so that no parasitic currents flow through them. When turning the power supply on, the data line is taken high as
 * it's normally high when idle.
 */
void lcd_power_set_state (LcdPowerState new_state)
{
	switch (new_state) {
		case LCD_POWER_ON:
			LCD_PORT |= (1 << LCD_DATA);
			LCD_POWER_PORT |= (1 << LCD_POWER_OUT);
			_delay_ms (100); /* allow controller to boot */
			break;
		case LCD_POWER_OFF:
			LCD_POWER_PORT &= ~(1 << LCD_POWER_OUT);
			LCD_PORT &= ~(1 << LCD_DATA) & ~(1 << LCD_CLK);
			break;
		default:
			ASSERT_NOT_REACHED ();
	}
}

/**
 * \brief Get LCD power state.
 *
 * Get whether the LCD and controller are turned on or off.
 */
LcdPowerState lcd_power_get_state (void)
{
	return (LCD_POWER_PORT & (1 << LCD_POWER_OUT)) ? LCD_POWER_ON : LCD_POWER_OFF;
}

/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <avr/io.h>

#include "adc.h"
#include "common.h"
#include "config.h"
#include "gas.h"
#include "humidity.h"

/**
 * \file
 * \brief Alcohol gas concentration sensor input handling.
 *
 * Handling of the alcohol gas concentration sensor, including preheating it, taking measurements and normalising them for ambient temperature and
 * humidity.
 */

/**
 * Throughout this file, the following naming convention is used for variables representing sensor resistance:
 *    rs_[rh]_[temperature]_[gas concentration]
 * Where 'rh' is the relative humidity (in percent), 'temperature' is the temperature (degrees Celsius) and 'g' is the alcohol gas concentration
 * (tenths of a milligram per litre) at the time of measuring the resistance. If any of these quantities are unknown/variable at the time, then 'rh',
 * 't' or 'g' are used instead of integer literals. For example, the variable name rs_65_20_g means the sensor resistance, R_s, was measured at 65%
 * RH, 20 degrees Celsius, for an unknown gas concentration.
 */

/**
 * \brief Fixed point resistance ratio type.
 *
 * A fixed point type used to represent ratios between resistances. Values are stored in hundredths, e.g. 240 represents the ratio 2.40. Ratios from
 * 0.00 to 2.55 can be represented; there are no invalid or illegal values.
 */
typedef uint8_t ResistanceRatio;

/**
 * \brief Fixed point resistance type.
 *
 * A fixed point type used to represent resistances. Values are stored in kiloohms, e.g. 5 represents the resistance 5 kOhm. Resistances from 0 kOhm
 * to 65.535 MOhm can be represented; there are no invalid or illegal values.
 */
typedef uint16_t Resistance;

/**
 * \brief Fixed point division of resistances.
 *
 * Divide \c num by \c denom to give a ResistanceRatio. This ensures that the fixed point type is respected, as ratios are represented in hundredths.
 */
#define RESISTANCES_TO_RATIO(num, denom) (((uint32_t) (num) * 100) / (denom))

/**
 * \brief Fixed point multiplication of a resistance ratio by a resistance.
 *
 * Multiply \c ratio by \c denom to give the numerator Resistance. This ensures that the fixed point type is respected, as ratios are represented in
 * hundredths, and resistances are represented in kiloohms.
 */
#define RESISTANCE_RATIO_MULTIPLY(ratio, denom) (((uint32_t) (ratio) * (denom)) / 100)

/* Reference: MQ-3 data sheet, Figure 3, 'Alcohol' line. Relative humidity: 65%, O_2 concentration: 21%, Temperature: 20 degrees Celsius. */
static const struct {
	ResistanceRatio rs_ro; /* R_s / R_o, stored in hundredths, e.g. '240' = 2.40 */
	GasReading mg_l; /* mg / l, stored in hundreths of a milligram per litre, e.g. '620' = 6.20 mg/l */
} sensitivity_table[] = {
	/* R_s / R_o,	mg / l */
	{ 240,		  10 },
	{ 160,		  22 },
	{ 100,		  41 },
	{  55,		 105 },
	{  28,		 260 },
	{  20,		 410 },
	{  16,		 620 },
	{  14,		 800 },
	{  12,		1000 },
};
#define NUM_SENSITIVITIES (uint8_t) (sizeof (sensitivity_table) / sizeof (*sensitivity_table))

/* Reference: MQ-3 data sheet, Figure 4, both lines, ignoring negative temperatures. */
static const struct {
	ResistanceRatio rs_ro_rh33; /* R_s / R_o at 33% relative humidity, stored in hundredths, e.g. '122' = 1.22 */
	ResistanceRatio rs_ro_rh85; /* R_s / R_o at 85% relative humidity, stored in hundredths, e.g. '98' = 0.98 */
	uint8_t temperature; /* degrees Celsius, e.g. 15 means 15 degrees Celsius; note this is different from TemperatureReading */
} adjustment_table[] = {
	/* R_s / R_o 33,	R_s / R_o 85,	Temp. */
	{ 135,			113,		 0 },
	{ 122,			108,		 5 },
	{ 112,			 98,		13 },
	{ 100,			 89,		20 },
	{  92,			 84,		30 },
	{  89,			 80,		40 },
	{  87,			 78,		50 },
};
#define NUM_ADJUSTMENTS (uint8_t) (sizeof (adjustment_table) / sizeof (*adjustment_table))

void gas_init (void)
{
	/* We assume ADC0 below when setting ADMUX and DIDR0. */
	STATIC_ASSERT (GAS_SENSOR_IN == PA0 /* == ADC0 */);

	/* Set the gas sensor pin as an input. */
	GAS_SENSOR_DDR &= ~(1 << GAS_SENSOR_IN);

	adc_init ();
	DIDR0 |= (1 << ADC0D); /* disable digital input on ADC0 to save power */

	/* Configure the gas sensor heater control port as an output, initially turned off. */
	GAS_SENSOR_HEATER_DDR |= (1 << GAS_SENSOR_HEATER_OUT);
	GAS_SENSOR_HEATER_PORT &= ~(1 << GAS_SENSOR_HEATER_OUT);
}

static void _mq3_interpolate_adjustment_rs_ro (TemperatureReading temperature,
                                               ResistanceRatio *rs_ro_rh33, ResistanceRatio *rs_ro_rh85) NONNULL (2, 3);

static void _mq3_interpolate_adjustment_rs_ro (TemperatureReading temperature, ResistanceRatio *rs_ro_rh33, ResistanceRatio *rs_ro_rh85)
{
	uint8_t i = 0;
	int8_t d_resistance_ratio_rh33, d_resistance_ratio_rh85;
	uint8_t d_temperature;

#define TEMP(t) TEMPERATURE_READING_FROM_DEGREES ((t), 0)

	if (temperature < TEMP (adjustment_table[0].temperature)) {
		*rs_ro_rh33 = adjustment_table[0].rs_ro_rh33;
		*rs_ro_rh85 = adjustment_table[0].rs_ro_rh85;
		return;
	} else if (temperature >= TEMP (adjustment_table[NUM_ADJUSTMENTS - 1].temperature)) {
		*rs_ro_rh33 = adjustment_table[NUM_ADJUSTMENTS - 1].rs_ro_rh33;
		*rs_ro_rh85 = adjustment_table[NUM_ADJUSTMENTS - 1].rs_ro_rh85;
		return;
	}

	/* Handle the typical cases where the temperature lies within the table. */
	for (i = 0; i + 1 < NUM_ADJUSTMENTS; i++) {
		/* Does the temperature fall in the interval between adjustment_table[i] and adjustment_table[i + 1]? If so, linearly interpolate
		 * between the two R_s / R_o values. */
		if (temperature >= TEMP (adjustment_table[i].temperature) &&
		    temperature < TEMP (adjustment_table[i + 1].temperature)) {
			break;
		}
	}

	/* We should still be inside the table, and various invariants should hold. */
	DYNAMIC_ASSERT (i < NUM_ADJUSTMENTS - 1);
	DYNAMIC_ASSERT (adjustment_table[i + 1].rs_ro_rh33 < adjustment_table[i].rs_ro_rh33);
	DYNAMIC_ASSERT (adjustment_table[i + 1].rs_ro_rh85 < adjustment_table[i].rs_ro_rh85);
	DYNAMIC_ASSERT (adjustment_table[i + 1].temperature > adjustment_table[i].temperature);
	DYNAMIC_ASSERT (temperature >= TEMP (adjustment_table[i].temperature));

	/* Linear interpolation. */
	d_resistance_ratio_rh33 = adjustment_table[i + 1].rs_ro_rh33 - adjustment_table[i].rs_ro_rh33;
	d_resistance_ratio_rh85 = adjustment_table[i + 1].rs_ro_rh85 - adjustment_table[i].rs_ro_rh85;
	d_temperature = TEMP (adjustment_table[i + 1].temperature - adjustment_table[i].temperature);

	*rs_ro_rh33 = adjustment_table[i].rs_ro_rh33 +
	              d_resistance_ratio_rh33 * (temperature - TEMP (adjustment_table[i].temperature)) / d_temperature;
	*rs_ro_rh85 = adjustment_table[i].rs_ro_rh85 +
	              d_resistance_ratio_rh85 * (temperature - TEMP (adjustment_table[i].temperature)) / d_temperature;

#undef TEMP
}

static Resistance _mq3_interpolate_resistance (Resistance rs_rh_t_g, TemperatureReading temperature, HumidityReading relative_humidity)
{
	ResistanceRatio rs_ro_adjustment, rs_33_t_4_over_rs_33_20_4, rs_85_t_4_over_rs_33_20_4;
	Resistance rs_rh_t_4;
	int16_t d_rs_rh_t_4_over_rs_rh_20_4;
	uint16_t d_humidity;

	/* Interpolate between temperatures in the data from Figure 4 to give a pair of R_s / R_o values at 33% and 85% RH which we can then
	 * interpolate between again. I guess this is technically bilinear interpolation. */
	_mq3_interpolate_adjustment_rs_ro (temperature, &rs_33_t_4_over_rs_33_20_4, &rs_85_t_4_over_rs_33_20_4);

	/* Linearly interpolate between the R_s / R_o values at RH 33% and 85% to give the adjustment to apply to the resistance ratio. This is the
	 * multiplier to apply to an R_s / R_o ratio taken at arbitrary RH and temperature in order to get a ratio at 33% RH and 20 degrees Celsius.
	 * Note that since we only have two data points, we perform the interpolation without clamping to [33, 85]% RH (i.e. we extrapolate). */
	d_rs_rh_t_4_over_rs_rh_20_4 = rs_85_t_4_over_rs_33_20_4 - rs_33_t_4_over_rs_33_20_4;
	d_humidity = HUMIDITY_READING_FROM_PERCENT (85, 0) - HUMIDITY_READING_FROM_PERCENT (33, 0);

	rs_ro_adjustment = (int32_t) rs_33_t_4_over_rs_33_20_4 +
	                   d_rs_rh_t_4_over_rs_rh_20_4 * ((int32_t) relative_humidity - (int32_t) HUMIDITY_READING_FROM_PERCENT (33, 0)) / d_humidity;
	rs_rh_t_4 = RESISTANCE_RATIO_MULTIPLY (rs_ro_adjustment, GAS_SENSOR_RS_33_20_4);

	/* The rs_rh_t_g input gives the R_s / R_o resistance ratio at the specified temperature and relative_humidity (assumed to be at 21% O_2
	 * concentration). */
	return ((uint32_t) rs_rh_t_g * GAS_SENSOR_RS_65_20_4) / rs_rh_t_4;
}

static GasReading _mq3_interpolate_sensitivity (ResistanceRatio rs_ro)
{
	uint8_t i = 0;
	uint8_t d_mg_l;
	int8_t d_rs_ro;

	/* Handle the cases where the resistance ratio is lower than the lowest entry in the table or higher than the highest entry. Just clamp to the
	 * extremum's gas concentration. */
	if (rs_ro >= sensitivity_table[0].rs_ro) {
		return sensitivity_table[0].mg_l;
	} else if (rs_ro <= sensitivity_table[NUM_SENSITIVITIES - 1].rs_ro) {
		return sensitivity_table[NUM_SENSITIVITIES - 1].mg_l;
	}

	/* Handle the typical cases where the resistance ratio lies within the table. */
	for (i = 0; i + 1 < NUM_SENSITIVITIES; i++) {
		/* Does the resistance ratio fall in the interval between sensitivity_table[i] and sensitivity_table[i + 1]? If so, linearly
		 * interpolate between the two gas concentrations. */
		if (rs_ro <= sensitivity_table[i].rs_ro && rs_ro > sensitivity_table[i + 1].rs_ro) {
			break;
		}
	}

	/* We should still be inside the table, and some invariants should hold. */
	DYNAMIC_ASSERT (i < NUM_SENSITIVITIES - 1);
	DYNAMIC_ASSERT (sensitivity_table[i + 1].rs_ro < sensitivity_table[i].rs_ro);
	DYNAMIC_ASSERT (sensitivity_table[i + 1].mg_l > sensitivity_table[i].mg_l);
	DYNAMIC_ASSERT (rs_ro <= sensitivity_table[i].rs_ro);

	/* Linear interpolation. */
	d_mg_l = sensitivity_table[i + 1].mg_l - sensitivity_table[i].mg_l;
	d_rs_ro = sensitivity_table[i + 1].rs_ro - sensitivity_table[i].rs_ro;

	return sensitivity_table[i].mg_l + (int16_t) d_mg_l * ((int16_t) rs_ro - (int16_t) sensitivity_table[i].rs_ro) / d_rs_ro;
}

/* Convert the given measured voltage from the gas sensor potential divider to a gas concentration, assuming the sensor took its measurement at the
 * given temperature and relative_humidity. i.e. The temperature and relative humidity were measured at the same time as the voltage, and the sensors
 * are colocated. */
static GasReading _mq3_convert_voltage_to_concentration (AdcReading voltage, TemperatureReading temperature, HumidityReading relative_humidity)
{
	Resistance rs_rh_t_g, rs_65_20_g;

	/* The input voltage is the tap off a potential divider between R_s (the sensing resistance of the sensor) and R_L (a fixed reference
	 * resistance). We assume the potential divider is purely resistive. This resistance varies with relative humidity, temperature and O_2
	 * concentration. We assume a fixed O_2 concentration of 21%, but adjust for temperature and relative humidity below. */
	rs_rh_t_g = ((uint32_t) GAS_SENSOR_RL * (uint32_t) ADC_READING_FROM_VOLTAGE (GAS_SENSOR_VCC)) / voltage - GAS_SENSOR_RL;

	/* Adjust the resistance for the temperature and relative humidity, returning an equivalent resistance for a measurement taken at 65% RH,
	 * 20 degrees Celsius. */
	rs_65_20_g = _mq3_interpolate_resistance (rs_rh_t_g, temperature, relative_humidity);

	/* Now we've adjusted R_s to 65% RH, 20 degrees Celsius, we can derive an R_s / R_o value which can be used to linearly interpolate into
	 * Figure 3 from the MQ-3 data sheet. This will yield a gas concentration in milligrams per litre. */
	return _mq3_interpolate_sensitivity (RESISTANCES_TO_RATIO (rs_65_20_g, GAS_SENSOR_RS_65_20_4));
}

MeasurementResponse gas_take_measurement (GasReading *measurement, TemperatureReading temperature, HumidityReading relative_humidity)
{
	/* Make the measurement. */
	*measurement = _mq3_convert_voltage_to_concentration (adc_take_measurement (ADC0), temperature, relative_humidity);

	return MEASUREMENT_SUCCESS;
}

/**
 * \brief Turn gas sensor heater on.
 *
 * Turn the heater for the gas sensor on.
 */
void gas_heater_on (void)
{
	GAS_SENSOR_HEATER_PORT |= (1 << GAS_SENSOR_HEATER_OUT);
}

/**
 * \brief Turn gas sensor heater off.
 *
 * Turn the heater for the gas sensor off.
 */
void gas_heater_off (void)
{
	GAS_SENSOR_HEATER_PORT &= ~(1 << GAS_SENSOR_HEATER_OUT);
}

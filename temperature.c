/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <avr/io.h>

#include "adc.h"
#include "common.h"
#include "config.h"
#include "temperature.h"

/**
 * \file
 * \brief Temperature sensor input handling.
 *
 * Handling of the temperature sensors, including data sanity checking and detection of sensor failure. This is important, as the temperature sensors
 * are safety critical since they're used as the (only) input to the heater control loop.
 */

void temperature_init (void)
{
	/* We assume ADC2 and ADC3 below when setting ADMUX and DIDR0. */
	STATIC_ASSERT (THERMOMETER0_IN == PA2 /* == ADC2 */);
	STATIC_ASSERT (THERMOMETER1_IN == PA3 /* == ADC3 */);

	/* Set the thermometer pins as inputs. */
	THERMOMETER0_DDR &= ~(1 << THERMOMETER0_IN);
	THERMOMETER1_DDR &= ~(1 << THERMOMETER1_IN);

	adc_init ();
	DIDR0 |= (1 << ADC2D) | (1 << ADC3D); /* disable digital input on ADC2 and ADC3 to save power */
}

/**
 * \brief Convert measured voltage to tenths of degrees Celsius.
 *
 * Convert a voltage output (as an AdcReading) from an MCP9700 to a fixed point temperature in tenths of degrees Celsius. There are two stages to this
 * calculation: converting the AdcReading to a voltage, then converting the voltage linearly to a temperature.
 *
 * Converting the AdcReading to a voltage follows Section 23.8 of the ATmega164/324/644 data sheet. The AdcReading is multiplied by AVCC, then divided
 * by 1024.
 *
 * Converting the voltage to a temperature uses a linear function with gradient 100 degrees Celsius per Volt, and intercept -50 degrees Celsius. This
 * was extracted by eye from the 'MCP9700' line on Figure 2-16 of the MCP9700 data sheet. Note that the figure's axes are swapped with respect to this
 * code. Finally, the whole calculation is multiplied by 10 to give an answer in tenths of a degree Celsius. This multiplication is pushed in as far
 * as possible to preserve accuracy in the division.
 *
 * Reference: MCP9700 data sheet, Figure 2-16.
 * Reference: ATmega164/324/644 data sheet, Section 23.8.
 */
#define MCP9700_READING_TO_TENTHS_OF_DEGREES(v) (((uint32_t) (AVCC * GRADIENT * 10.0) * (v)) / 1024 + INTERCEPT * 10)
#define GRADIENT 100.0 /* degrees Celsius per Volt */
#define INTERCEPT (-50) /* degrees Celsius */

MeasurementResponse temperature_take_measurement (TemperatureReading *measurements)
{
	AdcReading reading0, reading1;

	/* Make the measurements. These calls block. */
	reading0 = adc_take_measurement (ADC2);
	reading1 = adc_take_measurement (ADC3);

	/* Transform them to useful units (tenths of a degree). */
	measurements[0] = MCP9700_READING_TO_TENTHS_OF_DEGREES (reading0);
	measurements[1] = MCP9700_READING_TO_TENTHS_OF_DEGREES (reading1);

	/* Sanity check the measurements. Temperatures must not exceed 150 degrees Celsius. They must also not fall below 5 degrees Celsius, as that
	 * suggests the sensor has failed. The temperature measurements are safety critical, since they're used to control the heater; we want to
	 * ensure that the heater won't ever get turned on permanently because the temperature sensors are consistently under-reporting the
	 * temperature of the liquid. */
	if (measurements[0] > TEMPERATURE_READING_MAX || measurements[1] > TEMPERATURE_READING_MAX ||
	    measurements[0] < TEMPERATURE_READING_MIN || measurements[1] < TEMPERATURE_READING_MIN) {
		return MEASUREMENT_INVALID;
	}

	return MEASUREMENT_SUCCESS;
}

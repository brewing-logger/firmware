/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _KRAUSEN_LEVEL_H
#define _KRAUSEN_LEVEL_H

/**
 * \file
 * \brief Krausen level sensor input declarations.
 *
 * Declarations for symbols in krausen-level.c.
 */

/**
 * \brief Krausen level measurement.
 *
 * A krausen level measurement, in tenths of a centimetre (i.e. in a fixed point representation with a scaling factor of 10). Use the
 * KRAUSEN_LEVEL_READING_TO_CM_INT and KRAUSEN_LEVEL_READING_TO_CM_FRAC macros to convert a KrausenLevelReading to centimetres.
 *
 * For example, a reading of 215 equates to 21.5cm.
 *
 * This data type can represent distances from 0 to 25.4cm, inclusive.
 */
typedef uint8_t KrausenLevelReading;

/**
 * \brief Invalid krausen level measurement.
 *
 * If the krausen level sensor reports a measurement which is outside of the allowable range, the current log entry will use this as its value for the
 * krausen level measurement. This is guaranteed to not be a valid measurable value.
 */
#define KRAUSEN_LEVEL_READING_INVALID ((KrausenLevelReading) -1)

/**
 * \brief Convert a KrausenLevelReading to centimetres.
 *
 * Macro to convert a KrausenLevelReading from its native format to (integeric) centimetres. i.e. This yields the integeric part of the reading.
 * The result is truncated, rather than rounded. Use KRAUSEN_LEVEL_READING_TO_CM_FRAC to get the fractional part of the krausen level.
 *
 * Converting KRAUSEN_LEVEL_READING_INVALID will result in an undefined value.
 */
#define KRAUSEN_LEVEL_READING_TO_CM_INT(t) ((t) / 10)

/**
 * \brief Convert a KrausenLevelReading to fractional centimetres.
 *
 * Macro to convert a KrausenLevelReading from its native format to (fractional) centimetres. i.e. This yields the fractional part of the reading.
 *
 * Converting KRAUSEN_LEVEL_READING_INVALID will result in an undefined value.
 */
#define KRAUSEN_LEVEL_READING_TO_CM_FRAC(t) ((t) % 10)

void krausen_level_init (void);
MeasurementResponse krausen_level_take_measurement (KrausenLevelReading *measurement) NONNULL (1);

#endif /* !_KRAUSEN_LEVEL_H */

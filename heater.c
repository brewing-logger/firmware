/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <avr/io.h>

#include "heater.h"

/**
 * \file
 * \brief Heater control.
 *
 * Allows the heater for the primary fermenter to be turned on and off, and implements a control loop for maintaining a desired fermentation
 * temperature.
 *
 * The control loop uses 16-bit fixed point temperature calculations, but gives a 1-bit output (whether the heater should be on). PWM output to the
 * heater isn't supported since the heater is connected through a relay, which wouldn't react well to PWM usage. If PWM support is required, the relay
 * will have to be replaced with a triac.
 */

/**
 * \brief Heater controller state.
 *
 * State for the heater control loop. The controller is implemented as a proportional–integral controller, so just needs to store the current integral
 * state, which is the sum of previously measured errors. All parameters for the controller are compile-time constants.
 */
typedef struct {
	int32_t integral_state;	/**< Current state of the integral term (sum of past errors). */
} HeaterControllerState;

static HeaterControllerState heater_controller_state;

/**
 * \brief Initialise heater.
 *
 * Initialise the I/O ports for heater control, and set up the state for the control loop.
 */
void heater_init (void)
{
	/* Configure the heater control port as an output, initially turned off. */
	HEATER_DDR |= (1 << HEATER_OUT);
	HEATER_PORT &= ~(1 << HEATER_OUT);

	/* Set up the initial controller state. */
	heater_controller_state.integral_state = 0;
}

/**
 * \brief Turn heater on/off.
 *
 * Turn the heater on or off, according to the given \a state.
 *
 * \param state New state for the heater.
 */
void heater_set_state (HeaterState state)
{
	switch (state) {
		case HEATER_ON:
			HEATER_PORT |= (1 << HEATER_OUT);
			break;
		case HEATER_OFF:
			HEATER_PORT &= ~(1 << HEATER_OUT);
			break;
		default:
			ASSERT_NOT_REACHED ();
	}
}

/* Update the PI controller's state given a new pair of temperature and error measurements. This will return the new state of the controller, which
 * can be used to set PWM output (or just used to turn the heater on/off) */
static int32_t _heater_update_controller (HeaterControllerState *state, int16_t error, TemperatureReading temperature) NONNULL (1);

static int32_t _heater_update_controller (HeaterControllerState *state, int16_t error, TemperatureReading temperature)
{
	int32_t proportional_term, integral_term;

	/* Calculate the proportional term. */
	proportional_term = HEATER_PROPORTIONAL_GAIN * error;

	/* Calculate the integral term and limit if necessary. */
	state->integral_state += error;
	if (state->integral_state > HEATER_INTEGRAL_MAXIMUM) {
		state->integral_state = HEATER_INTEGRAL_MAXIMUM;
	} else if (state->integral_state < HEATER_INTEGRAL_MINIMUM) {
		state->integral_state = HEATER_INTEGRAL_MINIMUM;
	}

	integral_term = state->integral_state / HEATER_INTEGRAL_INVERSE_GAIN;

	return integral_term + proportional_term;
}

/**
 * \brief Update heater controller state.
 *
 * Update the controller for the heater, given the \a desired_temperature and \a current_temperature (both in tenths of a degree Celsius). This
 * function will return a new HeaterState for the heater, but won't actually apply it. The caller should call heater_set_state() themselves.
 *
 * \param desired_temperature Desired temperature (in tenths of a degree Celsius) (the *setpoint*).
 * \param current_temperature Current measured temperature (in tenths of a degree Celsius) (the *process variable*).
 * \returns New heater state (*manipulated variable*).
 */
HeaterState heater_update_controller (TemperatureReading desired_temperature, TemperatureReading current_temperature)
{
	int32_t new_value = _heater_update_controller (&heater_controller_state, desired_temperature - current_temperature, current_temperature);
	return (new_value > 0) ? HEATER_ON : HEATER_OFF;
}

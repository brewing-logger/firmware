/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <avr/io.h>

#include "lib/ds3231.h"

#include "rtc.h"

/**
 * \file
 * \brief Real-time clock control.
 *
 * Initialisation and interaction of the real-time clock peripheral so that accurate timestamps can be extracted from it for LogEntrys. The clock is
 * never initialised to an absolute time, but its use does guarantee that the relation between timestamps on log entries will always be accurate. The
 * absolute timestamp of a log entry can then be calculated from the timestamp of the first log entry and the start time in the brew configuration.
 */

/**
 * \brief Initialise RTC.
 *
 * Initialise the I2C bus and the RTC chip (which is the only slave on that bus). This does not set the current time on the RTC chip, but does ensure
 * that it's counting the progression of time.
 */
void rtc_init (void)
{
	uint8_t error_code;

	/* Set up the TWI ports for the RTC. */
	reset_rtc_i2c ();

	/* Set the control and status registers. */
	error_code = set_rtc_register_pointer (0x0e);
	_delay_us (10); /* let the I2C bus recover */
	if (error_code == 0) {
		/* Control and status registers. Disable everything we can. */
		const uint8_t control_values[] = { 0x04, 0x00 };
		error_code = write_i2c_device (0xD0, 2, (uint8_t *) &control_values);
	}
}

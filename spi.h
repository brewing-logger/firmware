/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPI_H
#define _SPI_H

/**
 * \file
 * \brief SPI bus control declarations.
 *
 * Declarations for symbols in spi.c.
 */

void spi_init (void);
void spi_select (uint8_t chip_select);
void spi_deselect (void);
void spi_enable (void);
void spi_disable (void);
uint8_t spi_transmit_byte (uint8_t data);

#endif /* !_SPI_H */

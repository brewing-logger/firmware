/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SLEEP_H
#define _SLEEP_H

/**
 * \file
 * \brief Sleep control declarations.
 *
 * Declarations for symbols in sleep.c.
 */

void sleep_init (void);

/**
 * \brief Wake up event.
 *
 * The cause for waking up and returning from sleep_until_event().
 */
typedef enum {
	WAKEUP_TIMER,	/**< The given \c sleep_time timeout was reached. */
	WAKEUP_BUTTON,	/**< The UI button was pressed by the user. */
} WakeupReason;

WakeupReason sleep_until_event (uint8_t sleep_time);

#endif /* !_SLEEP_H */

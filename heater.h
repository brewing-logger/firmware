/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _HEATER_H
#define _HEATER_H

#include "common.h"

/**
 * \file
 * \brief Heater control declarations.
 *
 * Declarations for symbols in heater.c.
 */

/**
 * \brief Heater power state.
 *
 * Whether the heater is turned on.
 */
typedef enum {
	HEATER_ON,	/**< Heater turned on. */
	HEATER_OFF,	/**< Heater turned off. */
} HeaterState;

void heater_init (void);
void heater_set_state (HeaterState state);
HeaterState heater_update_controller (TemperatureReading desired_temperature, TemperatureReading current_temperature);

#endif /* !_HEATER_H */

/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _LCD_H
#define _LCD_H

#include "lib/serial_lcd.h"

/**
 * \file
 * \brief LCD display output declarations.
 *
 * Declarations for symbols in lcd.c.
 */

/**
 * \brief Power states for the LCD.
 *
 * Different possible power states for the LCD and its controller.
 */
typedef enum {
	LCD_POWER_ON,	/**< LCD and controller are powered and the backlight is on. */
	LCD_POWER_OFF,	/**< LCD and controller are completely powered down. */
} LcdPowerState;

void lcd_init (void);
void lcd_power_set_state (LcdPowerState new_state);
LcdPowerState lcd_power_get_state (void);

#endif /* !_LCD_H */

/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <avr/io.h>
#include <util/delay.h>

#include "adc.h"
#include "power.h"

/**
 * \file
 * \brief Power management.
 *
 * Handling of power domains (mainly the sensor power domain), plus disabling unused microcontroller features at initialisation time.
 */

/**
 * \brief Initialise power management.
 *
 * Set up power management, including the sensor power domain. Initialise the sensor power domain to be off by default.
 */
void power_init (void)
{
	SENSOR_POWER_DDR |= (1 << SENSOR_POWER_OUT);
	sensor_power_set_state (SENSOR_POWER_OFF);

	/* Turn off various other bits of hardware we never use. */
	ACSR &= ~(1 << ACIE); /* analogue comparator */
	ACSR |= (1 << ACD);
	DIDR1 |= (1 << AIN1D) | (1 << AIN0D);

	DIDR0 = 0xff; /* ADC digital inputs */
}

/**
 * \brief Turn sensor power on/off.
 *
 * Turn the power supply for the sensors and associated hardware (such as the ADC) on or off.
 */
void sensor_power_set_state (SensorPowerState new_state)
{
	switch (new_state) {
		case SENSOR_POWER_ON:
			SENSOR_POWER_PORT |= (1 << SENSOR_POWER_OUT);
			adc_power_up ();
			_delay_ms (2000); /* gas sensor requires 600us to stabilise; humidity sensor requires 2s */
			break;
		case SENSOR_POWER_OFF:
			adc_power_down ();
			SENSOR_POWER_PORT &= ~(1 << SENSOR_POWER_OUT);
			break;
		default:
			ASSERT_NOT_REACHED ();
	}
}

/**
 * \brief Get sensor power state.
 *
 * Get the state of the power supply to the sensors.
 */
SensorPowerState sensor_power_get_state (void)
{
	return (SENSOR_POWER_PORT & (1 << SENSOR_POWER_OUT)) ? SENSOR_POWER_ON : SENSOR_POWER_OFF;
}

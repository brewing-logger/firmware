/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _LOG_H
#define _LOG_H

#include "common.h"
#include "humidity.h"
#include "temperature.h"
#include "krausen-level.h"
#include "gas.h"

/**
 * \file
 * \brief Log entry declarations.
 *
 * Common declarations for log entries, including the storage format for them on flash rom and the SD card.
 */

/**
 * \brief Brewing log entry.
 *
 * An entry recording various sensor measurements at a particular instant in time. The brewing logger records a series of these to log the
 * fermentation process.
 *
 * \note The log entries are stored on the SD card and Flash ROM in the same format as this, with some extra metadata appended. Any incompatible
 *       change to this data structure must be accompanied by a version bump (BREW_CONFIG_VERSION).
 */
typedef struct {
	/* Metadata. */
	TimerReading time;						/**< Time at which the measurement was taken. */

	/* Sensor readings. */
	HumidityTemperatureReading humidity_reading;			/**< Relative humidity. */
	TemperatureReading temperature_reading[NUM_THERMOMETERS];	/**< Temperatures. */
	KrausenLevelReading krausen_level_reading;			/**< Krausen level. */
	GasReading gas_reading;						/**< Alcohol gas concentration. */
} LogEntry;

#endif /* !_LOG_H */

/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _POWER_H
#define _POWER_H

/**
 * \file
 * \brief Power management declarations.
 *
 * Declarations for symbols in power.c.
 */

/**
 * \brief Power states for the sensors.
 *
 * Different possible power states for the sensors.
 */
typedef enum {
	SENSOR_POWER_ON,	/**< Sensors are powered. */
	SENSOR_POWER_OFF,	/**< Sensors are completely powered down. */
} SensorPowerState;

void power_init (void);
void sensor_power_set_state (SensorPowerState new_state);
SensorPowerState sensor_power_get_state (void);

#endif /* !_POWER_H */

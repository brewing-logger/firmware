/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GAS_H
#define _GAS_H

#include "common.h"
#include "humidity.h"

/**
 * \file
 * \brief Alcohol gas concentration sensor input declarations.
 *
 * Declarations for symbols in gas.c.
 */

/**
 * \brief Alcohol gas concentration measurement.
 *
 * An alcohol gas concentration measurement, in hundredths of a milligram per litre (i.e. in a fixed point representation with a scaling factor of
 * 100). Use the GAS_READING_TO_MG_L_INT and GAS_READING_TO_MG_L_FRAC macros to convert a GasReading to milligrams per litre. All gas concentrations
 * are assumed to be at 21% O_2 concentration, at a temperature and relative humidity specified when calling gas_take_measurement(). It is the
 * caller's responsibility to maintain an association between a GasReading and its temperature and relative humidity.
 *
 * For example, a reading of 620 equates to 6.20 mg/l alcohol gas concentration.
 *
 * This data type can represent concentrations from 0.00 mg/l to 655.34 mg/l, inclusive.
 */
typedef uint16_t GasReading;

/**
 * \brief Invalid gas measurement.
 *
 * If the gas sensor reports a measurement which is outside of the allowable range, the current log entry will use this as its value for the gas
 * measurement. This is guaranteed to not be a valid measurable value.
 */
#define GAS_READING_INVALID ((GasReading) -1)

/**
 * \brief Convert a GasReading to milligrams per litre.
 *
 * Macro to convert a GasReading from its native format to (integeric) milligrams per litre. i.e. This yields the integeric part of the reading.
 * The result is truncated, rather than rounded. Use GAS_READING_TO_MG_L_FRAC to get the fractional part of the gas concentration.
 *
 * Converting GAS_READING_INVALID will result in an undefined value.
 */
#define GAS_READING_TO_MG_L_INT(t) ((t) / 100)

/**
 * \brief Convert a GasReading to fractional milligrams per litre.
 *
 * Macro to convert a GasReading from its native format to (fractional) milligrams per litre. i.e. This yields the fractional part of the reading.
 *
 * Converting GAS_READING_INVALID will result in an undefined value.
 */
#define GAS_READING_TO_MG_L_FRAC(t) ((t) % 100)

/**
 * \brief Default humidity for a gas concentration measurement.
 *
 * If the relative humidity isn't known when taking a gas concentration measurement (e.g. due to the humidity sensor reporting an error), this value
 * may be used as the estimated humidity for the gas concentration without making the results too inaccurate.
 */
#define GAS_READING_DEFAULT_HUMIDITY 650 /* percent relative humidity, in tenths of a degree, so this is 65.0% */

/**
 * \brief Default temperature for a gas concentration measurement.
 *
 * If the temperature isn't known when taking a gas concentration measurement (e.g. due to the thermometer reporting an error), this value may be used
 * as the estimated temperature for the gas concentration without making the results too inaccurate.
 */
#define GAS_READING_DEFAULT_TEMPERATURE 200 /* degrees Celsius, in tenths of a degree, so this is 20.0 degrees Celsius */

void gas_init (void);
MeasurementResponse gas_take_measurement (GasReading *measurement, TemperatureReading temperature, HumidityReading relative_humidity) NONNULL (1);
void gas_heater_on (void);
void gas_heater_off (void);

#endif /* !_GAS_H */

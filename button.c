/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <avr/interrupt.h>
#include <avr/io.h>

#include "button.h"
#include "common.h"

/**
 * \file
 * \brief Button input handling.
 *
 * Handling of button presses, debouncing, and the interaction between button interrupts and the main loop. This is tightly coupled to the code and
 * functions in sleep.c.
 */

/* Note that this assumes interrupts have been enabled globally in SREG for button presses to be detected.
 *
 * Since the button is configured to use INT0, it can wake the processor up from deep sleep. Debouncing is performed by simply disabling the button
 * interrupt for a short period after the first interrupt. The bouncing should've stopped after that period, so the interrupt is re-enabled. */
void button_init (void)
{
	/* We assume the button is connected to INT0, which means PD2. */
	STATIC_ASSERT (BUTTON_IN == PD2);

	/* Configure the button to be an input with its pull-up resistor enabled. The physical button will pull the input pin low when pressed. */
	BUTTON_DDR &= ~(1 << BUTTON_IN);
	BUTTON_PORT |= (1 << BUTTON_IN);

/* Reference: ATmega164/324/644 manual, Section 13.2.2. For some reason, AVR doesn't define this. */
#define IINT0 0

	/* Enable INT0 to be triggered on falling edges. Disable the interrupt while we change this. */
	EIMSK &= ~(1 << IINT0);
	EICRA = (EICRA & 0xfc) | 0x02;
	EIFR |= (1 << INTF0);
	EIMSK |= (1 << IINT0);
}

static volatile uint8_t button_pressed = 0;

/* Technically should be 'button_has_been_pressed_since_last_time_this_was_called', but that's a bit of a mouthful. */
uint8_t button_was_pressed (void)
{
	uint8_t _button_was_pressed = button_pressed;
	button_pressed = 0; /* reset the button press */
	return _button_was_pressed;
}

/* Reset the button, disabling the debounce timer and enabling the interrupt. */
void button_reset (void)
{
	/* Disable the timer. */
	TCCR0B &= ~(1 << CS02) & ~(1 << CS01) & ~(1 << CS00);

	/* Re-enable the button interrupt. */
	EIMSK |= (1 << IINT0);
}

ISR (INT0_vect)
{
	/* Disable the button interrupt for debouncing. */
	EIMSK &= ~(1 << IINT0);

	/* Set a timer to re-enable the button interrupt. */
	TCCR0A = (1 << WGM01); /* normal port operation, CTC mode, waveform generation mode off */
	TCCR0B = (1 << CS02); /* prescale by 256 */
	TIMSK0 = (1 << OCIE0A); /* generate an interrupt on Timer0 Output Compare Match A */
	OCR0A = 80; /* a 1MHz clock prescaled by 256 gives a timer counter period of 256us; we want a debounce period of 20ms; so 20/0.256 ~= 80 */
	TCNT0 = 0; /* clear the counter */

	/* Mark the button as pressed. */
	button_pressed = 1;

	/* At this point the main stack should be woken up from _sleep_8s() in sleep.c if it was sleeping there to begin with. Otherwise, _sleep_8s()
	 * will check button_pressed before next sleeping. */
}

ISR (TIMER0_COMPA_vect)
{
	button_reset ();
}

/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _HUMIDITY_H
#define _HUMIDITY_H

#include "common.h"

/**
 * \file
 * \brief Humidity sensor input declarations.
 *
 * Declarations for symbols in humidity.c.
 */

/**
 * \brief Humidity measurement.
 *
 * A relative humidity measurement, in tenths of a percentage point (i.e. in a fixed point representation with a scaling factor of 10). Use the
 * HUMIDITY_READING_TO_PERCENT_INT and HUMIDITY_READING_TO_PERCENT_FRAC macros to convert a HumidityReading to percentage points.
 *
 * For example, a reading of 215 equates to 21.5% relative humidity.
 *
 * This data type can represent relative humidities from 0.0% to 100.0%, inclusive. Values outside this range (i.e. above 1000) are invalid.
 */
typedef uint16_t HumidityReading;

/**
 * \brief Invalid humidity measurement.
 *
 * If the humidity sensor reports a measurement which is outside of the allowable range, or a checksum failure occurs while taking the measurement,
 * the current log entry will use this as its value for the humidity measurement. This is guaranteed to not be a valid measurable value.
 */
#define HUMIDITY_READING_INVALID ((HumidityReading) -1)

/**
 * \brief Convert a HumidityReading to percent.
 *
 * Macro to convert a HumidityReading from its native format to (integeric) percentage points. i.e. This yields the integeric part of the reading.
 * The result is truncated, rather than rounded. Use HUMIDITY_READING_TO_PERCENT_FRAC to get the fractional part of the relative humidity.
 *
 * Converting HUMIDITY_READING_INVALID will result in an undefined value.
 */
#define HUMIDITY_READING_TO_PERCENT_INT(t) ((t) / 10)

/**
 * \brief Convert a HumidityReading to fractional percent.
 *
 * Macro to convert a HumidityReading from its native format to (fractional) percentage points. i.e. This yields the fractional part of the reading.
 *
 * Converting HUMIDITY_READING_INVALID will result in an undefined value.
 */
#define HUMIDITY_READING_TO_PERCENT_FRAC(t) ((t) % 10)

/**
 * \brief Convert a percentage to a HumidityReading.
 *
 * Macro to convert a percentage relative humidity, given as an integer and fractional part, to a HumidityReading in its native format. Fractional
 * parts of 10 or over will have their modulus taken.
 *
 * For example: HUMIDITY_READING_FROM_PERCENT(10, 5) gives a HumidityReading representing a relative humidity of 10.5%.
 */
#define HUMIDITY_READING_FROM_PERCENT(int, frac) (((HumidityReading) (int) * 10) + ((frac) % 10))

/**
 * \brief Maximum value for a HumidityReading.
 *
 * The maximum valid value for a HumidityReading. This corresponds to 100.0% relative humidity.
 */
#define HUMIDITY_READING_MAX 1000

/**
 * \brief Humidity/Temperature measurement.
 *
 * A combined relative humidity and temperature measurement, with both readings taken at the same time by the same sensor. This may allow for
 * adjustment of the relative humidity to compensate for the measurement temperature.
 *
 * \todo TODO: Investigate adjusting the humidity for the temperature.
 */
typedef struct {
	HumidityReading relative_humidity;	/**< Relative humidity in tenths of percentage points. */
	TemperatureReading temperature;		/**< Temperature in tents of degrees Celsius. */
} HumidityTemperatureReading;

void humidity_init (void);
MeasurementResponse humidity_take_measurement (HumidityTemperatureReading *measurement) NONNULL (1);

#endif /* !_HUMIDITY_H */

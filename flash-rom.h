/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _FLASH_ROM_H
#define _FLASH_ROM_H

#include "log.h"

/**
 * \file
 * \brief Flash memory control declarations.
 *
 * Declarations for symbols in flash-rom.c.
 */

void flash_rom_init (void);
void flash_rom_append_entry (LogEntry *entry) NONNULL (1);

/**
 * \brief Flash ROM read entry response.
 *
 * Whether the LogEntry returned by flash_rom_read_head_entry() is valid. It is only valid if flash_rom_read_head_entry() returns
 * FLASH_ROM_READ_ENTRY_SUCCESS. In all other cases, the entry will be written out as received from the Flash ROM, but its contents may be invalid.
 */
typedef enum {
	FLASH_ROM_READ_ENTRY_SUCCESS = 0,	/**< Success. */
	FLASH_ROM_READ_ENTRY_CHECKSUM_FAILURE,	/**< Checksum did not match entry contents. */
} FlashRomReadEntryResponse;

FlashRomReadEntryResponse flash_rom_read_head_entry (LogEntry *entry) NONNULL (1);
void flash_rom_overwrite_head_entry_checksum (void);
void flash_rom_erase (void);

/**
 * \brief Flash ROM flush response.
 *
 * Whether flushing log entries from the Flash ROM to the SD card was successful. FLASH_ROM_FLUSH_SUCCESS is returned if and only if at least the
 * current sector's worth of log entries were successfully flushed from the Flash ROM to the SD card, and marked as flushed on the Flash ROM.
 * Otherwise, a failure code is returned. In case of failure, one or more log entries may still have been successfully flushed from the Flash ROM to
 * the SD card. If so, they will have been marked as flushed on the Flash ROM.
 */
typedef enum {
	FLASH_ROM_FLUSH_SUCCESS = 0,	/**< Success. */
	FLASH_ROM_FLUSH_SD_CARD_ERROR,	/**< Error received from the SD card part-way through the flush operation. */
} FlashRomFlushResponse;

FlashRomFlushResponse flash_rom_flush_to_sd_card (void);

void flash_rom_power_down (void);
void flash_rom_power_up (void);

#endif /* !_FLASH_ROM_H */

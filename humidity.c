/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <avr/io.h>

#include <util/delay_basic.h>
#include <util/delay.h>

#include "common.h"
#include "humidity.h"

/**
 * \file
 * \brief Humidity sensor input handling.
 *
 * Handling of the humidity sensor, including checksum handling and data sanity checking.
 */

/* Reference: RHT03 data sheet. */

static void _rht03_port_input_mode (void)
{
	/* Set the DDR to input mode, and disable the internal pull-up resistor. */
	HUMIDITY_SENSOR_DDR &= ~(1 << HUMIDITY_SENSOR_DATA);
	HUMIDITY_SENSOR_PORT &= ~(1 << HUMIDITY_SENSOR_DATA);
}

static void _rht03_port_output_mode (void)
{
	/* Set the DDR to output mode. */
	HUMIDITY_SENSOR_DDR |= (1 << HUMIDITY_SENSOR_DATA);
}

static void _rht03_port_low (void)
{
	HUMIDITY_SENSOR_PORT &= ~(1 << HUMIDITY_SENSOR_DATA);
}

static void _rht03_port_high (void)
{
	HUMIDITY_SENSOR_PORT |= (1 << HUMIDITY_SENSOR_DATA);
}

/* NOTE: This returns 0 or an unspecified non-zero value. Do *not* compare it directly to 1. */
static uint8_t _rht03_port_read (void)
{
	return (HUMIDITY_SENSOR_PIN & (1 << HUMIDITY_SENSOR_DATA));
}

/* The bit is the LSB of the return value. */
static uint8_t _rht03_read_bit (void)
{
	uint8_t low_counter = 0, high_counter = 0;

	/* The RHT03 transmits each bit as a low level on the data line, followed by a variable-length high level. The duration of the high level
	 * indicates whether the bit is a 1 or a 0.
	 * A 1 is transmitted as 50us low followed by 70us high.
	 * A 0 is transmitted as 50us low followed by 26--28us high.
	 *
	 * We count the length of the low period and compare this to the length of the high period to work out the value of the bit. In either case,
	 * if we end up counting for more than 50% longer than the expected period, break out. We don't return an error status because the checksum
	 * should catch the failure. */
	while (_rht03_port_read () == 0) {
		_delay_us (5);

		if (low_counter++ >= 15) {
			break;
		}
	}

	while (_rht03_port_read () != 0) {
		_delay_us (5);

		if (high_counter++ >= 20) {
			break;
		}
	}

	return (high_counter > low_counter) ? 1 : 0;
}

static uint8_t _rht03_read_byte (void)
{
	unsigned int i;
	uint8_t output = 0;

	/* The protocol doesn't frame bytes specially (i.e. there's no start bit or stop bit. */
	for (i = 0; i < 8; i++) {
		output = (output << 1) | _rht03_read_bit ();
	}

	return output;
}

void humidity_init (void)
{
	/* Initialise the port for the humidity sensor to be an output port. We'll change to being an input port when receiving data. */
	_rht03_port_output_mode ();

	/* Serial line for the humidity sensor is normally high. */
	_rht03_port_high ();
}

MeasurementResponse humidity_take_measurement (HumidityTemperatureReading *measurement)
{
	uint8_t humidity_int;
	uint8_t humidity_frac;
	uint8_t temperature_int;
	uint8_t temperature_frac;
	uint8_t checksum;
	uint16_t humidity;
	uint16_t temperature;
	uint8_t retry_counter;

	/* Disable interrupts for the duration of this critical section. */
	SREG &= ~(1 << 7);

	retry_counter = 0;
	do {
		if (retry_counter++ >= 10) {
			return MEASUREMENT_TIMEOUT;
		}

		/* Send a start signal to the RHT03. This means taking the data line low for 1--10ms, then taking it high and waiting for 20--40us for
		 * a response. */
		_rht03_port_output_mode ();
		_rht03_port_low ();
		_delay_us (1100);
		_rht03_port_high ();

		/* Change the data line to be an input and wait until the RHT03 pulls it low, which should happen 20--40us after we take it high. If
		 * 45us has passed and the sensor hasn't pulled the line low, loop and try again. */
		_rht03_port_input_mode ();

		_delay_us (45);
	} while (_rht03_port_read () != 0);

	/* Wait for the RHT03 to pull the line low for 80us. The RHT03 will then take it high again for 80us, then transmission will start. */
	retry_counter = 0;
	do {
		if (retry_counter++ >= 10) {
			return MEASUREMENT_TIMEOUT;
		}

		_delay_us (10);
	} while (_rht03_port_read () == 0);

	retry_counter = 0;
	do {
		if (retry_counter++ >= 10) {
			return MEASUREMENT_TIMEOUT;
		}

		_delay_us (10);
	} while (_rht03_port_read () != 0);

	/* Receive the data. */
	humidity_int = _rht03_read_byte ();
	humidity_frac = _rht03_read_byte ();
	temperature_int = _rht03_read_byte ();
	temperature_frac = _rht03_read_byte ();
	checksum = _rht03_read_byte ();

	/* Once the data has been received, the RHT03 pulls the data line low. We then change it to be an output and take it high again. */
	_rht03_port_output_mode ();
	_rht03_port_high ();

	/* Re-enable interrupts. */
	SREG |= (1 << 7);

	/* Validate the data against the checksum. The checksum is literally just the sum of the other four fields (truncated to 8 bits). */
	if (checksum != (uint8_t) (humidity_int + humidity_frac + temperature_int + temperature_frac)) {
		return MEASUREMENT_CHECKSUM_FAILURE;
	}

	/* Return the measurement. The RH value is given as a percentage (and the value from the wire needs to be divided by 10 to get this), and the
	 * temperature is given in celsius. The value on the wire is given as a sign and magnitude, and the magnitude needs to be divided by 10 to
	 * give celsius. We ignore the sign and don't divide by 10 so the result remains in tenths of a degree (as required by TemperatureReading). */
	humidity = (((uint16_t) humidity_int) << 8) | humidity_frac;
	temperature = (((uint16_t) temperature_int) << 8) | temperature_frac;

	measurement->relative_humidity = humidity;
	measurement->temperature = (temperature & 0x7fff);

	/* Sanity check the measurements. Relative humidities must not exceed 100.0%, and temperatures must not be negative. Temperatures must not
	 * exceed 150 degrees Celsius or fall below 5 degrees Celsius. */
	if (measurement->relative_humidity > HUMIDITY_READING_MAX || (temperature & 0x1000) != 0 ||
	    temperature > TEMPERATURE_READING_MAX || temperature < TEMPERATURE_READING_MIN) {
		return MEASUREMENT_INVALID;
	}

	return MEASUREMENT_SUCCESS;
}

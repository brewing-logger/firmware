/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <avr/interrupt.h>

#include "adc.h"

/**
 * \file
 * \brief ADC control.
 *
 * Utility functions to trigger a measurement using the ADC, including blocking on conversion completion and handling ADC interrupts.
 */

static volatile uint8_t adc_captured_high, adc_captured_low, adc_conversion_complete = 0;

void adc_init (void)
{
	/* Set up the ADC. Callers will probably also want to set bits on DIDR0 for their input pin, and ensure the DDR for their pin is set to 0. */
	ADCSRA = (1 << ADEN) | (1 << ADIE) | (1 << ADPS1) | (1 << ADPS0); /* enable ADC, don't start conversion, don't auto trigger, enable interrupts, prescale 1MHz clock by 8 to give 125kHz */
	ADCSRB = 0; /* free running mode */
}

AdcReading adc_take_measurement (AdcInput adc_mux)
{
	AdcReading retval;

	/* The ADC must be enabled. */
	DYNAMIC_ASSERT (ADCSRA & (1 << ADEN));

	/* Set up the ADC to measure from the specified input. */
	ADMUX = (1 << REFS0) | (adc_mux & 0x07); /* AVCC used with capacitor on AREF, right-adjusted result, adc_mux as input first */

	/* Start the ADC measurement. We will get an interrupt in ADC_vect once it's complete. */
	adc_conversion_complete = 0;
	ADCSRA |= (1 << ADSC);

	/* Spin until we receive the interrupt. This is acceptable, as a typical ADC conversion will take ~14 ADC cycles, with each ADC cycle (125kHz) being 8 CPU cycles.
	 * This gives a delay of ~14 * 8 = 112 cycles, which is not worth going to sleep for. See: ATmega644A manual, Section 23.5. */
	while (adc_conversion_complete == 0);

	/* Read out the measurement. */
	retval = ((uint16_t) adc_captured_high << 8) | adc_captured_low;
	adc_conversion_complete = 0;

	return retval;
}

ISR (ADC_vect)
{
	adc_captured_low = ADCL;
	adc_captured_high = ADCH;
	adc_conversion_complete = 1;
}

/**
 * \brief Power up the ADC.
 *
 * Enable the ADC. This must be done before any conversions can be performed using adc_take_measurement().
 */
void adc_power_up (void)
{
	ADCSRA |= (1 << ADEN);
}

/**
 * \brief Power down the ADC.
 *
 * Disable the ADC. If this is done while a conversion is in progress, the conversion will be aborted.
 */
void adc_power_down (void)
{
	ADCSRA &= ~(1 << ADEN);
}

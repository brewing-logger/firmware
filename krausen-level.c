/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <avr/io.h>

#include "adc.h"
#include "common.h"
#include "config.h"
#include "krausen-level.h"

/**
 * \file
 * \brief Krausen level sensor input handling.
 *
 * Handling of the krausen level sensor, which uses triangulated IR beams to work out the distance from the lid of the fermentation vessel to the top
 * of the krausen (the foamy head on top of the fermenting liquid). This includes taking measurements and normalising them against the sensor's
 * response curve.
 */

void krausen_level_init (void)
{
	/* We assume ADC0 below when setting ADMUX and DIDR0. */
	STATIC_ASSERT (PROXIMITY_SENSOR_IN == PA1 /* == ADC1 */);

	/* Set the proximity sensor pin as an input. */
	PROXIMITY_SENSOR_DDR &= ~(1 << PROXIMITY_SENSOR_IN);

	adc_init ();
	DIDR0 |= (1 << ADC1D); /* disable digital input on ADC1 to save power */
}

/**
 * \brief Convert measured voltage to centimetres.
 *
 * Convert a voltage output from a GP2Y0A21YK0F to a fixed point distance in tenths of centimetres. This is not a linear conversion, since the
 * sensor's response is exponential. We approximate by linear interpolation between the data points given in the lower graph in Figure 2.
 *
 * Reference: GP2Y0A21YK0F data sheet, Figure 2.
 */
static inline KrausenLevelReading _gp2y0a21yk0f_convert_voltage_to_distance (AdcReading voltage)
{
	/* This table is lifted straight from the lower graph in Figure 2. It must be kept in increasing order of voltage. Note that the distances in
	 * this table are in centimetres, rather than tenths of centimetres, so need additional conversion to KrausenLevelReading format. */
	static struct {
		AdcReading voltage; /* Volts */
		uint8_t distance; /* centimetres */
	} characteristics[] = {
		{ ADC_READING_FROM_VOLTAGE (0.4), 80 },
		{ ADC_READING_FROM_VOLTAGE (0.6), 50 },
		{ ADC_READING_FROM_VOLTAGE (0.7), 40 },
		{ ADC_READING_FROM_VOLTAGE (0.9), 30 },
		{ ADC_READING_FROM_VOLTAGE (1.1), 25 },
		{ ADC_READING_FROM_VOLTAGE (1.3), 20 },
		{ ADC_READING_FROM_VOLTAGE (1.6), 15 },
		{ ADC_READING_FROM_VOLTAGE (2.3), 10 },
		{ ADC_READING_FROM_VOLTAGE (2.7),  8 },
		{ ADC_READING_FROM_VOLTAGE (3.0),  7 },
		{ ADC_READING_FROM_VOLTAGE (3.1),  6 },
		/* data point for 5cm omitted because its voltage is lower than 3.1V */
	};
	#define NUM_CHARACTERISTICS (sizeof (characteristics) / sizeof (*characteristics))

	uint8_t i = 0;
	int16_t d_distance;
	uint16_t d_voltage;

	/* Handle the cases where voltage is lower than the lowest entry in the table or higher than the highest entry. Just clamp to the extremum's
	 * distance. */
	if (voltage <= characteristics[0].voltage) {
		return (KrausenLevelReading) characteristics[0].distance * 10;
	} else if (voltage >= characteristics[NUM_CHARACTERISTICS - 1].voltage) {
		return (KrausenLevelReading) characteristics[NUM_CHARACTERISTICS - 1].distance * 10;
	}

	/* Handle the typical cases where voltage lies within the table. */
	for (i = 0; i + 1 < (uint8_t) NUM_CHARACTERISTICS; i++) {
		/* Does the voltage fall in the interval between characteristics[i] and characteristics[i + 1]? If so, linearly interpolate between
		 * the two distances. */
		if (voltage >= characteristics[i].voltage && voltage < characteristics[i + 1].voltage) {
			break;
		}
	}

	/* We should still be inside the table, and some invariants should hold. */
	DYNAMIC_ASSERT (i < NUM_CHARACTERISTICS - 1);
	DYNAMIC_ASSERT (voltage >= characteristics[i].voltage);
	DYNAMIC_ASSERT (characteristics[i + 1].voltage > characteristics[i].voltage);

	/* Linear interpolation. */
	d_distance = (characteristics[i + 1].distance - characteristics[i].distance); /* guaranteed to be negative */
	d_voltage = (characteristics[i + 1].voltage - characteristics[i].voltage); /* guaranteed to be positive */

	return ((int32_t) characteristics[i].distance * 10 /* convert to mm */) + ((int32_t) d_distance * 10 /* convert to mm */) *
	       (int32_t) (voltage - characteristics[i].voltage) / (int32_t) d_voltage;
}

MeasurementResponse krausen_level_take_measurement (KrausenLevelReading *measurement)
{
	/* Make the measurement. */
	*measurement = _gp2y0a21yk0f_convert_voltage_to_distance (adc_take_measurement (ADC1));

	return MEASUREMENT_SUCCESS;
}

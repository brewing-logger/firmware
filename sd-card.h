/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SD_CARD_H
#define _SD_CARD_H

#include "common.h"
#include "log.h"

/**
 * \file
 * \brief SD card control declarations.
 *
 * Declarations for symbols in sd-card.c.
 */

/**
 * \brief Common responses to SD card operations.
 *
 * Responses to SD card operations which are common to all operations.
 */
typedef enum {
	SD_CARD_RESPONSE_SUCCESS = 0,			/**< Success. */
	SD_CARD_RESPONSE_IO_ERROR = 1,			/**< I/O error on the bus or SD card. */
	SD_CARD_RESPONSE_MISC_ERROR = 2,		/**< Miscellaneous error. */
	SD_CARD_RESPONSE_NOT_EXIST = 3,			/**< Requested file does not exist. */
	SD_CARD_RESPONSE_INVALID_PERMISSIONS = 4,	/**< File permissions prohibit the operation. */
} SdCardCommonResponse;

/**
 * \brief Reponses to an sd_card_init() operation.
 *
 * Possible return values from sd_card_init().
 */
typedef enum {
	/* Common. */
	SD_CARD_INIT_SUCCESS = SD_CARD_RESPONSE_SUCCESS,	/**< Success. */
	SD_CARD_INIT_IO_ERROR = SD_CARD_RESPONSE_IO_ERROR,	/**< I/O error on the bus or SD card. */
	SD_CARD_INIT_MISC_ERROR = SD_CARD_RESPONSE_MISC_ERROR,	/**< Miscellaneous error. */

	/* Specialised. */
	SD_CARD_INIT_NO_CARD_PRESENT = 100,			/**< No SD card inserted in the SD card slot. */
	SD_CARD_INIT_CARD_WRITE_PROTECTED = 101,		/**< SD card is write protected. */
} SdCardInitResponse;

SdCardInitResponse sd_card_init (void);

/**
 * \brief Responses to a load file operation.
 *
 * Possible return values from sd_card_load_brew_config() or other functions which load a file from the SD card.
 */
typedef enum {
	/* Common. */
	SD_CARD_LOAD_FILE_SUCCESS = SD_CARD_RESPONSE_SUCCESS,				/**< Success. */
	SD_CARD_LOAD_FILE_NOT_EXIST = SD_CARD_RESPONSE_NOT_EXIST,			/**< Requested file does not exist. */
	SD_CARD_LOAD_FILE_INVALID_PERMISSIONS = SD_CARD_RESPONSE_INVALID_PERMISSIONS,	/**< File permissions prohibit reads. */
	SD_CARD_LOAD_FILE_IO_ERROR = SD_CARD_RESPONSE_IO_ERROR,				/**< I/O error on the bus or SD card. */
	SD_CARD_LOAD_FILE_MISC_ERROR = SD_CARD_RESPONSE_MISC_ERROR,			/**< Miscellaneous error. */

	/* Specialised. */
	SD_CARD_LOAD_FILE_UNSUPPORTED_VERSION = 100,					/**< Version of brew configuration file is not supported. */
	SD_CARD_LOAD_FILE_CHECKSUM_FAILURE = 101,					/**< Brew configuration checksum didn't match file content. */
} SdCardLoadFileResponse;

SdCardLoadFileResponse sd_card_load_brew_config (BrewConfig *brew_config) NONNULL (1);

/**
 * \brief Responses to a write file operation.
 *
 * Possible return values from sd_card_create_brew_config(), sd_card_append_log_entries(), or other functions which write to a file on the SD card.
 */
typedef enum {
	/* Common. */
	SD_CARD_WRITE_FILE_SUCCESS = SD_CARD_RESPONSE_SUCCESS,				/**< Success. */
	SD_CARD_WRITE_FILE_IO_ERROR = SD_CARD_RESPONSE_IO_ERROR,			/**< I/O error on the bus or SD card. */
	SD_CARD_WRITE_FILE_MISC_ERROR = SD_CARD_RESPONSE_MISC_ERROR,			/**< Miscellaneous error. */
	SD_CARD_WRITE_FILE_INVALID_PERMISSIONS = SD_CARD_RESPONSE_INVALID_PERMISSIONS,	/**< File permissions prohibit writes or
	                                                                                     directory permissions prohibit file creation. */
} SdCardWriteFileResponse;

SdCardWriteFileResponse sd_card_create_brew_config (const BrewConfig *brew_config) NONNULL (1);

/* The ATmega1284P has 16KiB of data RAM. When transferring log entries from the Flash ROM to the SD card, the entries need to be held in memory
 * temporarily. We want to transfer as many entries as possible at once, to minimise the overhead of switching between the Flash ROM and SD card
 * (several times). Here, we choose to use at most 4 KiB of RAM to transfer as many entries as will fit in there. */
#define SD_CARD_APPEND_LOG_ENTRIES_NUM_ENTRIES_MAX (4096 /* bytes */ / FLASH_ROM_LOG_ENTRY_SIZE) /* entries */

SdCardWriteFileResponse sd_card_append_log_entries (LogEntry *entries, unsigned int num_entries, unsigned int *num_entries_appended) NONNULL (1, 3);

#endif /* !_SD_CARD_H */

/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>
#include <avr/wdt.h>

#include "button.h"
#include "common.h"
#include "sleep.h"

/**
 * \file
 * \brief Sleep control.
 *
 * Implementation of sleep states for the microcontroller, and their interaction with event handling. This allows the microcontroller to be put into
 * a deep sleep state for multiples of 8 seconds, being woken up by a timer or an external interrupt (such as the user pressing the UI button).
 */

/* Initialise the watchdog to generate a timer interrupt every 8s. */
static void _watchdog_init (void)
{
	cli (); /* disable interrupts */
	wdt_reset (); /* reset the watchdog */

	/* Start timed sequence */
	WDTCSR |= (1 << WDCE) | (1 << WDE);

	/* Set new prescaler (time-out) value = 1024K cycles (~8s); interrupts enabled */
	WDTCSR = (1 << WDIE) | (1 << WDP3) | (1 << WDP0);

	sei (); /* re-enable interrupts */
}

ISR (WDT_vect)
{
	/* Watchdog vector. Nothing to see here. */
	wdt_reset ();
}

void sleep_init (void)
{
	_watchdog_init ();
}

/* Sleep for ~8s (or until the UI button is pressed) in power-down mode. Return an identifier for the event which caused the wakeup.
 * This takes care of handling race conditions between interrupts (e.g. button presses) and going to sleep. It also disables the brown-out detector
 * before sleeping. See: http://www.nongnu.org/avr-libc/user-manual/group__avr__sleep.html */
static WakeupReason _sleep_8s (void)
{
	set_sleep_mode ((1 << SM1)); /* sleep in power-down mode */

	cli ();
	if (button_was_pressed ()) {
		/* The button was pressed. Stay awake and return. */
		sei ();
		return WAKEUP_BUTTON;
	}

	/* Reset the watchdog timer before going to sleep. This ensures our sleep period is actually something approaching 8s. */
	wdt_reset ();

	/* No button press. Atomically go to sleep and re-enable interrupts. This does admit the possibility of a button press waking us up,
	 * some processing being performed, and then _sleep_8s() being called again before the button has stopped bouncing, and thus a
	 * spurious wakeup occurring. We assume the processing will take more than ~10ms, and so ignore the possibility of spurious
	 * wakeups. Consequently, we re-enable the button interrupt before sleeping. */
	button_reset ();
	sleep_enable ();
	sleep_bod_disable ();

	/* Night night. This is effectively atomic because the instruction after an SEI instruction is always guaranteed to execute before
	 * interrupts start to be handled again. */
	sei ();
	sleep_cpu ();
	sleep_disable ();

	/* Check whether we woke up from a button press before returning. */
	if (button_was_pressed ()) {
		return WAKEUP_BUTTON;
	}

	return WAKEUP_TIMER;
}

/**
 * \brief Sleep until an event happens.
 *
 * Sleep for \c sleep_time seconds, or until the button is pressed. Sleeping is done in power-down mode to reduce power consumption as much as
 * possible. This handles race conditions between interrupts and putting the CPU to sleep.
 *
 * The cause for waking up is returned.
 */
WakeupReason sleep_until_event (uint8_t sleep_time)
{
	uint8_t i;

	if (sleep_time <= 8) {
		return _sleep_8s ();
	}

	/* Note: We only have granularity of 8s at the moment. This is good enough for what we need. */
	for (i = 0; i < sleep_time / 8; i++) {
		if (_sleep_8s () == WAKEUP_BUTTON) {
			return WAKEUP_BUTTON;
		}
	}

	return WAKEUP_TIMER;
}

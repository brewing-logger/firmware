/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <stdio.h>
#include <string.h>

#include <avr/io.h>
#include <avr/interrupt.h>

#include <util/delay_basic.h>
#include <util/delay.h>

#include "button.h"
#include "common.h"
#include "flash-rom.h"
#include "gas.h"
#include "heater.h"
#include "humidity.h"
#include "krausen-level.h"
#include "lcd.h"
#include "log.h"
#include "power.h"
#include "rtc.h"
#include "sd-card.h"
#include "sleep.h"
#include "spi.h"
#include "temperature.h"
#include "test.h"

/**
 * TODO
 *
 * Philip Withnall, 2012-11-02
 */

/* Intended for ATmega1284P */
/* Clock rate is 1.0 MHz  (8MHz internal oscillator (default), divide by 8 (default), to give CLKIO of 1MHz */

/* Current log entry. This is updated periodically by reading the sensors. It's static so that the most recent readings can be accessed by
 * _display_sensor_readings(). */
static LogEntry current_log_entry;

ISR (__vector_default) {
	/* Nothing to see here. */
}

/* TODO: How do we handle clearing the Flash ROM if a new brewing config is found? Perhaps put some metadata at the start of each sector saying which
 * brewing config that sector contains results for.
 * Same for the results file. */

/* Display a summary of the provided brewing config on the LCD.
 * The LCD has 24 columns and 2 rows, so the summary should fit into these constraints if possible.
 *
 * NOTE: The LCD power domain must have been turned on with lcd_power_set_state(LCD_POWER_ON) before this is called. */
static void _display_brew_config (BrewConfig *brew_config) NONNULL (1);

static void _display_brew_config (BrewConfig *brew_config)
{
	/* NOTE: This length must be kept up-to-date with the string below. */
	char row1_string[LCD_COLUMNS + 1 /* nul terminator */];
	char row2_string[LCD_COLUMNS + 1 /* nul terminator */];
	char date_string[11];

	DYNAMIC_ASSERT (lcd_power_get_state () == LCD_POWER_ON);

	/* Prepare the text for the rows. Only the first 10 characters are taken from the start_date, so that only the ISO 8601 date is shown, and
	 * not the time as well. We have to use memcpy() because the length modifier on format placeholders doesn't work in AVR's snprintf(). */
	memcpy (date_string, brew_config->start_date, 10);
	date_string[10] = '\0';

	snprintf (row1_string, sizeof (row1_string), "Brewing: %15s", brew_config->brew_name);
	snprintf (row2_string, sizeof (row2_string), "Start: %10s  %2u.%1uC", date_string,
	          TEMPERATURE_READING_TO_DEGREES_INT (brew_config->fermentation_temperature),
	          TEMPERATURE_READING_TO_DEGREES_FRAC (brew_config->fermentation_temperature));

	/* Display them on the LCD. */
	clear_lcd ();
	LCD_display_string (row1_string);
	row2_lcd ();
	LCD_display_string (row2_string);
}

/* Display a page summarising some of the sensor readings. Since there are a lot of sensors, they won't all fit on one page. The zero-based
 * screen_number chooses between pages. The sensor readings are taken from the most recent log entry (current_log_entry).
 *
 * NOTE: The LCD power domain must have been turned on with lcd_power_set_state(LCD_POWER_ON) before this is called. */
static void _display_sensor_readings (uint8_t screen_number)
{
	/* NOTE: This length must be kept up-to-date with the string below. */
	char row1_string[LCD_COLUMNS + 1 /* nul terminator */];
	char row2_string[LCD_COLUMNS + 1 /* nul terminator */];

	DYNAMIC_ASSERT (lcd_power_get_state () == LCD_POWER_ON);

	/* Prepare the text for the rows. */
	switch (screen_number) {
		case 0:
			snprintf (row1_string, sizeof (row1_string), "Temp.:     %2u.%1uC, %2u.%1uC ",
			          TEMPERATURE_READING_TO_DEGREES_INT (current_log_entry.temperature_reading[0]),
			          TEMPERATURE_READING_TO_DEGREES_FRAC (current_log_entry.temperature_reading[0]),
			          TEMPERATURE_READING_TO_DEGREES_INT (current_log_entry.temperature_reading[1]),
			          TEMPERATURE_READING_TO_DEGREES_FRAC (current_log_entry.temperature_reading[1]));
			snprintf (row2_string, sizeof (row2_string), "Humidity:  %2u.%1u%% (%2u.%1uC)",
			          HUMIDITY_READING_TO_PERCENT_INT (current_log_entry.humidity_reading.relative_humidity),
			          HUMIDITY_READING_TO_PERCENT_FRAC (current_log_entry.humidity_reading.relative_humidity),
			          TEMPERATURE_READING_TO_DEGREES_INT (current_log_entry.humidity_reading.temperature),
			          TEMPERATURE_READING_TO_DEGREES_FRAC (current_log_entry.humidity_reading.temperature));
			break;
		case 1:
			snprintf (row1_string, sizeof (row1_string), "Alcohol conc.: %2u.%1umg/l ",
			          GAS_READING_TO_MG_L_INT (current_log_entry.gas_reading), GAS_READING_TO_MG_L_FRAC (current_log_entry.gas_reading));
			snprintf (row2_string, sizeof (row2_string), "Krausen level: %2u.%1ucm   ",
			          KRAUSEN_LEVEL_READING_TO_CM_INT (current_log_entry.krausen_level_reading),
			          KRAUSEN_LEVEL_READING_TO_CM_INT (current_log_entry.krausen_level_reading));
			break;
		default:
			ASSERT_NOT_REACHED ();
	}

	/* Display them on the LCD. */
	clear_lcd ();
	LCD_display_string (row1_string);
	row2_lcd ();
	LCD_display_string (row2_string);
}

/* Display a message about the heater being disabled.
 *
 * NOTE: The LCD power domain must have been turned on with lcd_power_set_state(LCD_POWER_ON) before this is called. */
static void _display_heater_failure_message (void)
{
	DYNAMIC_ASSERT (lcd_power_get_state () == LCD_POWER_ON);

	clear_lcd ();
	LCD_display_string ("WARNING: Thermometer has");
	row2_lcd ();
	LCD_display_string ("failed. Heater disabled.");
}

/* Display an error message on the LCD and spin forever. This is for critical errors which cannot be recovered from without user intervention. Once
 * the user has intervened, they will restart the hardware.
 *
 * NOTE: The LCD power domain must have been turned on with lcd_power_set_state(LCD_POWER_ON) before this is called. */
static void _initialisation_error (const char *error_message_row1, const char *error_message_row2) NONNULL (1, 2) __attribute__((noreturn));

static void _initialisation_error (const char *error_message_row1, const char *error_message_row2)
{
	DYNAMIC_ASSERT (strlen (error_message_row1) <= LCD_COLUMN_BUFFERS);
	DYNAMIC_ASSERT (strlen (error_message_row2) <= LCD_COLUMN_BUFFERS);
	DYNAMIC_ASSERT (lcd_power_get_state () == LCD_POWER_ON);

	/* Display the error message. *//* TODO: Might need to implement scrolling or text wrapping for this. */
	clear_lcd ();
	LCD_display_string (error_message_row1);
	row2_lcd ();
	LCD_display_string (error_message_row2);

	/* Loop forever, scrolling if necessary. */
	if (strlen (error_message_row1) > LCD_COLUMNS || strlen (error_message_row2) > LCD_COLUMNS) {
		while (1) {
			_delay_ms (300);
			scroll_lcd_left ();
		}
	} else {
		while (1);
	}
}

/* Initialise the SD card and error out if initialisation fails. */
static void _sd_card_init (void)
{
	SdCardInitResponse init_response = sd_card_init ();

	switch (init_response) {
		case SD_CARD_INIT_SUCCESS:
			/* Successfully mounted the SD card. Continue. */
			break;
		case SD_CARD_INIT_NO_CARD_PRESENT:
			_initialisation_error ("No SD card found.",
			                       "Insert an SD card and restart.");
			break;
		case SD_CARD_INIT_CARD_WRITE_PROTECTED:
			_initialisation_error ("SD card is read-only.",
			                       "Allow writes on SD card and restart.");
			break;
		case SD_CARD_INIT_IO_ERROR:
		case SD_CARD_INIT_MISC_ERROR:
		default:
			_initialisation_error ("SD card could not be mounted.",
			                       "Please restart and try again.");
			break;
	}
}

/* Load the brewing configuration off the SD card and error out if that fails. */
static void _load_brew_config (BrewConfig *brew_config) NONNULL (1);

static void _load_brew_config (BrewConfig *brew_config)
{
	SdCardLoadFileResponse load_config_response;

	const BrewConfig default_brew_config = {
		.start_date = { '2', '0', '0', '0', '-', '0', '1', '-', '0', '1', 'T', '0', '0', ':', '0', '0', 'Z' },
		.brew_name = { 'd', 'e', 'f', 'a', 'u', 'l', 't', 0, },
		.fermentation_temperature = TEMPERATURE_READING_FROM_DEGREES (21, 0),
	};

	load_config_response = sd_card_load_brew_config (brew_config);

	switch (load_config_response) {
		case SD_CARD_LOAD_FILE_SUCCESS:
			/* Successfully loaded the config. Continue. */
			break;
		case SD_CARD_LOAD_FILE_NOT_EXIST:
			/* Configuration file doesn't exist. Create a default one and start logging anyway. If that fails, error out as before. */
			if (sd_card_create_brew_config (&default_brew_config) != SD_CARD_WRITE_FILE_SUCCESS) {
				_initialisation_error ("Configuration file not found on SD card.",
				                       "Create '" BREW_CONFIG_FILE_NAME "' and restart.");
			}

			/* Success! */
			memcpy (brew_config, &default_brew_config, sizeof (*brew_config));

			break;
		case SD_CARD_LOAD_FILE_INVALID_PERMISSIONS:
			/* Invalid permissions on the config file. Error out. */
			_initialisation_error ("Configuration file could not be read from the SD card.",
			                       "Check the permissions of '" BREW_CONFIG_FILE_NAME "' and restart.");
			break;
		case SD_CARD_LOAD_FILE_UNSUPPORTED_VERSION:
			/* Unsupported version (higher version number than is supported by this embedded software) of the configuration file. */
			_initialisation_error ("Configuration file is too recent to be supported by this version of the brewing logger.",
			                       "Re-create '" BREW_CONFIG_FILE_NAME "' with an older version number and restart.");
			break;
		case SD_CARD_LOAD_FILE_CHECKSUM_FAILURE:
			/* Brew config file checksum didn't match the loaded data. */
			_initialisation_error ("Configuration file is corrupted.",
			                       "Re-create '" BREW_CONFIG_FILE_NAME "' and restart.");
			break;
		case SD_CARD_LOAD_FILE_IO_ERROR:
		case SD_CARD_LOAD_FILE_MISC_ERROR:
		default:
			/* Unknown error when loading the configuration file. Run away. */
			_initialisation_error ("Configuration file '" BREW_CONFIG_FILE_NAME "' could not be read from the SD card.",
			                       "Please restart and try again.");
			break;
	}
}

/* Update current_log_entry with sensor readings from all the sensors.
 *
 * NOTE: The sensor power domain must have been turned on with sensor_power_set_state(SENSOR_POWER_ON) before this is called. */
static void _update_sensor_readings (void)
{
	MeasurementResponse response;
	TemperatureReading gas_sensor_temperature;
	HumidityReading gas_sensor_humidity;

	DYNAMIC_ASSERT (sensor_power_get_state () == SENSOR_POWER_ON);

	/* If measurements fail, anull them in the log entry, but keep measuring other things. */
#define HANDLE_MEASUREMENT_RESPONSE(measurement, default_value) \
	switch (response) { \
		case MEASUREMENT_SUCCESS: \
			/* Success. */ \
			break; \
		case MEASUREMENT_CHECKSUM_FAILURE: \
		case MEASUREMENT_INVALID: \
		case MEASUREMENT_TIMEOUT: \
		default: \
			/* Failure. Anull the entry, but keep measuring other things. */ \
			(measurement) = (default_value); \
			break; \
	}

	/* Fill out the log entry. */
	current_log_entry.time = get_fattime ();

	/* Humidity. */
	response = humidity_take_measurement (&current_log_entry.humidity_reading);
	HANDLE_MEASUREMENT_RESPONSE (current_log_entry.humidity_reading.relative_humidity, HUMIDITY_READING_INVALID)
	HANDLE_MEASUREMENT_RESPONSE (current_log_entry.humidity_reading.temperature, TEMPERATURE_READING_INVALID)

	gas_sensor_humidity = current_log_entry.humidity_reading.relative_humidity;
	HANDLE_MEASUREMENT_RESPONSE (gas_sensor_humidity, GAS_READING_DEFAULT_HUMIDITY)

	/* Temperature. */
	response = temperature_take_measurement (current_log_entry.temperature_reading);
	HANDLE_MEASUREMENT_RESPONSE (current_log_entry.temperature_reading[0], TEMPERATURE_READING_INVALID)
	HANDLE_MEASUREMENT_RESPONSE (current_log_entry.temperature_reading[1], TEMPERATURE_READING_INVALID)

	gas_sensor_temperature = current_log_entry.temperature_reading[0];
	HANDLE_MEASUREMENT_RESPONSE (gas_sensor_temperature, GAS_READING_DEFAULT_TEMPERATURE)

	/* Krausen level. */
	response = krausen_level_take_measurement (&current_log_entry.krausen_level_reading);
	HANDLE_MEASUREMENT_RESPONSE (current_log_entry.krausen_level_reading, KRAUSEN_LEVEL_READING_INVALID)

	/* Alcohol gas level. We have to provide a temperature and relative humidity in order to compensate for the sensor measurements. If either of
	 * the temperature/humidity readings are invalid, provide defaults instead (better than nothing, and just about acceptable). In the worst
	 * case, the use of a default temperature/humidity value will cause an absolute error of 0.10 in the adjusted R_s / R_o ratio used to
	 * calculate the gas concentration. This can cause an error of up to 7mg/l in the gas concentration. However, this would be flagged by the
	 * fact that one or other of the temperature and humidity measurements were marked as invalid.
	 *
	 * Note that this assumes that temperature sensor 0 is colocated with the gas concentration sensor. */
	response = gas_take_measurement (&current_log_entry.gas_reading, gas_sensor_temperature, gas_sensor_humidity);
	HANDLE_MEASUREMENT_RESPONSE (current_log_entry.gas_reading, GAS_READING_INVALID)

#undef HANDLE_MEASUREMENT_RESPONSE
}

/* Record sensor readings from all the sensors.
 *
 * NOTE: The sensor power domain must have been turned on with sensor_power_set_state(SENSOR_POWER_ON) before this is called. The flash memory must
 * also have been put into the standby state using flash_rom_power_up(). */
static void _record_sensor_readings (void)
{
	_update_sensor_readings ();

	/* Write the log entry to Flash ROM. */
	flash_rom_append_entry (&current_log_entry);
}

int main (void)
{
	unsigned int flash_rom_flush_counter = 0;
	enum {
		HEATER_ENABLED,
		HEATER_DISABLED,
	} heater_mode = HEATER_ENABLED;
	uint8_t temperature_reading_invalid_counter = 0; /* number of consecutive times the temperature reading has been invalid */

#if defined(TEST_MODE) && TEST_MODE
	test ();

	while (1);
#endif /* TEST_MODE */

	/* I/O port initialisation. This needs to be done early so that the ports for the LCD are set up. The LCD is needed to display error messages
	 * to the user. */
	lcd_init ();
	spi_init ();
	power_init ();

	/* Initialisation message. */
	lcd_power_set_state (LCD_POWER_ON);
	clear_lcd ();
	LCD_display_string ("Initialising...");

	/* Set up the flash memory ready to receive sensor logs. Do this early because it could take a long time (if it has to scan the entire flash
	 * memory). Once that's done, put it into a deep power-down state */
	spi_enable ();
	flash_rom_init ();
	flash_rom_power_down ();
	spi_disable ();

	/* Display a loading message to the user. */
	clear_lcd ();
	LCD_display_string ("Loading brewing");
	row2_lcd ();
	LCD_display_string ("configuration file...");

	/* Load the file system and read the brewing configuration file off the SD card. */
	spi_enable ();
	_sd_card_init ();
	_load_brew_config (&global_brew_config);
	spi_disable ();

	/* The brewing configuration has been successfully loaded. Display some of the configuration data to the user. */
	_display_brew_config (&global_brew_config);

	/* Set up the watchdog timer which will wake the MCU up to take sensor readings every 8s. */
	sleep_init ();

	/* Initialise the sensors and other devices. */
	humidity_init ();
	temperature_init ();
	krausen_level_init ();
	gas_init ();
	rtc_init ();
	button_init ();
	heater_init ();

	/* Globally enable interrupts so sensor readings can be taken. */
	SREG = (1 << 7);

	/* Take some initial sensor readings so that if the user presses the UI button during the first sleep, we have something useful to show. */
	sensor_power_set_state (SENSOR_POWER_ON);
	_update_sensor_readings ();
	sensor_power_set_state (SENSOR_POWER_OFF);

	/* Main event loop. Sleep for 30s, then wake up and take sensor readings. Record them to Flash ROM. Occasionally, also flush the Flash ROM
	 * to the SD card. Only two things can wake the MCU up from sleep_until_event(): a watchdog timer interrupt (every 30s), or a button press
	 * interrupt (which could happen at any time). In the latter case, we don't log any entries, and display the UI instead.
	 *
	 * In the first few cycles, the configuration message displayed on the LCD will be cleared and the LCD powered down. After that, the LCD will
	 * only display messages in response to a button press. */
	while (1) {
		TemperatureReading current_temperature;
		enum {
			SCREEN_BREW_CONFIG = 0,
			SCREEN_HEATER_FAILURE = 1,
			SCREEN_SENSOR_READINGS1 = 2,
			SCREEN_SENSOR_READINGS2 = 3,
		} screen_number = SCREEN_HEATER_FAILURE; /* since we show the brew config to begin with */
		#define MAX_SCREEN_NUMBER (SCREEN_SENSOR_READINGS2)

		/* Sleep for ~30s. */
		WakeupReason wakeup_reason = sleep_until_event (32 /* seconds */);

		/* Did we wake up as a result of a button press or a watchdog interrupt? If a button, display the brew config for 8s, then clear the
		 * screen and go back to logging. If the button is pressed while the screen is active, move to the next screen. Note that no new
		 * sensor readings are taken while showing the screen. */
		while (wakeup_reason == WAKEUP_BUTTON) {
			lcd_power_set_state (LCD_POWER_ON);

			switch (screen_number) {
				case SCREEN_BREW_CONFIG:
					_display_brew_config (&global_brew_config);
					break;
				case SCREEN_HEATER_FAILURE:
					/* Only display the heater failure message if the heater has failed. Otherwise, fall through. */
					if (heater_mode == HEATER_DISABLED) {
						_display_heater_failure_message ();
						break;
					}
					screen_number = SCREEN_SENSOR_READINGS1;
					/* Fall through. */
				case SCREEN_SENSOR_READINGS1:
				case SCREEN_SENSOR_READINGS2:
					_display_sensor_readings (screen_number - SCREEN_SENSOR_READINGS1);
					break;
				default:
					ASSERT_NOT_REACHED ();
			}

			screen_number = (screen_number + 1) % (MAX_SCREEN_NUMBER + 1);

			/* Sleep for ~8s. */
			wakeup_reason = sleep_until_event (8 /* seconds */);
		}

		/* Turn the LCD off (and consequently clear its message). */
		lcd_power_set_state (LCD_POWER_OFF);

		/* Power up the flash memory. */
		spi_enable ();
		flash_rom_power_up ();

		/* Take sensor readings and write them to Flash ROM. */
		sensor_power_set_state (SENSOR_POWER_ON);
		_record_sensor_readings ();
		sensor_power_set_state (SENSOR_POWER_OFF);

		/* Update the heater controller and turn the heater on/off as appropriate. This gives a minimum system settling time of
		 * [10, 100] * 30s = [300, 3000]s, which is acceptable. We don't expect large variations in the external environment temperature.
		 * See 'Sampling Rate' in http://www.embedded.com/design/prototyping-and-development/4211211/PID-without-a-PhD
		 *
		 * Take the maximum of the two temperature readings as the current temperature. If the temperature sensor fails, it will most likely
		 * either go open circuit (0 reading) or closed circuit (UINT16_MAX reading). In the latter case, this will measure over 150 degrees
		 * Celsius, so the temperature reading will be TEMPERATURE_READING_INVALID. In the former case, try and detect sensor failure (this is
		 * done in _record_sensor_readings()) and go into failure mode with the heater turned off. This is an attempt to prevent the system
		 * erroneously keeping the heater on continuously and thus boiling the beer. That would, arguably, be quite bad. */
		if (current_log_entry.temperature_reading[0] == TEMPERATURE_READING_INVALID &&
		    current_log_entry.temperature_reading[1] == TEMPERATURE_READING_INVALID &&
		    ++temperature_reading_invalid_counter >= 3) {
			/* Failure mode. Disable the heater until the logger is restarted (hopefully, with the fault fixed). */
			heater_mode = HEATER_DISABLED;
			screen_number = SCREEN_HEATER_FAILURE;
			temperature_reading_invalid_counter = 3; /* pin it */
		} else if (current_log_entry.temperature_reading[0] == TEMPERATURE_READING_INVALID ||
		           current_log_entry.temperature_reading[1] > current_log_entry.temperature_reading[0]) {
			current_temperature = current_log_entry.temperature_reading[1];
			temperature_reading_invalid_counter = 0;
		} else {
			current_temperature = current_log_entry.temperature_reading[0];
			temperature_reading_invalid_counter = 0;
		}

		switch (heater_mode) {
			case HEATER_ENABLED:
				heater_set_state (heater_update_controller (global_brew_config.fermentation_temperature, current_temperature));
				break;
			case HEATER_DISABLED:
				heater_set_state (HEATER_OFF);
				break;
			default:
				ASSERT_NOT_REACHED ();
		}

		/* Flush Flash ROM every so often. */
		if (flash_rom_flush_counter++ >= FLASH_ROM_FLUSH_PERIOD / 30) {
			FlashRomFlushResponse response;

			flash_rom_flush_counter = 0;
			response = flash_rom_flush_to_sd_card ();

			switch (response) {
				case FLASH_ROM_FLUSH_SUCCESS:
					/* Yay. Continue as before. */
					break;
				case FLASH_ROM_FLUSH_SD_CARD_ERROR:
				default:
					/* Error from the SD card part-way through flushing. Ignore it and hope it doesn't happen again.
					 * TODO: Perhaps notify the user somehow? */
					break;
			}
		}

		/* Power down the flash memory again. */
		flash_rom_power_down ();
		spi_disable ();
	}
}

/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include "button.h"
#include "flash-rom.h"
#include "gas.h"
#include "heater.h"
#include "humidity.h"
#include "krausen-level.h"
#include "lcd.h"
#include "log.h"
#include "power.h"
#include "rtc.h"
#include "sleep.h"
#include "spi.h"
#include "test.h"
#include "temperature.h"

/* NOTE: Keep me up to date! */
#define NUM_TESTS 11

static void _display_test_result (uint8_t test_number, const char *message_format, ...) __attribute__((format (printf, 2, 3))) NONNULL (2);

static void _display_test_result (uint8_t test_number, const char *message_format, ...)
{
	char message[LCD_COLUMNS + 1 /* nul */];
	va_list args;

	/* Format the message. */
	va_start (args, message_format);
	vsnprintf (message, sizeof (message) / sizeof (*message), message_format, args);
	va_end (args);

	/* Display some metadata. */
	clear_lcd ();
	LCD_display_string ("Test ");
	LCD_display_uint8 (test_number);
	LCD_display_string ("/");
	LCD_display_uint8 (NUM_TESTS);
	LCD_display_string (":");

	/* Display the message. */
	row2_lcd ();
	LCD_display_string (message);

	_delay_ms (500);
}

static void _test_lcd (void)
{
	/* Initialise the LCD and display some text. */
	lcd_init ();

	_display_test_result (1, "The quick brown fox...");
}

/* Simple test for the RTC. */
static void _test_rtc (void)
{
	/* Reference: ds3231.h. */
	uint8_t new_time[8] = { 0x00, 12 /* seconds */, 27 /* minutes */, 15 /* hours */,
	                        5 /* day of week */, 23 /* day */, 11 /* month */, 12 /* year */ };

	/* Set up the RTC and get the current time. */
	rtc_init ();

	/* Set the time. */
	_display_test_result (2, "Setting time: %u", write_to_rtc (new_time));

	/* Print out the time a few times. */
	_display_test_result (3, "Time 1: %lu", get_fattime ());
	_display_test_result (3, "Time 2: %lu", get_fattime ());
	_display_test_result (3, "Time 3: %lu", get_fattime ());
	_display_test_result (3, "Time 4: %lu", get_fattime ());
	_display_test_result (3, "Time 5: %lu", get_fattime ());
}

static void _test_humidity (void)
{
	MeasurementResponse response;
	HumidityTemperatureReading reading;

	/* Initialise the humidity sensor. */
	humidity_init ();

	/* Take a humidity measurement. */
	response = humidity_take_measurement (&reading);
	_display_test_result (4, "Humidity: %u, %3u.%1u%%, %2u.%1uC", response,
	                      HUMIDITY_READING_TO_PERCENT_INT (reading.relative_humidity),
	                      HUMIDITY_READING_TO_PERCENT_FRAC (reading.relative_humidity),
	                      TEMPERATURE_READING_TO_DEGREES_INT (reading.temperature), TEMPERATURE_READING_TO_DEGREES_FRAC (reading.temperature));
}

static void _test_temperature (void)
{
	MeasurementResponse response;
	TemperatureReading reading[NUM_THERMOMETERS];

	/* Initialise the thermometers. */
	temperature_init ();

	/* Take a temperature measurement. */
	response = temperature_take_measurement (reading);
	_display_test_result (5, "Temperatures: %u, %u.%uC, %u.%uC", response,
	                      TEMPERATURE_READING_TO_DEGREES_INT (reading[0]), TEMPERATURE_READING_TO_DEGREES_FRAC (reading[0]),
	                      TEMPERATURE_READING_TO_DEGREES_INT (reading[1]), TEMPERATURE_READING_TO_DEGREES_FRAC (reading[1]));
}

static void _test_krausen_level (void)
{
	MeasurementResponse response;
	KrausenLevelReading reading;

	/* Initialise the proximity sensor. */
	krausen_level_init ();

	/* Take a krausen level measurement. */
	response = krausen_level_take_measurement (&reading);
	_display_test_result (6, "Krausen level: %u, %u.%ucm", response,
	                      KRAUSEN_LEVEL_READING_TO_CM_INT (reading), KRAUSEN_LEVEL_READING_TO_CM_FRAC (reading));
}

static void _test_gas (void)
{
	MeasurementResponse response;
	GasReading reading;

	/* Initialise the gas concentration sensor. */
	gas_init ();

	/* Take a gas concentration measurement. */
	response = gas_take_measurement (&reading, GAS_READING_DEFAULT_TEMPERATURE, GAS_READING_DEFAULT_HUMIDITY);
	_display_test_result (7, "Gas conc.: %u, %u.%02u mg/l", response, GAS_READING_TO_MG_L_INT (reading), GAS_READING_TO_MG_L_FRAC (reading));

	/* Try turning the heater on and off. (The LED on the board should light up as appropriate.) */
	gas_heater_on ();
	_display_test_result (7, "Gas heater on.");

	gas_heater_off ();
	_display_test_result (7, "Gas heater off.");
}

static void _test_button (void)
{
	WakeupReason response;

	/* Initialise the button and watchdog timer. */
	sleep_init ();
	button_init ();

	/* Wait for the button to be pressed. */
	_display_test_result (8, "Please press button.");
	response = sleep_until_event (8 /* seconds */);

	/* Why did we wake up? */
	switch (response) {
		case WAKEUP_TIMER:
			_display_test_result (8, "Button not pressed.");
			break;
		case WAKEUP_BUTTON:
			_display_test_result (8, "Button pressed!");
			break;
		default:
			ASSERT_NOT_REACHED ();
	}
}

static void _test_flash_rom (void)
{
	LogEntry log_entry, log_entry_copy;

	/* Initialise SPI and the Flash ROM. Display a message because this takes a while. */
	_display_test_result (9, "Initialising flash...");

	spi_init ();
	flash_rom_init ();

	/* Erase all the sectors. */
	_display_test_result (9, "Erasing flash...");
	flash_rom_erase ();

	/* Try appending an entry to the Flash ROM. The values used are chosen arbitrarily. */
	log_entry.time = 1234;
	log_entry.humidity_reading.relative_humidity = 0xfcde;
	log_entry.humidity_reading.temperature = 0xfedc;
	log_entry.temperature_reading[0] = 0x8000;
	log_entry.temperature_reading[1] = 0x8001;
	log_entry.krausen_level_reading = 0x54;
	log_entry.gas_reading = 0x6994;

	flash_rom_append_entry (&log_entry);

	/* Try reading the entry back and compare the two. */
	switch (flash_rom_read_head_entry (&log_entry_copy)) {
		case FLASH_ROM_READ_ENTRY_CHECKSUM_FAILURE:
			_display_test_result (9, "Checksum failure.");
			break;
		case FLASH_ROM_READ_ENTRY_SUCCESS:
			if (memcmp (&log_entry, &log_entry_copy, sizeof (log_entry)) == 0) {
				_display_test_result (9, "Entries matched!");
			} else {
				_display_test_result (9, "Entries did not match.");
			}
			break;
		default:
			ASSERT_NOT_REACHED ();
	}

	/* Try overwriting the checksum and check the entries no longer match (because read_head_entry() will zero out the entry on a checksum
	 * failure). */
	flash_rom_overwrite_head_entry_checksum ();

	switch (flash_rom_read_head_entry (&log_entry_copy)) {
		case FLASH_ROM_READ_ENTRY_SUCCESS:
			_display_test_result (10, "Overwrite failure.");
			break;
		case FLASH_ROM_READ_ENTRY_CHECKSUM_FAILURE:
			_display_test_result (10, "Overwrite success!");
			break;
		default:
			ASSERT_NOT_REACHED ();
	}
}

static void _test_heater (void)
{
	/* Initialise the heater. */
	heater_init ();

	/* Try turning the heater on and off. (The LED on the board should light up as appropriate.) */
	heater_set_state (HEATER_ON);
	_display_test_result (11, "Heater on.");

	heater_set_state (HEATER_OFF);
	_display_test_result (11, "Heater off.");
}

/* Run all the tests. */
void test (void)
{
	/* Initialise power management so the sensors can be turned on. */
	power_init ();
	sensor_power_set_state (SENSOR_POWER_ON);

	/* Globally enable interrupts so sensor readings can be taken. */
	SREG = (1 << 7);

	_test_lcd ();
	_test_rtc ();
	_test_humidity ();
	_test_temperature ();
	_test_krausen_level ();
	_test_gas ();
	_test_button ();
	_test_flash_rom ();
	_test_heater ();
}

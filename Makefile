#
# Main code
#

MAIN_SOURCES = \
	adc.c \
	button.c \
	flash-rom.c \
	gas.c \
	heater.c \
	humidity.c \
	krausen-level.c \
	lcd.c \
	main.c \
	power.c \
	rtc.c \
	sd-card.c \
	sleep.c \
	spi.c \
	temperature.c \
	test.c \
	$(NULL)

LIB_SOURCES = \
	lib/diskio.c \
	lib/ds3231.c \
	lib/ff.c \
	lib/i2c.c \
	lib/serial_lcd.c \
	$(NULL)

CFLAGS = \
	-Wdeclaration-after-statement -Wextra -Wformat=2 -Winit-self -Winline -Wpacked -Wpointer-arith -Wlarger-than-65500 -Wmissing-declarations \
	-Wmissing-format-attribute -Wmissing-noreturn -Wmissing-prototypes -Wnested-externs -Wold-style-definition -Wsign-compare \
	-Wstrict-aliasing=2 -Wstrict-prototypes -Wswitch-enum -Wundef -Wunsafe-loop-optimizations -Wwrite-strings -Wno-missing-field-initializers \
	-Wno-unused-parameter -Wshadow -Wcast-align -Wformat-nonliteral -Wformat-security -Wswitch-default -Wmissing-include-dirs \
	-Waggregate-return -Wredundant-decls -Warray-bounds -Wunreachable-code -ggdb
# CFLAGS which aren't supported on the lab machines
# CFLAGS += \
#	-Wunused-but-set-variable
CPPFLAGS = -Ilib

main.elf: $(MAIN_SOURCES:.c=.o) $(LIB_SOURCES:.c=.o)

all: main.hex
program-main: main.hex
	$(call program,$<)
.PHONY: program-main all

clean-main:
	rm -f $(MAIN_SOURCES:.c=.o) $(LIB_SOURCES:.c=.o) main.elf main.hex
.PHONY: clean-main


#
# Documentation
#
empty:=
space:= $(empty) $(empty)

docs: doxygen.conf
	doxygen $<
	@echo "Documentation generated as:"
	@echo "	file://$(subst $(space),%20,$(abspath docs))/html/index.html"
.PHONY: docs

clean-docs:
	rm -rf docs
.PHONY: clean-docs


#
# Simulation using simavr
#
# If you want to debug it with gdb, run:
#  > ./simulation/main main.elf 1234
#  > avr-gdb main.elf
#  > > target remote :1234
#
simulate: main.elf
	PKG_CONFIG_PATH=$$PKG_CONFIG_PATH:/home/philip/bin/simavr/lib/pkgconfig/ $(MAKE) -C simulation main
	LD_LIBRARY_PATH=$$LD_LIBRARY_PATH:/home/philip/bin/simavr/lib/ ./simulation/main main.elf 1234 sdcard.img
.PHONY: simulate


#
# SD card support
#
# Mount/Unmount the SD card image used by the simulator as a read-only loopback file system. This allows it to be inspected while the simulation is
# running.
#
mount-sd-card:
	mkdir /tmp/fatfs
	sudo losetup /dev/loop0 sdcard.img
	sudo mount -o ro /dev/loop0 /tmp/fatfs
umount-sd-card:
	sudo umount /dev/loop0
	rmdir /tmp/fatfs
	sudo losetup -d /dev/loop0
.PHONY: mount-sd-card umount-sd-card


#
# Fuses
#
# These are the default fuses, which result in using an 8MHz internal oscillator with no CLKOUT signal
# Note that some fuse bits (e.g. clock out) are active low
#
fuses:
	avrdude -p $(AVRDUDE_MCU) -P $(AVRDUDE_PROGRAMMER_DEVNODE) -c $(AVRDUDE_PROGRAMMER) -e -U hfuse:w:0xd9:m
	avrdude -p $(AVRDUDE_MCU) -P $(AVRDUDE_PROGRAMMER_DEVNODE) -c $(AVRDUDE_PROGRAMMER) -e -U lfuse:w:0x62:m


#
# Clean
#

clean: clean-docs clean-main
.PHONY: clean

#
# Common
#

CPPFLAGS=-I. -I./lib
MCU=atmega1284p
AVRDUDE_MCU=m1284p # see http://www.nongnu.org/avrdude/user-manual/avrdude_4.html#Option-Descriptions
AVRDUDE_PROGRAMMER=avrusb500 # USB FTDI device
AVRDUDE_PROGRAMMER_DEVNODE=/dev/ttyUSB0

# Common rules for compiling programs for the MCU.
%.o: %.c
	@avr-gcc ${CPPFLAGS} -Wall -Os ${CFLAGS} -mmcu=${MCU} -o $@ -c $^

%.elf: %.o
	avr-gcc -Os -mmcu=${MCU} -o $@ $^

%.hex: %.elf
	avr-objcopy -j .text -j .data -O ihex $^ $@

%.lst: %.elf
	avr-objdump -h -S $^ > $@

# Function to program the device with the hex file given in $1.
program = avrdude -p ${AVRDUDE_MCU} -P ${AVRDUDE_PROGRAMMER_DEVNODE} -c ${AVRDUDE_PROGRAMMER} -e -U flash:w:$1

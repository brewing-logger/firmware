/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include <avr/io.h>
#include <util/crc16.h>
#include <util/delay.h>

#include "flash-rom.h"
#include "sd-card.h"
#include "spi.h"

/**
 * \file
 * \brief Flash memory control.
 *
 * Interaction with the flash memory chip, implementing a ring buffer for LogEntrys on it. This code is tightly tied to the M25P16 flash memory
 * chip \cite m25p16. The design of the ring buffer is such that the buffer will never be left in an inconsistent state. Once a log entry has been
 * successfully appended to the flash memory, it will never be lost; but may be duplicated if power is lost part-way through flushing the ring buffer
 * to the SD card. Some of the ideas for flash memory safety in the event of a power loss were inspired by \cite tseng2011understanding.
 *
 * All fields in data structures stored on the flash memory are encoded in little-endian format. e.g. A 16-bit field with value \c 0x1234 is encoded
 * as bytes \c 0x12 and \c 0x34, with \c 0x12 being placed at the _higher_ address.
 *
 * \todo TODO: What if a checksum byte is legitimately 0x00 or 0xff?
 *
 * Logging design
 * ==============
 *
 *  - Log entries are appended to the flash memory as they're taken. Each append is done atomically so that the contents of flash memory are
 *    guaranteed uncorrupted even if power fails half-way through.
 *  - This is achieved by writing a checksum at the end of each entry and ensuring that the checksum can never be \c 0xff or \c 0x00. This is because
 *    bytes can only be set to \c 0xff by erasing the sector (and not writing to that byte again). By erasing at least one entry ahead of the one
 *    we're writing, we can guarantee that the content after the current entry is all 1s. If checksums can't be \c 0xff, we can detect the end of the
 *    valid entries. Similarly, if a checksum is \c 0x00, we know that it was valid, but has now been copied to the SD card.
 *  - The flash memory essentially acts as a persistent ring buffer for entries before they're copied to the SD card. No pointers to the head/tail of
 *    the buffer are kept in the flash memory because that would cost an entire sector, and wear out that sector rapidly due to erasing it for every
 *    entry. Those pointers could also become stale if power is lost between updating them and updating the data.
 *  - As a general principle, the validity of checksums is reported to the caller, but entries with invalid checksums are not dropped or ignored. As
 *    long as the caller knows they're invalid, the caller can do something sensible with them.
 *
 * Initialisation
 * --------------
 *
 *    1. Scan through the flash memory to detect the first and last entries in the memory. These are the entries with non-\c 0xff checksums (even if
 *       those checksums are invalid). If the 0th entry in the flash memory is valid, make sure to check the last entry in the memory as well, since
 *       the ring buffer might have its head at a higher address than its tail. Store the head and tail addresses in RAM.
 *
 * _Fail safety_: This performs no writes, so is fail-safe.
 *
 * Appending an entry
 * ------------------
 *
 *    1. If at the end of a sector, erase the next sector. Before doing so, check whether that sector needs flushing to the SD card first.
 *    2. Write out the log entry, followed by its checksum.
 *    3. Advance the tail pointer in memory.
 *
 * _Fail safety_: If power is lost during the erase in step 1, the next sector will be left in an indeterminate state. There should be no valid
 * entries on it, as their checksums will be wrong. This could result in at most one bogus entry appearing in the sector, if the operation fails
 * part-way through erasing the first entry on the sector, leaving its checksum as neither \c 0x00 or \c 0xff. This is acceptable.
 *
 * If power is lost while writing out the new log entry, the entry never gets marked as valid as its checksum is still \c 0xff. If power is lost while
 * writing out the checksum, the same situation as described above occurs, and at most one bogus entry appears.
 *
 * Flushing flash memory
 * ---------------------
 *
 *    1. Read an entry from the head of the flash memory into RAM.
 *    2. Write that entry to the SD card.
 *    3. Overwrite its checksum with \c 0x00. This marks the entry as invalid without requiring the entire sector to be erased (which would erase
 *       other entries).
 *    4. Advance the head pointer in RAM. There is no need to erase a sector once it's no longer being used by the ring buffer, as this is done on
 *       writing an entry anyway.
 *
 * _Fail safety_: If power is lost while overwriting the checksum, that entry becomes a bogus entry at the head of the flash memory as described
 * above.
 *
 * Constraints of flash memory
 * ===========================
 *
 *  - Any operation may fail and leave the memory in an intermediate state. This is because both program and erase operations are typically
 *    implemented bit-by-bit, pumping or releasing charge from a storage cell until the desired voltage is reached \cite tseng2011understanding.
 *  - Erase operations are expensive; a factor of 10 more expensive than write operations. Write operations are themselves a factor of 10 more
 *    expensive than read operations.
 *  - We assume that writes are performed sequentially in ascending address order.
 */

/**
 * \brief Checksum for a log entry.
 *
 * The checksum used for a log entry when stored on the flash memory. It is calculated over all the fields of the LogEntry. A CRC-8 checksum is used,
 * so all burst errors of 8 bits (or shorter) will be detected, as will longer burst errors with probability \f$1 - 2^{-8}\f$.
 */
typedef uint8_t LogEntryChecksum;

/* NOTE: This evaluates C multiple times. */
#define LOG_ENTRY_CHECKSUM_IS_OVERWRITTEN(C) ((C) == 0x00 || (C) == 0xff)

/**
 * \brief Flash memory address.
 *
 * The address of a byte on the flash memory. Addresses start at 0 and end at \f$2^{24} - 1\f$. Only the least significant 24 bits of a
 * FlashRomAddress are used; the values of the more significant bits are undefined.
 */
typedef uint32_t FlashRomAddress;

/**
 * \brief Flash memory length.
 *
 * A length (in bytes) of something such as an array of data which is being read off or written onto the flash memory. This should be considered as a
 * version of \c offset_t.
 *
 * As with FlashRomAddress, only the least significant 24 bits of a FlashRomOffset are used; the values of the more significant bits are undefined.
 */
typedef uint32_t FlashRomOffset;

#define FLASH_ROM_PAGE_SIZE ((FlashRomAddress) 256) /* bytes */
#define FLASH_ROM_SECTOR_SIZE ((FlashRomAddress) (256 * FLASH_ROM_PAGE_SIZE)) /* bytes */
#define FLASH_ROM_SIZE ((FlashRomAddress) (32 * FLASH_ROM_SECTOR_SIZE)) /* bytes */
#define FLASH_ROM_ADDRESS_IS_SECTOR_ALIGNED(A) (((A) % FLASH_ROM_SECTOR_SIZE) == 0)
/* NOTE: This evaluates A and S multiple times. */
#define FLASH_ROM_ADDRESS_IS_IN_SECTOR(A, S) ((A) >= (S) && (A) < (S) + FLASH_ROM_SECTOR_SIZE)
#define FLASH_ROM_IS_EMPTY() (flash_rom_head == flash_rom_tail)

/* NOTE: This must be an even divisor of the Flash ROM page size (and hence also of the sector size). (Checked by a static assertion below.) */
#define FLASH_ROM_LOG_ENTRY_SIZE 32 /* bytes */

/**
 * \brief Storage layout of a log entry.
 *
 * The storage layout of a LogEntry, as it's stored on flash memory. This includes padding to make streaming reads off flash memory easier to handle;
 * it's assumed that this data structure will only exist temporarily as a way of getting LogEntrys on/off flash memory.
 */
typedef struct {
	union {
		struct {
			LogEntry entry;
			BrewConfigId brew_id;
			LogEntryChecksum checksum;
		};
		uint8_t data[sizeof (LogEntry) + sizeof (BrewConfigId) + sizeof (LogEntryChecksum)];
	};
	uint8_t padding[FLASH_ROM_LOG_ENTRY_SIZE - (sizeof (LogEntry) + sizeof (BrewConfigId) + sizeof (LogEntryChecksum))];	/**< Padding. Do not use. */
} FlashRomLogEntry;

STATIC_ASSERT (FLASH_ROM_LOG_ENTRY_SIZE >= sizeof (FlashRomLogEntry));
STATIC_ASSERT ((FLASH_ROM_PAGE_SIZE % FLASH_ROM_LOG_ENTRY_SIZE) == 0);

/* Calculate a checksum for the given log entry. The Dallas/Maxim CRC-8 from avr-libc is used.
 * It uses polynomial: x^8 + x^5 + x^4 + 1 with initial value 0x00.
 * See: http://www.nongnu.org/avr-libc/user-manual/group__util__crc.html#ga37b2f691ebbd917e36e40b096f78d996 */
static LogEntryChecksum _log_entry_calculate_checksum (const LogEntry *entry, const BrewConfigId brew_id) NONNULL (1);

static LogEntryChecksum _log_entry_calculate_checksum (const LogEntry *entry, const BrewConfigId brew_id)
{
	uint8_t crc = 0, i;

	crc = _crc_ibutton_update (crc, brew_id);
	for (i = 0; i < sizeof (LogEntry); i++) {
		crc = _crc_ibutton_update (crc, ((const uint8_t *) entry)[i]);
	}

	return crc;
}

/* Instruction set of the M25PXX Flash ROM. See: Section 6 of the M25P16 datasheet. */
typedef enum {
	INSTRUCTION_WREN = 0x06, /**< Write Enable */
	INSTRUCTION_WRDI = 0x04, /**< Write Disable */
	INSTRUCTION_RDID = 0x9F, /**< Read Identification */
	INSTRUCTION_RDSR = 0x05, /**< Read Status Register */
	INSTRUCTION_WRSR = 0x01, /**< Write Status Register */
	INSTRUCTION_READ = 0x03, /**< Read Data Bytes */
	INSTRUCTION_FAST_READ = 0x0B, /**< Read Data Bytes at Higher Speed */
	INSTRUCTION_PP = 0x02, /**< Page Program */
	INSTRUCTION_SE = 0xD8, /**< Sector Erase */
	INSTRUCTION_BE = 0xC7, /**< Bulk-Erase */
	INSTRUCTION_DP = 0xB9, /**< Deep Power-down */
	INSTRUCTION_RES = 0xAB, /**< Release from Deep Power-down */
} FlashRomInstruction;

/* Fields in the M25PXX Flash ROM's status register. See Section 6.4 of the M25P16 data sheet. */
typedef enum {
	STATUS_WIP = 1 << 0,
} FlashRomStatusRegisterField;

/* The index of the first byte of the first entry in the ring buffer (i.e. the next one to be flushed; the oldest one in the ring buffer). */
static FlashRomAddress flash_rom_head;
/* The index of the next byte after the final byte of the final entry in the ring buffer (i.e. the most recent entry to have been appended). */
static FlashRomAddress flash_rom_tail;
/* If these two are equal, the Flash ROM is empty. */

/* Transmit a Flash ROM address over the SPI bus. This assumes that the instruction code has been sent already, and that SPI is enabled. */
static void _flash_rom_transmit_address (FlashRomAddress sector_address)
{
	/* Transmit MSB first. */
	spi_transmit_byte ((uint8_t) (sector_address >> 16));
	spi_transmit_byte ((uint8_t) (sector_address >> 8));
	spi_transmit_byte ((uint8_t) (sector_address >> 0));
}

/* Poll until the current operation completes. TODO: This should put the MCU to sleep for a while, because operations take a while. PP takes 1.4--5ms,
 * SE takes 1--3s and BE takes 17--40s. */
static void _flash_rom_wait_until_operation_complete (void)
{
	uint8_t status_register;

	spi_select (SPI_CS_FLASH);

	/* Poll the status register's WIP bit. See Section 6.4.1 of the M25P16 data sheet. */
	do {
		spi_transmit_byte (INSTRUCTION_RDSR);
		status_register = spi_transmit_byte (0x00);
	} while ((status_register & STATUS_WIP) != 0);

	spi_deselect ();
}

/* Erase a sector on the Flash ROM. See Section 4.2 of the M25P16 datasheet.
 * This will block until the operation is complete. This will *not* update the head and tail pointers. */
static void _flash_rom_erase_sector (FlashRomAddress sector_address)
{
	/* Enable writes. */
	spi_select (SPI_CS_FLASH);
	spi_transmit_byte (INSTRUCTION_WREN);
	spi_deselect ();

	/* Transmit the sector erase instruction, followed by the address of the sector. */
	spi_select (SPI_CS_FLASH);
	spi_transmit_byte (INSTRUCTION_SE);
	_flash_rom_transmit_address (sector_address);

	/* Writes are implicitly disabled by the completion of the SE instruction. */

	spi_deselect ();

	/* Wait for completion. This will take between 1s and 3s (see Table 15 of the M25P16 data sheet). */
	_flash_rom_wait_until_operation_complete ();
}

/* Erase everything on the Flash ROM. This is primarily for testing. See Section 6.10 of the M25P16 data sheet.
 * This will block until the operation is complete. This will reset the head and tail pointers to 0. */
void flash_rom_erase (void)
{
	/* Enable writes. */
	spi_select (SPI_CS_FLASH);
	spi_transmit_byte (INSTRUCTION_WREN);
	spi_deselect ();

	/* Transmit the bulk erase instruction. */
	spi_select (SPI_CS_FLASH);
	spi_transmit_byte (INSTRUCTION_BE);

	/* Writes are implicitly disabled by the completion of the BE instruction. */

	spi_deselect ();

	/* Wait for completion. This will take between 17s and 40s (see Table 15 of the M25P16 data sheet). */
	_flash_rom_wait_until_operation_complete ();

	/* Update the head and tail pointers. */
	flash_rom_head = 0;
	flash_rom_tail = 0;
}

/* See Section 6.6 of the M25P16 datasheet. */
static LogEntryChecksum _flash_rom_read_checksum (FlashRomAddress entry_address)
{
	LogEntryChecksum checksum;

	spi_select (SPI_CS_FLASH);

	spi_transmit_byte (INSTRUCTION_READ);
	_flash_rom_transmit_address (entry_address + offsetof (FlashRomLogEntry, checksum));
	checksum = spi_transmit_byte (0x00);

	spi_deselect ();

	return checksum;
}

/* Initialise the Flash ROM and driver state. This will scan through the Flash ROM to work out the head and tail of the ring buffer currently in use. */
void flash_rom_init (void)
{
	FlashRomAddress i;

	/* TODO: Take brew_id into account. */
	/* Find the head of the ring buffer. */
	for (i = 0; i < FLASH_ROM_SIZE; i += FLASH_ROM_LOG_ENTRY_SIZE) {
		LogEntryChecksum checksum = _flash_rom_read_checksum (i);

		if (!LOG_ENTRY_CHECKSUM_IS_OVERWRITTEN (checksum)) {
			/* Found a non-erased checksum; we've found the head of the ring buffer. */
			flash_rom_head = i;
			break;
		}
	}

	/* Find the tail of the ring buffer. */
	for (; i < FLASH_ROM_SIZE; i += FLASH_ROM_LOG_ENTRY_SIZE) {
		LogEntryChecksum checksum = _flash_rom_read_checksum (i);

		if (LOG_ENTRY_CHECKSUM_IS_OVERWRITTEN (checksum)) {
			/* Found an erased checksum; we've found the tail of the ring buffer. */
			flash_rom_tail = i;
			break;
		}
	}

	/* So far we've found the first contiguous region of non-erased checksums in the Flash ROM. This doesn't handle the situation where the ring
	 * buffer has wrapped around so that its tail is before its head. If the head is at the beginning of the Flash ROM, walk backwards from it
	 * to find the tail. We assume that the buffer isn't full. */
	if (flash_rom_head == 0) {
		for (i = FLASH_ROM_SIZE - FLASH_ROM_LOG_ENTRY_SIZE; i > flash_rom_tail; i -= FLASH_ROM_LOG_ENTRY_SIZE) {
			LogEntryChecksum checksum = _flash_rom_read_checksum (i);

			if (LOG_ENTRY_CHECKSUM_IS_OVERWRITTEN (checksum)) {
				/* Found an erased checksum; we've found the head of the ring buffer. */
				flash_rom_head = (i + FLASH_ROM_LOG_ENTRY_SIZE) % FLASH_ROM_SIZE;
				break;
			}
		}
	}
}

/* Read n_bytes from addr (to (addr + n_bytes - 1) inclusive) and copy them into buffer (to (buffer + n_bytes - 1) inclusive).
 * This will generate one SPI bus transaction, enabling the bus beforehand and disabling it afterwards. */
static void _flash_rom_read_bytes (FlashRomAddress addr, uint8_t *buffer, uint32_t n_bytes) NONNULL (2);

static void _flash_rom_read_bytes (FlashRomAddress addr, uint8_t *buffer, uint32_t n_bytes)
{
	uint32_t i;

	spi_select (SPI_CS_FLASH);

	/* Start a read transaction at addr. */
	spi_transmit_byte (INSTRUCTION_READ);
	_flash_rom_transmit_address (addr);

	for (i = 0; i < n_bytes; i++) {
		buffer[i] = spi_transmit_byte (0x00);
	}

	spi_deselect ();
}

/* Basically just for testing. Read the first log entry in the flash memory. The checksum is checked; on a failure, the entry will be set to all
 * zeroes. */
FlashRomReadEntryResponse flash_rom_read_head_entry (LogEntry *entry)
{
	FlashRomLogEntry log_entry;
	LogEntryChecksum expected_checksum;

	/* Read the entry and copy it to the output. */
	_flash_rom_read_bytes (flash_rom_head, log_entry.data, FLASH_ROM_LOG_ENTRY_SIZE);
	memcpy (entry, &(log_entry.entry), sizeof (*entry));

	/* Check the checksum. */
	expected_checksum = _log_entry_calculate_checksum (entry, global_brew_config.brew_id);
	return (log_entry.checksum == expected_checksum) ? FLASH_ROM_READ_ENTRY_SUCCESS : FLASH_ROM_READ_ENTRY_CHECKSUM_FAILURE;
}

/* Overwrite the checksum byte of the head entry in the ring buffer, and advance the ring buffer's head pointer. This performs no checking as to
 * whether what's pointed to by the head (before or after advancing the pointer) is a valid entry. This will generate one SPI bus transaction,
 * enabling the bus beforehand and disabling it afterwards.
 *
 * WARNING: This will not copy the head entry to the SD card, or save it in any way. If you call this function while flash_rom_head points to a log
 * entry which isn't saved anywhere else, that log entry will (effectively) be lost. */
void flash_rom_overwrite_head_entry_checksum (void)
{
	/* Enable writes. */
	spi_select (SPI_CS_FLASH);
	spi_transmit_byte (INSTRUCTION_WREN);
	spi_deselect ();

	/* Enter page program mode. Transmit the instruction code and 3-byte address. */
	spi_select (SPI_CS_FLASH);
	spi_transmit_byte (INSTRUCTION_PP);
	_flash_rom_transmit_address (flash_rom_head + offsetof (FlashRomLogEntry, checksum));

	/* Overwrite the checksum byte with 0. */
	spi_transmit_byte (0x00);

	/* Writes are implicitly disabled by the completion of the PP instruction. */
	spi_deselect ();

	/* Wait for completion. This will take between 0.40ms and 5ms (for 1 byte; see Table 15 of the M25P16 data sheet). */
	_flash_rom_wait_until_operation_complete ();

	/* Advance the head pointer. */
	flash_rom_head = (flash_rom_head + FLASH_ROM_LOG_ENTRY_SIZE) % FLASH_ROM_SIZE;
}

/* Flush any log entries from the Flash ROM to the SD card, starting at the current head position. Empty sectors are not erased, but all the log
 * entries in them are marked as invalid. */
FlashRomFlushResponse flash_rom_flush_to_sd_card (void)
{
	unsigned int num_entries_appended = 0, pending_bytes;
	SdCardWriteFileResponse response;

	union {
		LogEntry entries[SD_CARD_APPEND_LOG_ENTRIES_NUM_ENTRIES_MAX];
		uint8_t bytes[SD_CARD_APPEND_LOG_ENTRIES_NUM_ENTRIES_MAX * sizeof (LogEntry)];
	} buffer;

	/* Calculate the number of bytes between the head and tail of the ring buffer; this is the number of bytes which need to be flushed. */
	pending_bytes = (flash_rom_tail > flash_rom_head) ? flash_rom_tail - flash_rom_head : FLASH_ROM_SIZE - flash_rom_head + flash_rom_tail;

	while (pending_bytes > 0) {
		FlashRomOffset n_bytes;
		uint32_t i;

		/* Read a chunk of entries from the Flash ROM. */
		n_bytes = sizeof (buffer.bytes) / sizeof (*(buffer.bytes));
		if (n_bytes > pending_bytes) {
			n_bytes = pending_bytes;
		}

		_flash_rom_read_bytes (flash_rom_head, buffer.bytes, n_bytes);

		/* Append the entries to the SD card. If this fails part-way through, proceed anyway with the entries we did manage to copy across.
		 * This guarantees to flush any buffers before returning, so if this returns successfully and power isn't lost, the log entries have
		 * definitely been written to the SD card and we can safely overwrite their checksums. */
		response = sd_card_append_log_entries (buffer.entries, n_bytes / FLASH_ROM_LOG_ENTRY_SIZE, &num_entries_appended);

		/* Overwrite the checksums of the entries which have been copied to the SD card. */
		for (i = 0; i < num_entries_appended; i++) {
			flash_rom_overwrite_head_entry_checksum ();
		}

		/* Housekeeping. */
		pending_bytes -= num_entries_appended * FLASH_ROM_LOG_ENTRY_SIZE;

		/* If there was an error, bail now. We can't loop around and handle another buffer-full or we'll end up with discontinuities in the
		 * ring buffer which will cause initialisation to fail (or lose data) next time we boot. */
		switch (response) {
			case SD_CARD_WRITE_FILE_SUCCESS:
				continue;
			case SD_CARD_WRITE_FILE_IO_ERROR:
			case SD_CARD_WRITE_FILE_MISC_ERROR:
			case SD_CARD_WRITE_FILE_INVALID_PERMISSIONS:
			default:
				return FLASH_ROM_FLUSH_SD_CARD_ERROR;
		}
	}

	return FLASH_ROM_FLUSH_SUCCESS;
}

/* Program a page on the Flash ROM. See Section 4.1 of the M25P16 datasheet.
 * Note that this does block on write completion. */
void flash_rom_append_entry (LogEntry *entry)
{
	FlashRomAddress new_entry_address, new_entry_tail;
	FlashRomLogEntry log_entry;
	unsigned int i;
	unsigned int data_length;

	/* Set up the data structure and calculate the log entry's checksum. Do this before we start writing, so that we keep the window of
	 * opportunity for data loss small while writing. */
	memcpy (&(log_entry.entry), entry, sizeof (*entry));
	log_entry.brew_id = global_brew_config.brew_id;
	log_entry.checksum = _log_entry_calculate_checksum (entry, global_brew_config.brew_id);
	memset (&(log_entry.padding), 0xff, sizeof (log_entry.padding));

	/* Work out whether we're almost at the end of a sector. If so, erase the next sector before writing the new entry. We do this one entry ahead
	 * of time to ensure the ring buffer is never completely full, which could be confused with it being completely empty (in both cases
	 * flash_rom_head == flash_rom_tail). */
	new_entry_address = flash_rom_tail;
	new_entry_tail = (new_entry_address + FLASH_ROM_LOG_ENTRY_SIZE) % FLASH_ROM_SIZE;

	if (FLASH_ROM_ADDRESS_IS_SECTOR_ALIGNED (new_entry_tail)) {
		/* Check whether the sector needs flushing first (if the head pointer is inside the sector, it means that the ring buffer starts
		 * somewhere within the sector, wraps around at the top of the Flash ROM, and has its tail just before the start of the sector). */
		if (FLASH_ROM_ADDRESS_IS_IN_SECTOR (flash_rom_head, new_entry_tail) && !FLASH_ROM_IS_EMPTY ()) {
			if (flash_rom_flush_to_sd_card () != FLASH_ROM_FLUSH_SUCCESS) {
				/* Flushing (partially?) failed, so there is potentially at least 1 entry still in the sector and we mustn't erase it.
				 * TODO: What should we do here? Probably continue logging and hope the error is transient. Perhaps display an error
				 * message to the user? For the moment, just silently bail. */
				return;
			}
		}

		/* Now erase the sector. */
		_flash_rom_erase_sector (new_entry_tail);
	}

	/* Enable writes. */
	spi_select (SPI_CS_FLASH);
	spi_transmit_byte (INSTRUCTION_WREN);
	spi_deselect ();

	/* Enter page program mode. Transmit the instruction code and 3-byte address. */
	spi_select (SPI_CS_FLASH);
	spi_transmit_byte (INSTRUCTION_PP);
	_flash_rom_transmit_address (new_entry_address);

	/* Transmit the data and checksum. */
	data_length = FLASH_ROM_LOG_ENTRY_SIZE;
	DYNAMIC_ASSERT (data_length <= FLASH_ROM_PAGE_SIZE); /* PP can't operate on more than one page */

	for (i = 0; i < data_length; i++) {
		spi_transmit_byte (log_entry.data[i]);
	}

	/* Writes are implicitly disabled by the completion of the PP instruction. */
	spi_deselect ();

	/* Wait for completion. This will take between 0.52ms and 5ms (for 32 bytes; see Table 15 of the M25P16 data sheet). */
	/* TODO: Why not check the WIP bit at the *start* of this function, and then we only pay the penalty of waiting for writes to complete when
	 * doing several in a row? */
	_flash_rom_wait_until_operation_complete ();

	/* Advance the tail pointer. */
	flash_rom_tail = new_entry_tail;
}

/**
 * \brief Power down flash memory.
 *
 * Move the flash memory into a deep power-down state, where it will consume roughly 5 times less power. Before any reads, programming cycles or
 * erase cycles can be performed, the flash memory has to be brought back to a standby state using flash_rom_power_up().
 */
void flash_rom_power_down (void)
{
	/* Transmit the deep power-down instruction. Reference: Figure 18 of the M25P16 data sheet. */
	spi_select (SPI_CS_FLASH);
	spi_transmit_byte (INSTRUCTION_DP);
	spi_deselect ();
}

/**
 * \brief Power up flash memory.
 *
 * Move the flash memory into a standby power state from its deep power-down state. This must be done before any reads, programming cycles or erase
 * cycles can be performed. This is safe to execute if the device is already in the standby power state.
 */
void flash_rom_power_up (void)
{
	/* Transmit the read electronic signature (power-up) instruction. Reference: Figure 20 of the M25P16 data sheet. */
	spi_select (SPI_CS_FLASH);
	spi_transmit_byte (INSTRUCTION_RES);
	spi_deselect ();

	/* Wait for power-up. This will take 30us (see Table 15 of the M25P16 data sheet). */
	_delay_us (30);
}

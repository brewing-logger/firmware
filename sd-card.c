/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <string.h>

#include <avr/io.h>
#include <util/crc16.h>

#include "lib/diskio.h"
#include "lib/ff.h"

#include "sd-card.h"

/**
 * \file
 * \brief SD card control.
 *
 * Interaction with the SD card, if one is inserted in the SD card slot. This supports a single partition FAT file system on the SD, containing a brew
 * configuration file and a brew results. The brew configuration file can be loaded (or overwritten if a default configuration is generated). The
 * brew results file can be created and appended to.
 *
 * The code also supports generating a partition table and FAT file system if the card previously didn't have one.
 *
 * All fields in data structures stored on the SD card are encoded in little-endian format. e.g. A 16-bit field with value \c 0x1234 is encoded as
 * bytes \c 0x12 and \c 0x34, with \c 0x12 being placed at the _higher_ address.
 *
 * \todo What happens if a non-FAT card is inserted? Does its file system get destroyed? That would be bad.
 */

/**
 * Notes:
 *  - Chip selects are performed automatically by diskio.c.
 */

/**
 * \brief Checksum for a brew configuration.
 *
 * The checksum used for a brew configuration when stored on an SD card. It is calculated over all the fields of the BrewConfig except the version.
 * A CRC-8 checksum is used, so all burst errors of 8 bits (or shorter) will be detected, as will longer burst errors with probability
 * \f$1 - 2^{-8}\f$.
 */
typedef uint8_t BrewConfigChecksum;

/**
 * \brief Checksum for a brew results log entry.
 *
 * The checksum used for a log entry in a brew results file on an SD card. It is calculated over all the fields of the LogEntry. A CRC-8 checksum is
 * used, so all burst errors of 8 bits (or shorter) will be detected, as will longer burst errors with probability \f$1 - 2^{-8}\f$.
 */
typedef uint8_t BrewResultsChecksum;

/**
 * \brief Brew configuration header.
 *
 * The storage layout of the header for a brew configuration file. This is used to read/write the first few bytes of a brew configuration file on the
 * SD card. Once the header has been read, the version can be checked and then the correct storage format will be used to load/store the BrewConfig
 * itself.
 */
typedef struct {
	uint8_t version;		/**< Brew configuration format version number. */
	BrewConfigChecksum checksum;	/**< Brew configuration data checksum over all fields in the BrewConfig. */
} SdCardBrewConfigHeader;

/**
 * \brief Brew results header.
 *
 * The storage layout of the header for a brew results file. This is used to write the first few bytes of a brew results file on the SD card. This
 * allows the client-side software to parse the rest of the file as appropriate.
 */
typedef struct {
	uint8_t version;		/**< Brew results format version number. */
	BrewConfigId brew_id;		/**< ID of the brew configuration for these results. */
} SdCardBrewResultsHeader;

/**
 * \brief Current brew results format version.
 *
 * The version number of the current brew results format.
 */
#define BREW_RESULTS_VERSION 1

/* We only support a single disk drive (the only drive; the SD card on the SPI bus). */
#define DISK_DRIVE_NUMBER 0

/* FAT filesystem on the SD card. */
static FATFS fat_fs;

SdCardInitResponse sd_card_init (void)
{
	FRESULT fat_op_status;
	SdCardInitResponse retval = SD_CARD_INIT_SUCCESS;

	/* Mount the FAT filesystem. */
	fat_op_status = f_mount (DISK_DRIVE_NUMBER, &fat_fs);

	switch (fat_op_status) {
		case FR_OK:
			/* Success! */
			break;
		case FR_DISK_ERR:
		case FR_INT_ERR:
		case FR_NOT_READY:
			/* Low-level I/O error. */
			retval = SD_CARD_INIT_IO_ERROR;
			goto done;
		case FR_WRITE_PROTECTED:
			/* SD card is write protected. */
			retval = SD_CARD_INIT_CARD_WRITE_PROTECTED;
			goto done;
		case FR_INVALID_DRIVE:
			/* Logical drive number is invalid. */
			retval = SD_CARD_INIT_MISC_ERROR;
			goto done;
		case FR_NOT_ENABLED:
		case FR_NO_FILESYSTEM:
		case FR_NO_FILE:
		case FR_NO_PATH:
		case FR_INVALID_NAME:
		case FR_DENIED:
		case FR_EXIST:
		case FR_INVALID_OBJECT:
		case FR_MKFS_ABORTED:
		case FR_TIMEOUT:
		case FR_LOCKED:
		case FR_NOT_ENOUGH_CORE:
		case FR_TOO_MANY_OPEN_FILES:
		case FR_INVALID_PARAMETER:
		default:
			/* These shouldn't ever be encountered when calling f_mount(). */
			retval = SD_CARD_INIT_MISC_ERROR;
			goto done;
	}

done:
	/* Success! */
	return retval;
}

static SdCardCommonResponse _sd_card_convert_f_open_response (FRESULT f_open_response)
{
	switch (f_open_response) {
		case FR_OK:
			/* Success! */
			return SD_CARD_RESPONSE_SUCCESS;
		case FR_DISK_ERR:
		case FR_INT_ERR:
		case FR_NOT_READY:
			/* Low-level I/O error. */
			return SD_CARD_RESPONSE_IO_ERROR;
		case FR_INVALID_DRIVE:
			/* Logical drive number is invalid. */
			return SD_CARD_RESPONSE_MISC_ERROR;
		case FR_NOT_ENABLED:
		case FR_NO_FILESYSTEM:
			/* No filesystem exists. Bail. */
			return SD_CARD_RESPONSE_NOT_EXIST;
		case FR_NO_FILE:
		case FR_NO_PATH:
		case FR_INVALID_NAME:
		case FR_EXIST:
		case FR_INVALID_OBJECT:
			/* File doesn't exist on the filesystem. */
			return SD_CARD_RESPONSE_NOT_EXIST;
		case FR_DENIED:
			/* Access denied. */
			return SD_CARD_RESPONSE_INVALID_PERMISSIONS;
		case FR_WRITE_PROTECTED:
		case FR_MKFS_ABORTED:
		case FR_TIMEOUT:
		case FR_LOCKED:
		case FR_NOT_ENOUGH_CORE:
		case FR_TOO_MANY_OPEN_FILES:
		case FR_INVALID_PARAMETER:
		default:
			/* These shouldn't ever be encountered when calling f_open(). */
			return SD_CARD_RESPONSE_MISC_ERROR;
	}
}

static SdCardCommonResponse _sd_card_convert_f_read_response (FRESULT f_read_response)
{
	switch (f_read_response) {
		case FR_OK:
			return SD_CARD_RESPONSE_SUCCESS;
		case FR_DISK_ERR:
		case FR_INT_ERR:
		case FR_NOT_READY:
			/* Low-level I/O error. */
			return SD_CARD_RESPONSE_IO_ERROR;
		case FR_INVALID_DRIVE:
			/* Logical drive number is invalid. */
			return SD_CARD_RESPONSE_MISC_ERROR;
		case FR_NOT_ENABLED:
		case FR_NO_FILESYSTEM:
			/* No filesystem exists. Bail. */
			return SD_CARD_RESPONSE_NOT_EXIST;
		case FR_NO_FILE:
		case FR_NO_PATH:
		case FR_INVALID_NAME:
		case FR_EXIST:
		case FR_INVALID_OBJECT:
			/* File doesn't exist on the filesystem. */
			return SD_CARD_RESPONSE_NOT_EXIST;
		case FR_DENIED:
			/* Access denied. */
			return SD_CARD_RESPONSE_INVALID_PERMISSIONS;
		case FR_WRITE_PROTECTED:
		case FR_MKFS_ABORTED:
		case FR_TIMEOUT:
		case FR_LOCKED:
		case FR_NOT_ENOUGH_CORE:
		case FR_TOO_MANY_OPEN_FILES:
		case FR_INVALID_PARAMETER:
		default:
			/* These shouldn't ever be encountered when calling f_read(). */
			return SD_CARD_RESPONSE_MISC_ERROR;
	}
}

static SdCardCommonResponse _sd_card_convert_f_write_response (FRESULT f_write_response)
{
	switch (f_write_response) {
		case FR_OK:
			return SD_CARD_RESPONSE_SUCCESS;
		case FR_DISK_ERR:
		case FR_INT_ERR:
		case FR_NOT_READY:
			/* Low-level I/O error. */
			return SD_CARD_RESPONSE_IO_ERROR;
		case FR_INVALID_DRIVE:
			/* Logical drive number is invalid. */
			return SD_CARD_RESPONSE_MISC_ERROR;
		case FR_NOT_ENABLED:
		case FR_NO_FILESYSTEM:
			/* No filesystem exists. Bail. */
			return SD_CARD_RESPONSE_MISC_ERROR;
		case FR_NO_FILE:
		case FR_NO_PATH:
		case FR_INVALID_NAME:
		case FR_EXIST:
		case FR_INVALID_OBJECT:
			/* File doesn't exist on the filesystem. */
			return SD_CARD_RESPONSE_NOT_EXIST;
		case FR_DENIED:
			/* Access denied. */
			return SD_CARD_RESPONSE_INVALID_PERMISSIONS;
		case FR_WRITE_PROTECTED:
		case FR_MKFS_ABORTED:
		case FR_TIMEOUT:
		case FR_LOCKED:
		case FR_NOT_ENOUGH_CORE:
		case FR_TOO_MANY_OPEN_FILES:
		case FR_INVALID_PARAMETER:
		default:
			/* These shouldn't ever be encountered when calling f_write(). */
			return SD_CARD_RESPONSE_MISC_ERROR;
	}
}

/* Calculate a checksum for the brew config. The Dallas/Maxim CRC-8 from avr-libc is used.
 * It uses polynomial: x^8 + x^5 + x^4 + 1 with initial value 0x00.
 * See: http://www.nongnu.org/avr-libc/user-manual/group__util__crc.html#ga37b2f691ebbd917e36e40b096f78d996 */
static BrewConfigChecksum _brew_config_calculate_checksum (const BrewConfig *brew_config, uint8_t version) NONNULL (1);

static BrewConfigChecksum _brew_config_calculate_checksum (const BrewConfig *brew_config, uint8_t version)
{
	uint8_t crc = 0, i;

	crc = _crc_ibutton_update (crc, version);
	for (i = 0; i < sizeof (BrewConfig); i++) {
		crc = _crc_ibutton_update (crc, ((const uint8_t *) brew_config)[i]);
	}

	return crc;
}

SdCardLoadFileResponse sd_card_load_brew_config (BrewConfig *brew_config)
{
	/* TODO: preconditions and postconditions. */

	FRESULT fat_op_status;
	FIL brew_config_file;
	UINT bytes_read;
	SdCardBrewConfigHeader header;
	SdCardLoadFileResponse retval = SD_CARD_LOAD_FILE_SUCCESS;

	/* Open the config file. */
	fat_op_status = f_open (&brew_config_file, BREW_CONFIG_FILE_NAME, FA_READ);
	retval = _sd_card_convert_f_open_response (fat_op_status);

	if (retval != SD_CARD_LOAD_FILE_SUCCESS) {
		/* Failure. */
		goto done;
	}

	/* Read the file contents. Read the header first so we know how big the config file should be. */
	bytes_read = 0;
	fat_op_status = f_read (&brew_config_file, &header, sizeof (SdCardBrewConfigHeader), &bytes_read);
	retval = _sd_card_convert_f_read_response (fat_op_status);

	if (retval == SD_CARD_LOAD_FILE_SUCCESS) {
		/* Success! Check the number of bytes read. */
		if (bytes_read != sizeof (SdCardBrewConfigHeader)) {
			/* Somehow not even the version and checksum could be read. Bail. */
			retval = SD_CARD_LOAD_FILE_IO_ERROR;
			goto close_file;
		}

		/* Header was read successfully. Do we support this version? */
		if (header.version != BREW_CONFIG_VERSION) {
			retval = SD_CARD_LOAD_FILE_UNSUPPORTED_VERSION;
			goto close_file;
		}
	} else {
		/* Failure. */
		goto close_file;
	}

	/* Read the rest of the file contents into the config data structure. */
	bytes_read = 0;
	fat_op_status = f_read (&brew_config_file, brew_config, sizeof (BrewConfig), &bytes_read);
	retval = _sd_card_convert_f_read_response (fat_op_status);

	if (retval == SD_CARD_LOAD_FILE_SUCCESS) {
		/* Success! Check the number of bytes read. */
		if (bytes_read != sizeof (BrewConfig)) {
			/* The data structure was only partially filled. The config file on the SD card must be corrupt or in the wrong
			 * format. */
			retval = SD_CARD_LOAD_FILE_IO_ERROR;
			goto close_file;
		}

		/* Otherwise: file was read successfully and in full. */
	} else {
		/* Failure. */
		goto close_file;
	}

	/* Check the file's checksum. */
	if (header.checksum != _brew_config_calculate_checksum (brew_config, header.version)) {
		retval = SD_CARD_LOAD_FILE_CHECKSUM_FAILURE;
		goto close_file;
	}

close_file:
	/* Close the file. Ignore any errors. */
	f_close (&brew_config_file);

done:
	return retval;
}

SdCardWriteFileResponse sd_card_create_brew_config (const BrewConfig *brew_config)
{
	SdCardCommonResponse _retval;
	SdCardWriteFileResponse retval;
	FRESULT fat_op_status;
	FIL brew_config_file;
	UINT bytes_written = 0;
	SdCardBrewConfigHeader header;

	/* Create the file header. */
	header.version = BREW_CONFIG_VERSION;
	header.checksum = _brew_config_calculate_checksum (brew_config, BREW_CONFIG_VERSION);

	/* Try opening the file for writing. */
	fat_op_status = f_open (&brew_config_file, BREW_CONFIG_FILE_NAME, FA_WRITE | FA_OPEN_ALWAYS);

	if (fat_op_status == FR_NO_FILESYSTEM) {
		/* Create a file system and try again. Ignore failures in creating the file system; we'll catch then when trying to open the file.*/
		#define PARTITION_TABLE_FDISK 0 /* fdisk-style partition table */
		#define PARTITION_TABLE_SFD 1 /* no partition table */
		#define ALLOCATION_UNIT_AUTO 0 /* automatically select FAT allocation unit size (bytes) */

		f_mkfs (DISK_DRIVE_NUMBER, PARTITION_TABLE_SFD, ALLOCATION_UNIT_AUTO);
		fat_op_status = f_open (&brew_config_file, BREW_CONFIG_FILE_NAME, FA_WRITE | FA_CREATE_ALWAYS);
	}

	_retval = _sd_card_convert_f_open_response (fat_op_status);
	if (_retval == SD_CARD_RESPONSE_NOT_EXIST) {
		/* There is no equivalent to SD_CARD_RESPONSE_NOT_EXIST in SdCardWriteFileResponse, so filter that out. */
		retval = SD_CARD_RESPONSE_MISC_ERROR;
	} else {
		retval = (SdCardWriteFileResponse) _retval;
	}

	if (retval != SD_CARD_WRITE_FILE_SUCCESS) {
		/* Failure. */
		goto done;
	}

	/* Write out the header. */
	bytes_written = 0;
	fat_op_status = f_write (&brew_config_file, &header, sizeof (SdCardBrewConfigHeader), &bytes_written);
	_retval = _sd_card_convert_f_write_response (fat_op_status);
	if (_retval == SD_CARD_RESPONSE_NOT_EXIST) {
		/* There is no equivalent to SD_CARD_RESPONSE_NOT_EXIST in SdCardWriteFileResponse, so filter that out. */
		retval = SD_CARD_RESPONSE_MISC_ERROR;
	} else {
		retval = (SdCardWriteFileResponse) _retval;
	}

	if (retval != SD_CARD_WRITE_FILE_SUCCESS || bytes_written != sizeof (SdCardBrewConfigHeader)) {
		/* Failure. Ensure retval has a non-success value if we failed due to writing too little out. */
		if (retval == SD_CARD_WRITE_FILE_SUCCESS && bytes_written != sizeof (SdCardBrewConfigHeader)) {
			retval = SD_CARD_WRITE_FILE_IO_ERROR;
		}

		goto close_file;
	}

	/* Write out the config proper. */
	bytes_written = 0;
	fat_op_status = f_write (&brew_config_file, brew_config, sizeof (BrewConfig), &bytes_written);
	_retval = _sd_card_convert_f_write_response (fat_op_status);
	if (_retval == SD_CARD_RESPONSE_NOT_EXIST) {
		/* There is no equivalent to SD_CARD_RESPONSE_NOT_EXIST in SdCardWriteFileResponse, so filter that out. */
		retval = SD_CARD_RESPONSE_MISC_ERROR;
	} else {
		retval = (SdCardWriteFileResponse) _retval;
	}

	if (retval != SD_CARD_WRITE_FILE_SUCCESS || bytes_written != sizeof (BrewConfig)) {
		/* Failure. Ensure retval has a non-success value if we failed due to writing too little out. */
		if (retval == SD_CARD_WRITE_FILE_SUCCESS && bytes_written != sizeof (BrewConfig)) {
			retval = SD_CARD_WRITE_FILE_IO_ERROR;
		}

		goto close_file;
	}

close_file:
	f_close (&brew_config_file);

done:
	return retval;
}

static SdCardCommonResponse _sd_card_convert_f_lseek_response (FRESULT f_lseek_response)
{
	switch (f_lseek_response) {
		case FR_OK:
			return SD_CARD_RESPONSE_SUCCESS;
		case FR_DISK_ERR:
		case FR_INT_ERR:
		case FR_NOT_READY:
			/* Low-level I/O error. */
			return SD_CARD_RESPONSE_IO_ERROR;
		case FR_INVALID_DRIVE:
			/* Logical drive number is invalid. */
			return SD_CARD_RESPONSE_MISC_ERROR;
		case FR_NOT_ENABLED:
		case FR_NO_FILESYSTEM:
			/* No filesystem exists. Bail. */
			return SD_CARD_RESPONSE_MISC_ERROR;
		case FR_NO_FILE:
		case FR_NO_PATH:
		case FR_INVALID_NAME:
		case FR_EXIST:
		case FR_INVALID_OBJECT:
			/* File doesn't exist on the filesystem. */
			return SD_CARD_RESPONSE_NOT_EXIST;
		case FR_DENIED:
			/* Access denied. */
			return SD_CARD_RESPONSE_INVALID_PERMISSIONS;
		case FR_WRITE_PROTECTED:
		case FR_MKFS_ABORTED:
		case FR_TIMEOUT:
		case FR_LOCKED:
		case FR_NOT_ENOUGH_CORE:
		case FR_TOO_MANY_OPEN_FILES:
		case FR_INVALID_PARAMETER:
		default:
			/* These shouldn't ever be encountered when calling f_lseek(). */
			return SD_CARD_RESPONSE_MISC_ERROR;
	}
}

/* Calculate a checksum for the brew config. The Dallas/Maxim CRC-8 from avr-libc is used.
 * It uses polynomial: x^8 + x^5 + x^4 + 1 with initial value 0x00.
 * See: http://www.nongnu.org/avr-libc/user-manual/group__util__crc.html#ga37b2f691ebbd917e36e40b096f78d996 */
static BrewResultsChecksum _brew_results_calculate_checksum (const LogEntry *log_entry) NONNULL (1);

static BrewConfigChecksum _brew_results_calculate_checksum (const LogEntry *log_entry)
{
	uint8_t crc = 0, i;

	for (i = 0; i < sizeof (LogEntry); i++) {
		crc = _crc_ibutton_update (crc, ((const uint8_t *) log_entry)[i]);
	}

	return crc;
}

/**
 * \brief Append log entries to results file.
 *
 * Append the given log entries to the results file on the SD card. If power is lost part-way through a call to this function, the results file is
 * left in an indeterminate state (between 0 and \a num_entries entries could have been partially or wholly appended). However, once this function
 * returns, the entries are guaranteed to have been entirely written to the SD card and any buffers flushed.
 */
SdCardWriteFileResponse sd_card_append_log_entries (LogEntry *entries, unsigned int num_entries, unsigned int *num_entries_appended)
{
	FRESULT fat_op_status;
	FIL brew_results_file;
	SdCardCommonResponse _retval;
	SdCardWriteFileResponse retval = SD_CARD_WRITE_FILE_SUCCESS;
	SdCardBrewResultsHeader header;
	unsigned int i = 0;
	UINT bytes_written = 0;

	/* Set up the header for the brew results file. */
	header.version = BREW_RESULTS_VERSION;
	header.brew_id = global_brew_config.brew_id;

	/* Open the results file. */
	fat_op_status = f_open (&brew_results_file, BREW_RESULTS_FILE_NAME, FA_WRITE | FA_OPEN_ALWAYS);
	_retval = _sd_card_convert_f_open_response (fat_op_status);
	if (_retval == SD_CARD_RESPONSE_NOT_EXIST) {
		/* There is no equivalent to SD_CARD_RESPONSE_NOT_EXIST in SdCardWriteFileResponse, so filter that out. */
		retval = SD_CARD_RESPONSE_MISC_ERROR;
	} else {
		retval = (SdCardWriteFileResponse) _retval;
	}

	if (retval != SD_CARD_WRITE_FILE_SUCCESS) {
		/* Failure. */
		goto done;
	}

	/* Seek to the end of the file. */
	fat_op_status = f_lseek (&brew_results_file, brew_results_file.fsize);
	_retval = _sd_card_convert_f_lseek_response (fat_op_status);
	if (_retval == SD_CARD_RESPONSE_NOT_EXIST) {
		/* There is no equivalent to SD_CARD_RESPONSE_NOT_EXIST in SdCardWriteFileResponse, so filter that out. */
		retval = SD_CARD_RESPONSE_MISC_ERROR;
	} else {
		retval = (SdCardWriteFileResponse) _retval;
	}

	if (retval != SD_CARD_WRITE_FILE_SUCCESS) {
		/* Failure. */
		goto close_file;
	}

	/* If the file is zero-sized (i.e. we've just created it), write out a header containing the brew ID. */
	if (brew_results_file.fsize == 0) {
		bytes_written = 0;
		fat_op_status = f_write (&brew_results_file, &header, sizeof (SdCardBrewResultsHeader), &bytes_written);
		_retval = _sd_card_convert_f_write_response (fat_op_status);
		if (_retval == SD_CARD_RESPONSE_NOT_EXIST) {
			/* There is no equivalent to SD_CARD_RESPONSE_NOT_EXIST in SdCardWriteFileResponse, so filter that out. */
			retval = SD_CARD_RESPONSE_MISC_ERROR;
		} else {
			retval = (SdCardWriteFileResponse) _retval;
		}

		if (retval != SD_CARD_WRITE_FILE_SUCCESS || bytes_written != sizeof (SdCardBrewResultsHeader)) {
			/* Failure. Ensure retval has a non-success value if we failed due to writing too little out. */
			if (retval == SD_CARD_WRITE_FILE_SUCCESS && bytes_written != sizeof (SdCardBrewResultsHeader)) {
				retval = SD_CARD_WRITE_FILE_IO_ERROR;
			}

			goto close_file;
		}
	}

	/* Append each entry to the results file. */
	for (i = 0; i < num_entries; i++) {
		union {
			struct {
				LogEntry log_entry;
				BrewResultsChecksum checksum;
			};
			uint8_t data[sizeof (LogEntry) + sizeof (BrewResultsChecksum)];
		} output;

		/* Prepare the entry. */
		memcpy (&(output.log_entry), &(entries[i]), sizeof (LogEntry));
		output.checksum = _brew_results_calculate_checksum (&(output.log_entry));

		/* Write the entry and checksum out. */
		fat_op_status = f_write (&brew_results_file, output.data, sizeof (output.data), &bytes_written);
		_retval = _sd_card_convert_f_write_response (fat_op_status);
		if (_retval == SD_CARD_RESPONSE_NOT_EXIST) {
			/* There is no equivalent to SD_CARD_RESPONSE_NOT_EXIST in SdCardWriteFileResponse, so filter that out. */
			retval = SD_CARD_RESPONSE_MISC_ERROR;
		} else {
			retval = (SdCardWriteFileResponse) _retval;
		}

		if (retval != SD_CARD_WRITE_FILE_SUCCESS || bytes_written != sizeof (output.data)) {
			/* Failure. Ensure retval has a non-success value if we failed due to writing too little out. */
			if (retval == SD_CARD_WRITE_FILE_SUCCESS && bytes_written != sizeof (output.data)) {
				retval = SD_CARD_WRITE_FILE_IO_ERROR;
			}

			goto close_file;
		}
	}

close_file:
	f_close (&brew_results_file);

done:
	*num_entries_appended = i;

	return retval;
}

/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) 2012 Brian Jones (University of Cambridge Computer Lab)
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Support for DS3231 Real Time Clock

	DS1302 is an I2C device
	Requires I2C support, calls i2c.c
	This in turn requires a call of i2c_timerproc every 10mS to
	avoid failed I2C calls from blocking
	
	Make sure that TWI is initialised correctly before use. See note in i2c.c

*/


/* 

3/2/2012  svn 499  
Major alteration of order of parameters

*/



#include "ds3231.h"


void reset_rtc_i2c(void) {
	
	TW_DDR &= ~(1<<TW_SDA) ;								// SDA input
	TW_DDR |= (1<<TW_SCK) ;									// SCL output
	TW_PORT |= (1<<TW_SCK) ;
	while ( (TW_PIN & (1<<TW_SDA)) == 0 ) {					// Wait for SDA high
		TW_PORT &= ~(1<<TW_SCK) ;
		_delay_us(100);
		TW_PORT |= (1<<TW_SCK) ;
		_delay_us(100);
	}		
	TW_DDR &= ~(1<<TW_SCK) ;
	_delay_us(10);
	
}



uint8_t read_from_rtc(uint8_t *store_data) {

	uint8_t errorcode ;

	errorcode = set_rtc_register_pointer(0) ;
	 _delay_us(10);
	if (errorcode == 0 ) {
		errorcode = read_i2c_device(0xD1,7,store_data) ;
	}
	return errorcode ;	
}



uint8_t write_to_rtc(uint8_t *store_data) {

	 return write_i2c_device(0xD0,8,store_data) ;
}



// See ds3231.h, as this needs data set up in a particular way.
uint8_t write_alarm(uint8_t *alarm_data){

	return write_i2c_device(0xD0,9,alarm_data) ;		
}



uint8_t read_temp_from_rtc(uint8_t *rtc_temp) {
	
	uint8_t errorcode ;

	errorcode = set_rtc_register_pointer(0x11) ;	
	_delay_us(10);											// Needs time for I2C bus to recover
	if (errorcode == 0 ) {
		errorcode = read_i2c_device(0xD1,2,rtc_temp) ;
	}
	return errorcode ;	
}



uint8_t set_rtc_register_pointer(uint8_t regval) {
	
	uint8_t tmp = regval ;
	return write_i2c_device(0xD0,1,&tmp) ;	
}



uint32_t get_fattime(void) {
	
	uint32_t fattime ;
	uint16_t time ;
	uint16_t date ;
	uint8_t time_store[7];
	uint8_t min, hour, month, year;
	
	read_from_rtc(time_store);   /* get the time */

	time = bcd_to_hex(time_store[0]/2) ;
	min = bcd_to_hex(time_store[1]) ;
	hour = bcd_to_hex(time_store[2]) ;
	date = bcd_to_hex(time_store[4]) ;
	month = bcd_to_hex(time_store[5]) ;
	year = bcd_to_hex(time_store[6])+20 ;
	
	time |= (min << 5) ;
	time |= (hour << 11) ;
	date |= (month << 5) ;
	date |= (year << 9) ;
	
	fattime = time | ((uint32_t)date<<16);
	
	return fattime;
}



void dirname_from_date(uint8_t *dirname,uint8_t *time_store) {

	dirname[1] = hex2ascii_h(time_store[6]) ;				// last 2 digits of year
	dirname[2] = hex2ascii_l(time_store[6]) ;				// last 2 digits of year
	dirname[3] = hex2ascii_l(bcd_to_hex(time_store[5]));	// month as horrible 0x01 to 0x0C value
	dirname[4] = hex2ascii_h(time_store[4]) ;				// date
	dirname[5] = hex2ascii_l(time_store[4]) ;				// date
	dirname[6] = hex2ascii_h(time_store[2]) ;				// hour
	dirname[7] = hex2ascii_l(time_store[2]) ;				// hour
}



void filename_from_date(uint8_t *filename,uint8_t *time_store) {
	
	filename[1] = hex2ascii_l(bcd_to_hex(time_store[5]));// month as horrible 0x01 to 0x0C value
	filename[2] = (time_store[4] >> 4) + '0';			// 10s date
	filename[3] = (time_store[4] & 0x0F) + '0';			// date
	filename[4] = (time_store[2] >> 4) + '0';			// 10s hour
	filename[5] = (time_store[2] & 0x0F) + '0';			// hour
	filename[6] = (time_store[1] >> 4) + '0';			// 10s min
	filename[7] = (time_store[1] & 0x0F) + '0';			// mins
//	filename[10] = (time_store[0] >> 4) + '0';			// 10s sec
//	filename[11] = (time_store[0] & 0x0F) + '0';		// sec

}



void iso_time(uint8_t *iso_time_store,uint8_t *time_store) {

	iso_time_store[0] = '2';
	iso_time_store[1] = '0';
	iso_time_store[2] = (time_store[6] >> 4) + '0';
	iso_time_store[3] = (time_store[6] & 0x0F) + '0';
	iso_time_store[4] = (time_store[5] >> 4) + '0';
	iso_time_store[5] = (time_store[5] & 0x0F) + '0';
	iso_time_store[6] = (time_store[4] >> 4) + '0';
	iso_time_store[7] = (time_store[4] & 0x0F) + '0';
	iso_time_store[8] = '_';
	iso_time_store[9] = (time_store[2] >> 4) + '0';
	iso_time_store[10] = (time_store[2] & 0x0F) + '0';
	iso_time_store[11] = (time_store[1] >> 4) + '0';
	iso_time_store[12] = (time_store[1] & 0x0F) + '0';
	iso_time_store[13] = (time_store[0] >> 4) + '0';
	iso_time_store[14] = (time_store[0] & 0x0F) + '0';
	iso_time_store[15] = 0;	
}



void americandate(uint8_t *str,uint8_t *time_store) {
		
	str[0] = (time_store[5] >> 4) + '0';
	str[1] = (time_store[5] & 0x0F) + '0';
	str[2] = '/';
	str[3] = (time_store[4] >> 4) + '0';
	str[4] = (time_store[4] & 0x0F) + '0';
	str[5] = '/';
	str[6] = '2';
	str[7] = '0';
	str[8] = (time_store[6] >> 4) + '0';
	str[9] = (time_store[6] & 0x0F) + '0';
	str[10] = 0;
}
	
	
	
void hms_time(uint8_t *str,uint8_t *time_store) {
	
	str[0] = (time_store[2] >> 4) + '0';
	str[1] = (time_store[2] & 0x0F) + '0';
	str[2] = ':';
	str[3] = (time_store[1] >> 4) + '0';
	str[4] = (time_store[1] & 0x0F) + '0';
	str[5] = ':';
	str[6] = (time_store[0] >> 4) + '0';
	str[7] = (time_store[0] & 0x0F) + '0';
	str[8] = 0;	
}

 

// Take a byte (hexval) and return the ASCII character for the highest 4 bits.
uint8_t hex2ascii_h(uint8_t hexval) {
	uint8_t tmp;
	tmp = (hexval >> 4) + '0' ;
	if (tmp > 0x39) {
		tmp += 7;
	}
	return tmp;
}



// Take a byte (hexval) and return the ASCII character for the lowest 4 bits.
uint8_t hex2ascii_l(uint8_t hexval) {
	uint8_t tmp;
	tmp = (hexval & 0x0F) + '0' ;
	if (tmp > 0x39) {
		tmp += 7;
	}
	return tmp;
}



uint8_t bcd_to_hex(uint8_t bcd) {
	
	uint8_t msn = (bcd >> 4);
	return ((msn * 10) + (bcd & 0x0F)) ;
}



uint8_t set_datetime(uint8_t *iso_time_store) {

	uint8_t time_data[8] ;
	time_data[0] = 0;
	time_data[1] = ( ( (iso_time_store[13] - '0') << 4) + ((iso_time_store[14] - '0') & 0x0F)); // ss
	time_data[2] = ( ( (iso_time_store[11] - '0') << 4) + ((iso_time_store[12] - '0') & 0x0F)); // mm
	time_data[3] = ( ( (iso_time_store[9] - '0') << 4) + ((iso_time_store[10] - '0') & 0x0F));  // hh
	time_data[4] = 1;																			// dow
	time_data[5] = ( ( (iso_time_store[6] - '0') << 4) + ((iso_time_store[7] - '0') & 0x0F));   // dd
	time_data[6] = ( ( (iso_time_store[4] - '0') << 4) + ((iso_time_store[5] - '0') & 0x0F));   // mm
	time_data[7] = ( ( (iso_time_store[2] - '0') << 4) + ((iso_time_store[3] - '0') & 0x0F));   // yy

	return write_i2c_device(0xD0,8,time_data) ;
}



// Add seconds to the hh:mm:ss value
// Note that the result is put into result[1] onwards, ready for write to alarm
void addtime(uint8_t *result, uint8_t *start, uint16_t seconds) {

	uint16_t total_seconds ;
	uint8_t hex_ss,hex_mm,hex_hh ;

	total_seconds = seconds ; 	
	total_seconds +=       10*(start[0]>>4) + (start[0] & 0x0F) ;
	total_seconds +=   60*(10*(start[1]>>4) + (start[1] & 0x0F)) ;
	total_seconds += 3600*(10*(start[2]>>4) + (start[2] & 0x0F)) ;

// these hold a hex value which is the correct time (eg 0x0C for 12 O clock)
	hex_ss = (uint8_t)(total_seconds%60);
	hex_mm = (uint8_t)((total_seconds/60) % 60);
	hex_hh = (uint8_t)((total_seconds/3600) % 24);	

	result[1] = ((hex_ss / 10) << 4) + hex_ss % 10 ;
	result[2] = ((hex_mm / 10) << 4) + hex_mm % 10 ;
	result[3] = ((hex_hh / 10) << 4) + hex_hh % 10 ;
}



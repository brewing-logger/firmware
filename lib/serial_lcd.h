/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) 2011, 2012 Brian Jones (University of Cambridge Computer Lab)
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SERIAL_LCD_H
#define SERIAL_LCD_H


#include <config.h>
#include <avr/io.h>
#include <util/delay_basic.h>
#include <util/delay.h>

/* 23/2/2011  svn 282  */



/**
 * \brief 	Internal command used within the library to sens a byte over the two wire system.
 *
 * \param  rs   1=data, 0=control
 *
 * \param  lcd_data    8 bit value to be sent to the LCD
 *
 */
void send_byte_to_lcd(uint8_t rs,uint8_t lcd_data);


/**
 * \brief 	Move the LCD cursor to the home position without otherwise changing the display.
 *
 * 			Convenience function to issue the command 0x02
 *			This function will return as soon as the command is issued, the display may internally
 *			take an unspecified time to execute the command.
 *
 */
void home_lcd(void);


/**
 * \brief 	clear the display, and home the cursor.
 *
 * 			Convenience function to issue the command 0x01
 *			This function will return as soon as the command is issued, the display may internally
 *			take an unspecified time to execute the command.
 *
 */
void clear_lcd(void);


/**
 * \brief 	Scroll the display left by 1 character, without otherwise changing the display. 
 *
 * 			Note that both lines scroll left on a multi line display.
 *			The display has 40 characters of memory for each line, irrespective of the number of characters displayed.
 *			This function will return as soon as the command is issued, the display may internally
 *			take an unspecified time to execute the command.
 *
 */
void scroll_lcd_left(void);


/**
 * \brief 	Scroll the display right by 1 character, without otherwise changing the display.
 *
 * 			Note that both lines scroll right on a multi line display, either wrapping or inserting blank chacters at the 
 *			beginning of the line depending on the specific model of LCD.
 *			The display has 40 characters of memory for each line, irrespective of the number of characters displayed.
 *			This function will return as soon as the command is issued, the display may internally
 *			take an unspecified time to execute the command.
 *
 */
void scroll_lcd_right(void);

/**
 * \brief 	Move the LCD cursor to the second line of the display.
 *
 *			This function will return as soon as the command is issued, the display may internally
 *			take an unspecified time to execute the command.
 *
 */
void row2_lcd(void);



/**
 * \brief 	Move the address pointer to the memory address corresponding to the row and column specified.
 *
 * \param  row				Row number counting from 0
 * \param  column			Column number counting from 0
 *
 *			Moves the address pointer to the address specified, so new text will be inserted at this
 *			 new address. The blinking cursor will not be moved.
 *
 *			The display has 40 characters of memory for each line, irrespective of the number of characters displayed.
 *
 *			This function will return as soon as the command is issued, the display may internally
 *			take an unspecified time to execute the command.
 *
 */
void setaddress_lcd(uint8_t row, uint8_t column);


/**
 * \brief 	Display the specified character at the cursor position and move the cursor on 1 position.
 *
 * \param  character		The 8 bit character to be displayed
 *
 *			The display has 40 characters of memory for each line, irrespective of the number of characters displayed, so
 *			 it is perfectly possible to write 'off the right' of a display. The characters there would be seen in a subsequent scroll left.
 *
 *			This function will return as soon as the command is issued, the display may internally
 *			take an unspecified time to execute the command.
 *
 */
void LCD_display_char(char character);


/**
 * \brief 	Display the specified string at the cursor position and move the cursor on.
 *
 * \param  string			pointer to the start of the string
 *
 *			See the documentation for LCD_display_char for more detail and limitations of these LCD devices,
 *
 */
void LCD_display_string(const char* string);


/**
 * \brief 	Display an 8 bit unsigned value as up to three characters.
 *
 * \param  val				The 8 bit value to be displayed
 *
 *             Display an 8 bit unsigned integer on the LCD, as a series of up to 
 *				three decimal characters. 
 *			   Doesn't output leading zeroes, so number of characters transmitted may vary.
 *
 *			  See the documentation for LCD_display_char for more detail and limitations of these LCD devices,
 */
void LCD_display_uint8(uint8_t val);


/**
 * \brief 	Display a signed 8 bit value as an optional minus sign followed by up to three characters.
 *
 * \param  val				The 8 bit value to be displayed
 *
 *             Display an 8 bit signed integer on the LCD, as an optional minus sign followed by a series of up to 
 *				three decimal characters. 
 *			   Doesn't output leading zeroes, so number of characters transmitted may vary.
 *
 *			  See the documentation for LCD_display_char for more detail and limitations of these LCD devices,
 */
void LCD_display_int8(int8_t val);


/**
 * \brief 	Display a 16 bit unsigned value as up to three characters.
 *
 * \param  val				The 16 bit value to be displayed
 *
 *             Display a 16 bit unsigned integer on the LCD, as a series of up to 
 *				five decimal characters. 
 *			   Doesn't output leading zeroes, so number of characters transmitted may vary.
 *
 *			  See the documentation for LCD_display_char for more detail and limitations of these LCD devices,
 */
void LCD_display_uint16(uint16_t val);


/**
 * \brief 	Display a signed 16 bit value as an optional minus sign followed by up to three characters.
 *
 * \param  val				The 16 bit value to be displayed
 *
 *             Display an 16 bit signed integer on the LCD, as an optional minus sign followed by a series of up to 
 *				five decimal characters. 
 *			   Doesn't output leading zeroes, so number of characters transmitted may vary.
 *
 *			  See the documentation for LCD_display_char for more detail and limitations of these LCD devices,
 */
void LCD_display_int16(int16_t val);


/**
 * \brief 	Display an 8 bit unsigned value as exactly eight characters of 0 or 1.
 *
 * \param  val				The 8 bit value to be displayed
 *
 *            Display an 8 bit unsigned value as exactly eight characters of 0 or 1.
 *
 *			  See the documentation for LCD_display_char for more detail and limitations of these LCD devices,
 */
void LCD_display_bin(uint8_t val);


/**
 * \brief 	Display the lowest 4 bits of an 8 bit unsigned value as one ASCII character 0-9,A-F
 *
 * \param  nibble			The 8 bit value to be displayed
 *
 *            Display the lowest 4 bits of an 8 bit unsigned value as one ASCII character 0-9,A-F
 *				The upper bits of the value are ignored.
 *
 *			  See the documentation for LCD_display_char for more detail and limitations of these LCD devices,
 */
void LCD_display_nibble(uint8_t nibble);



/**
 * \brief 	Display an 8 bit unsigned value as two ASCII characters 0-9,A-F
 *
 * \param  val				The 8 bit value to be displayed
 *
 *            Display an 8 bit unsigned value as two ASCII characters 0-9,A-F, top 4 bits to ASCII first, then low 4 bits
 *			  
 *
 *			  See the documentation for LCD_display_char for more detail and limitations of these LCD devices,
 */
 void LCD_display_hex(uint8_t val);



#endif


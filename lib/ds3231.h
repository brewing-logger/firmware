/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) 2011, 2012 Brian Jones (University of Cambridge Computer Lab)
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */


/* Support for DS3231 Real Time Clock

	DS3231 is an I2C device
	Requires I2C support, calls i2c.c
	This in turn requires a call of i2c_timerproc every 10mS to
	avoid failed I2C calls from blocking
	
	Make sure that TWI is initialised correctly before use. See note in i2c.c

*/

/* 15/12/2011  svn 479  */



/* Note that if using the alarm, you need to write values to the alarm
registers, then write the interrupt enable bits to the control register.
Once the interrupt occurs, you need to write to the status register to clear the interrupt.
Unsetting the bit in the control register is not enough 
*/

#ifndef DS3231_H
#define DS3231_H


#include <avr/io.h>
#include "config.h"
#include <util/delay.h>
#include <util/delay_basic.h>
#include "i2c.h"




/** \brief		Resets the I2C to RTC communications.
 *
 */
void reset_rtc_i2c(void);



/** \brief		Read a set of data from the DS3231 Real time clock to memory
 *
 *	\param		*store_data		Pointer to location of 7 bytes to store RTC values
 * 								Order of RTC values in time_store is ss,mm,hh,dow,dd,mm,yy		valid dow is 1-7
 *
 *	\return						Returns the I2C error code from the transaction. 0=success.
 *
 */
uint8_t read_from_rtc(uint8_t *store_data);



/** \brief		Write a set of data from memory to the DS3231 Real time clock
 *
 *	\param		*store_data		Pointer to location of 8 bytes (first byte must be 0) from which to write RTC values
 * 								Order of RTC values for setting time is 0,ss,mm,hh,dow,dd,mm,yy    valid dow is 1-7
 *
 *	\return						Returns the I2C error code from the transaction. 0=success.
 *
 */
uint8_t write_to_rtc(uint8_t *store_data);



/** \brief		Write a set of data from memory to the alarm registers on the DS3231 Real time clock
 *
 *	\param		*alarm_data		Pointer to location of 9 bytes (first byte padding 0x07) from which to get values to send to alarm
 * 								See datasheet for order of RTC values for alarm1,alarm2, control 
 *								Note that the alarm control bits are the top bits of the alarm values.
 *
 *	\return						Returns the I2C error code from the transaction. 0=success.
 *
 */
uint8_t write_alarm(uint8_t *alarm_data);



/** \brief		Read a temperature from the DS3231 internal temp sensor.
 *
 *	\param		*store_data		Pointer to 2 bytes of storage for the result. 
 *								First byte temp as signed 8 bit hex value, second byte fraction of 1C (top 2 bits)
 *	
 *	\return						Returns the I2C error code from the transaction. 0=success.
 *
 */
uint8_t read_temp_from_rtc(uint8_t *store_data);



/** \brief		Set the register pointer in the RTC to the parameter supplied
 *
 *	\param		val			The address to be set
 *
 * 	\return					Returns the I2C error code from the transaction. 0=success.	
 *
 */
uint8_t set_rtc_register_pointer(uint8_t val);



/** \brief		Reads the time into internal variable time_store[7], and converts for FAT filesystems. 
 *
 * 	\return			Returns the current time as a a 32 bit value formatted correctly for FAT filesystems, eg FATFS
 *
 *
 */
uint32_t get_fattime(void);



/** \brief		Convert a time read from the RTC into a directory name. DOESN'T READ THE RTC
 *
 *	\param		dirname			Pointer to directory name. Directory name MUST be at least 8 characters + terminating null
 *
 *	\param		time_store		Pointer to the 7 byte time data 
 *
 *				note that this function doesn't read the time, or modify time_store
 *				The first character of the directory name is unchanged
 * 				Creates a dirname xyymddhh  where x is left unchanged
 *				The month is converted to an ASCII character as 1 hex digit  (december=C for example)
 *				So for example a directory name xxxxxxxx plus a time such as 3rd Dec 2010 at 14:16:04 
 *					is converted to the characters x10C0314 
 */
void dirname_from_date(uint8_t *dirname,uint8_t *time_store);



/** \brief		Convert a time read from the RTC into a file name. DOESN'T READ THE RTC
 *
 *	\param		*filename		Pointer to file name. File name MUST be at least 12 characters + terminating null
 *
 *	\param		*time_store		Pointer to the 7 byte data to be converted
 *
 *				note that this function doesn't read the time, or modify time_store
 *				The first character of the filename is unchanged
 * 				Creates a filename xmddhhmm  where x is left unchanged
 *				The month is converted to an ASCII character as 1 hex digit  (december=C for example)
 *				So for example a file name xxxxxxxx.zyx plus a time such as 3rd Dec 2010 at 14:16:04 
 *					is converted to the characters xC031416.zyx
 */
void filename_from_date(uint8_t *filename,uint8_t *time_store);



/** \brief		Create an ISO time string from a set of data. DOESN'T READ THE RTC
 *
 *	\param		*iso_time_store	Pointer to 16 byte result
 *
 *	\param		*time_store		Pointer to the 7 byte data to be converted
 * 								Order of RTC values in time_store is ss,mm,hh,dow,dd,mm,yy		valid dow is 1-7
 *
 *				note that this function doesn't read the time, or modify time_store
 *				A time such as 3rd Dec 2010 at 14:16:04 is converted from 0x04,0x16,0x14,dow,0x03,0x12,0x10
 *				    to the string "20101203 141604" with a trailing zero
 */
void iso_time(uint8_t *iso_time_store,uint8_t *time_store);



/** \brief		Create a string representing the data in the form mm/dd/yyyy from a set of data. DOESN'T READ THE RTC
 *
 *	\param		*time_store		Pointer to the 7 byte data to be converted
 * 								Order of RTC values in time_store is ss,mm,hh,dow,dd,mm,yy		valid dow is 1-7
 *
 *	\param		*result			Pointer to 11 byte result
 *
 *				note that this function doesn't read the time, or modify time_store
 */
void americandate(uint8_t *result,uint8_t *time_store);



/** \brief		Create an ISO time string from a set of data read from the RTC
 *
 *	\param		*result			Pointer to 9 byte result which is null terminated and of form hh:mm:ss
 *
 *	\param		*time_store		Pointer to the 7 byte data to be converted (only first 3 bytes used) in order ss,mm,hh
 * 								Order of RTC values in time_store is ss,mm,hh,dow,dd,mm,yy		valid dow is 1-7
 *
 *				note that this function doesn't read the time, or modify time_store
 */
void hms_time(uint8_t *result,uint8_t *time_store);



/** \brief		Convert the high nibble of a supplied byte into an ASCII character
 *
 *	\param		hexval   Hex value
 *
 *	\return		ASCII character of top nibble
 */
uint8_t hex2ascii_h(uint8_t hexval);



/** \brief		Convert the low nibble of a supplied byte into an ASCII character
 *
 *	\param		hexval   Hex value
 *
 *	\return		ASCII character of bottom nibble
 */
uint8_t hex2ascii_l(uint8_t hexval);



/** \brief		Convert a BCD value in a byte to the correct hex value
 *
 *	\param		bcd			uint8_t value in BCD
 *
 *	\return					returns uint8_t hex value converted from BCD
 *
 *				For example, 23 -> 0x17
 *
 */
uint8_t bcd_to_hex(uint8_t bcd);



/** \brief		Take in a pointer to time in ISO format (15 bytes) eg 20101203 141604
 *					and set RTC to that time. 
 *				Time format is xxyymmddxhhmmss		where xx and x are don't care
 *				Time format does not need to be null terminated
 *				Day of week on RTC is set to 1, as I never use dow.
 *
 *	\param		iso_time_store   pointer to 15 bytes holding time as ISO value
 *
 *	\return		Error code from I2C transaction. 0 = success.
 */
uint8_t set_datetime(uint8_t *iso_time_store);



/** \brief		Take a BCD time eg as read from rtc, in form ss,mm,hh supplied as *start 
 * 					and add seconds seconds to it putting the result in *result OFFSET BY 1
 *				Note that it only works for hh:mm:ss, not days, and also that hh will wrap.
 *
 *  \param      result   pointer to location for resulting bcd 4 bytes:  dummy,ss,mm,hh
 *
 *  \param		start   Pointer to start time as bcd 3 bytes: ss,mm,hh
 *
 *  \param		seconds   number of seconds to add to start to create result
 */
void addtime(uint8_t *result, uint8_t *start, uint16_t seconds);


#endif

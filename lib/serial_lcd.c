/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) 2011, 2012 Brian Jones (University of Cambridge Computer Lab)
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <serial_lcd.h>

/* 23/2/2011  svn 282  */


/* Library function to allow attachment of an LCD using 2 wires

   Requires definitions included in serial_lcd.h (ususally by a #include of config.h) of:
   
   	LCD_PORT
	LCD_DATA
	LCD_CLK

  plus initialisation as outputs 
  
  
  plus the CPU frequency defined as eg define F_CPU 8E6
  plus the inclusion of util/delay_basic.h and util/delay.h
  so that  _delay_us is available
  
*/


void send_byte_to_lcd(uint8_t rs,uint8_t lcd_data) {	// rs=1 display data, rs=0 control

	uint8_t i;
	
	LCD_PORT &= ~(1<<LCD_CLK) ;							// enter with clock low
	LCD_PORT |= (1<<LCD_DATA) ;							// and data high
	_delay_us(1);

// send start pattern
	LCD_PORT |= (1<<LCD_CLK) ;							// clock high
	_delay_us(1);
	LCD_PORT &= ~(1<<LCD_DATA) ;						// data low
	_delay_us(3);										// This delay is important. The serial LCD triggers on falling edge of data. 
														// interrupt routine must see a high level on CLOCK

	if (rs == 1) {										// set control bit if necessary
		LCD_PORT |= (1<<LCD_DATA) ;						// data high
	}
	LCD_PORT &= ~(1<<LCD_CLK) ;							// clock low
	_delay_us(4);


	LCD_PORT |= (1<<LCD_CLK) ;							// clock rs bit
	_delay_us(1);
	LCD_PORT &= ~(1<<LCD_CLK) ;
	_delay_us(1);

// send data
	for (i=0;i<8;i++) {
		if ((lcd_data<<i) & 0x80) {
			LCD_PORT |= (1<<LCD_DATA) ;
		} else {
			LCD_PORT &= ~(1<<LCD_DATA) ;
		}
		_delay_us(3);									// data setup time
		LCD_PORT |= (1<<LCD_CLK) ;						// clock in state of DATA
		_delay_us(3);									// delay for interrupt routine
		LCD_PORT &= ~(1<<LCD_CLK) ;
		_delay_us(1);
	}
	LCD_PORT |= (1<<LCD_DATA) ;							// exit with data high (clock is low)

	_delay_us(30);										// inter byte delay
}



void home_lcd (void) {
	send_byte_to_lcd(0,0x02) ;	
}


void clear_lcd (void) {
	send_byte_to_lcd(0,0x01) ;
}


void scroll_lcd_left (void) {
	send_byte_to_lcd(0,0x18) ;
}


void scroll_lcd_right (void) {
	send_byte_to_lcd(0,0x1C) ;
}


void row2_lcd (void) {
	send_byte_to_lcd(0,0xC0) ;	
}


void setaddress_lcd(uint8_t row, uint8_t column) {
	uint8_t address = ((row & 0x01) * 0x40) + column + 0x80;
	send_byte_to_lcd(0,address) ;	
}



// Display an 8 bit char as 1 character on the display
void LCD_display_char(char character) {
	send_byte_to_lcd(1,character);
}




// Display a string 
void LCD_display_string(const char* string) {
	while (*string) {
		send_byte_to_lcd(1,*(string++));
	}
}



// Display an unsigned 8 bit value as up to three decimal characters
void LCD_display_uint8(uint8_t val) {
	unsigned char buf[3];
	int8_t ptr;
	for(ptr=0;ptr<3;++ptr) {
		buf[ptr] = (val % 10) + '0';
		val /= 10;
	}
	for(ptr=2;ptr>0;--ptr) {
		if (buf[ptr] != '0') break;
	}
	for(;ptr>=0;--ptr) {
		LCD_display_char(buf[ptr]);
	}
}



// Display a signed 8 bit value as a possible minus character followed by up to three decimal characters
void LCD_display_int8(int8_t val) {
	unsigned char buf[3];
	int8_t ptr;
	if (val < 0) {
	  LCD_display_char('-');
	  val *= -1;
	}
	for(ptr=0;ptr<3;++ptr) {
		buf[ptr] = (val % 10) + '0';
		val /= 10;
	}
	for(ptr=2;ptr>0;--ptr) {
		if (buf[ptr] != '0') break;
	}
	for(;ptr>=0;--ptr) {
		LCD_display_char(buf[ptr]);
	}
}



// Display a signed 16 bit value as a possible minus character followed by up to five decimal characters
void LCD_display_int16(int16_t val) {
	unsigned char buf[5];
	int8_t ptr;

	if (val < 0) {
	  LCD_display_char('-');
	  val *= -1;
	}
	for(ptr=0;ptr<5;++ptr) {
		buf[ptr] = (val % 10) + '0';
		val /= 10;
	}
	for(ptr=4;ptr>0;--ptr) {
		if (buf[ptr] != '0') break;
	}
	for(;ptr>=0;--ptr) {
		LCD_display_char(buf[ptr]);
	}
}



// Display an unsigned 16 bit value as up to five decimal characters
void LCD_display_uint16(uint16_t val) {
	unsigned char buf[5];
	int8_t ptr;
	for(ptr=0;ptr<5;++ptr) {
		buf[ptr] = (val % 10) + '0';
		val /= 10;
	}
	for(ptr=4;ptr>0;--ptr) {
		if (buf[ptr] != '0') break;
	}
	for(;ptr>=0;--ptr) {
		LCD_display_char(buf[ptr]);
	}
}



// Display an 8 bit unsigned value as 8 binary digits
void LCD_display_bin(uint8_t val) {
	uint8_t i;
	for(i=0;i<8;i++) {
		if ((val<<i) & 0x80) {
			LCD_display_char('1');
		} else {
			LCD_display_char('0');			
		}
	}
}



// Display the bottom 4 bits of an 8 bit value as one character 0-9,A-F
void LCD_display_nibble(uint8_t nibble) {
	nibble &= 0x0F ;
    if (nibble>9) {
    	nibble += 0x37 ;
    } else {
    	nibble += 0x30 ;
    }
    LCD_display_char(nibble);
}	


	
// Display an 8 bit unsigned value as two hex digits 0-9,A-F
void LCD_display_hex(uint8_t val) {
	LCD_display_nibble(val >>4) ;
	LCD_display_nibble(val) ;
}



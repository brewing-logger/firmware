/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _CONFIG_H
#define _CONFIG_H

/**
 * \file
 * \brief Software and hardware configuration.
 *
 * Various definitions which tie the software to the hardware configuration.
 */

/* Test modes. */
#define TEST_MODE 0
#define ASSERTS_ENABLED 0

/* Clock frequency and MCU identifier. */
#define F_CPU 1E6 /* 1MHz */
#define MMCU "atmega1284p"

/* ADC reference voltage. */
#define AVCC 3.3 /* Volts */

/* Storage options. */
#define FLASH_ROM_FLUSH_PERIOD 3600 /* seconds */

/* LCD connections. LCD clock and data are connected to PD6 and PD7, respectively. */
#define LCD_DDR DDRD
#define LCD_PORT PORTD
#define LCD_DATA PD6
#define LCD_CLK PD7

#define LCD_POWER_DDR DDRC
#define LCD_POWER_PORT PORTC
#define LCD_POWER_OUT PC6

/* LCD size. */
#define LCD_COLUMNS 24
#define LCD_COLUMN_BUFFERS 40
#define LCD_ROWS 2

/* SPI connections. */
#define SPI_DDR DDRB
#define SPI_PORT PORTB
#define DD_nSS PB4
#define DD_MOSI PB5
#define DD_SCK PB7
#define SPI_CS_FLASH PB3
#define SPI_CS_SD_CARD PB4

/* Temperature/Humidity sensor. */
#define HUMIDITY_SENSOR_DDR DDRD
#define HUMIDITY_SENSOR_PORT PORTD
#define HUMIDITY_SENSOR_PIN PIND
#define HUMIDITY_SENSOR_DATA PD3

/* Alcohol gas sensor. */
#define GAS_SENSOR_DDR DDRA
#define GAS_SENSOR_PORT PORTA
#define GAS_SENSOR_IN PA0

#define GAS_SENSOR_RL 220 /* kOhms */
#define GAS_SENSOR_RS_65_20_4 1000 /* kOhms; calibration resistance for 0.4mg/l alcohol in clean air at 65% RH, 20 degrees Celsius, 21% O_2 TODO: this is guessed */
#define GAS_SENSOR_RS_33_20_4 1000 /* kOhms; calibration resistance for 0.4mg/l alcohol in clean air at 33% RH, otherwise as above TODO: this is guessed */
#define GAS_SENSOR_VCC 3.3 /* Volts */

#define GAS_SENSOR_HEATER_DDR DDRC
#define GAS_SENSOR_HEATER_PORT PORTC
#define GAS_SENSOR_HEATER_OUT PC4

/* Proximity sensor. */
#define PROXIMITY_SENSOR_DDR DDRA
#define PROXIMITY_SENSOR_PORT PORTA
#define PROXIMITY_SENSOR_IN PA1

/* Thermometers. */
#define THERMOMETER0_DDR DDRA
#define THERMOMETER0_PORT PORTA
#define THERMOMETER0_IN PA2

#define THERMOMETER1_DDR DDRA
#define THERMOMETER1_PORT PORTA
#define THERMOMETER1_IN PA3

/* RTC. */
#define TW_DDR DDRC
#define TW_SDA PC1
#define TW_SCK PC0 /* otherwise known as SCL */
#define TW_PORT PORTC
#define TW_PIN PINC

/* UI button. */
#define BUTTON_DDR DDRD
#define BUTTON_PORT PORTD
#define BUTTON_IN PD2

/* Heater relay. */
#define HEATER_DDR DDRC
#define HEATER_PORT PORTC
#define HEATER_OUT PC5

#define HEATER_PROPORTIONAL_GAIN 1 /* TODO: tune as per http://www.embedded.com/design/prototyping-and-development/4211211/PID-without-a-PhD */
#define HEATER_INTEGRAL_INVERSE_GAIN 100
#define HEATER_INTEGRAL_MINIMUM INT32_MIN
#define HEATER_INTEGRAL_MAXIMUM INT32_MAX

/* Sensor power domain. */
#define SENSOR_POWER_DDR DDRC
#define SENSOR_POWER_PORT PORTC
#define SENSOR_POWER_OUT PC7

#endif /* !_CONFIG_H */

/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Brewing Logger.
 * Copyright (C) Philip Withnall 2012 <philip@tecnocode.co.uk>
 *
 * Brewing Logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Brewing Logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Brewing Logger.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _COMMON_H
#define _COMMON_H

#include "config.h"

#include <stdint.h>

/**
 * \file
 * \brief Common declarations.
 *
 * Declarations which are common to different components in the software.
 */

/**
 * \brief Measurement response.
 *
 * Whether taking a measurement using one of the sensor peripherals was successful. If the measurement was not successful, the measured value is
 * undefined.
 */
typedef enum {
	MEASUREMENT_SUCCESS = 0,		/**< Success. */
	MEASUREMENT_CHECKSUM_FAILURE = 1,	/**< Checksum failure on data received from the sensor. */
	MEASUREMENT_INVALID = 2,		/**< Data received from the sensor exceeded its expected bounds. */
	MEASUREMENT_TIMEOUT = 3,		/**< Did not receive a response from the sensor in time. */
} MeasurementResponse;

/**
 * \brief Reading from an ADC.
 *
 * A value read from the microcontroller's analogue to digital converter. Only the least significant 10 bits of this value are used; the most
 * significant 6 bits are undefined.
 */
typedef uint16_t AdcReading;

/**
 * \brief Time value from the RTC.
 *
 * A timestamp read from the real-time clock.
 *
 * \todo The semantics of these values are undefined.
 */
typedef uint32_t TimerReading;

/**
 * \brief Pseudo-random brew configuration ID.
 *
 * A pseudo-random value which uniquely identifies a brew configuration (BrewConfig). BREW_CONFIG_VERSION is reserved for the default brew
 * configuration, and must not be used in user-provided brew configurations.
 */
typedef uint8_t BrewConfigId;

/**
 * \brief Temperature measurement.
 *
 * A temperature measurement, in tenths of a degree Celsius (i.e. in a fixed point representation with a scaling factor of 10). Use the
 * TEMPERATURE_READING_TO_DEGREES_INT and TEMPERATURE_READING_TO_DEGREES_FRAC macros to convert a TemperatureReading to degrees Celsius.
 *
 * For example, a reading of 215 equates to 21.5 degrees Celsius.
 *
 * This data type can represent temperatures from 0 to 150.0 degrees Celsius, inclusive. Values outside this range (i.e. above 1500) are invalid.
 */
typedef uint16_t TemperatureReading;

/**
 * \brief Invalid temperature measurement.
 *
 * If the thermometer reports a measurement which is outside of the allowable range, the current log entry will use this as its value for the
 * temperature measurement. This is guaranteed to not be a valid measurable value.
 */
#define TEMPERATURE_READING_INVALID ((TemperatureReading) -1)

/**
 * \brief Convert a TemperatureReading to degrees Celsius.
 *
 * Macro to convert a TemperatureReading from its native format to (integeric) degrees Celsius. i.e. This yields the integeric part of the reading.
 * The result is truncated, rather than rounded. Use TEMPERATURE_READING_TO_DEGREES_FRAC to get the fractional part of the temperature.
 *
 * Converting TEMPERATURE_READING_INVALID will result in an undefined value.
 */
#define TEMPERATURE_READING_TO_DEGREES_INT(t) ((t) / 10)

/**
 * \brief Convert a TemperatureReading to fractional degrees Celsius.
 *
 * Macro to convert a TemperatureReading from its native format to (fractional) degrees Celsius. i.e. This yields the fractional part of the reading.
 *
 * Converting TEMPERATURE_READING_INVALID will result in an undefined value.
 */
#define TEMPERATURE_READING_TO_DEGREES_FRAC(t) ((t) % 10)

/**
 * \brief Convert a temperature to a TemperatureReading.
 *
 * Macro to convert a temperature in degrees Celsius, given as an integer and fractional part, to a TemperatureReading in its native format.
 * Fractional parts of 10 or over will have their modulus taken.
 *
 * For example: TEMPERATURE_READING_FROM_DEGREES(10, 5) gives a TemperatureReading representing a temperature of 10.5 degrees Celsius.
 */
#define TEMPERATURE_READING_FROM_DEGREES(int, frac) (((TemperatureReading) (int) * 10) + ((frac) % 10))

/**
 * \brief Maximum value for a TemperatureReading.
 *
 * The maximum valid value for a TemperatureReading. This corresponds to 150 degrees Celsius, which is above the operating range of both the MCP9700
 * and the RHT03.
 */
#define TEMPERATURE_READING_MAX 1500

/**
 * \brief Minimum value for a TemperatureReading.
 *
 * The minimum valid value for a TemperatureReading. This corresponds to 5 degrees Celsius, which is within the operating ranges of both the MCP9700
 * and the RHT03, but is very unlikely to be measured under normal operating conditions (e.g. with the fermenter being stored in a centrally heated
 * house). Consequently, we assume that a sensor reading this low indicates sensor failure.
 */
#define TEMPERATURE_READING_MIN 50

/**
 * \brief Brew name string length.
 *
 * The maximum length of the name in a brew configuration. Names may be shorter (by terminating them with a nul byte), but may not be longer.
 */
#define BREW_CONFIG_BREW_NAME_LENGTH 100 /* bytes */

/**
 * \brief Brew start date length.
 *
 * The fixed length of the field holding the brew configuration start date string in BrewConfig. This is exactly 17 bytes, holding an ISO 8601
 * date/time, e.g. “2012-12-06T15:46Z”.
 */
#define BREW_CONFIG_START_DATE_LENGTH 17 /* bytes */

/**
 * \brief Brew configuration file name.
 *
 * The file name used for brew configuration files on the SD card. This is relative to the root directory of the SD card file system.
 */
#define BREW_CONFIG_FILE_NAME "brewconf"

/**
 * \brief Brew results file name.
 *
 * The file name used for brew results files on the SD card. This is relative to the root directory of the SD card file system.
 */
#define BREW_RESULTS_FILE_NAME "brewresl"

/**
 * \brief Default brew configuration ID.
 *
 * If a brew configuration file isn't provided on the SD card, a default one will be generated and used by the logger. It will have this ID.
 * User-provided brew configuration files _must not_ use this ID themselves.
 */
#define DEFAULT_BREW_ID 0

/**
 * \brief Current brew configuration format version.
 *
 * The version number of the current brew configuration format.
 */
#define BREW_CONFIG_VERSION 1

/**
 * \brief Brew configuration.
 *
 * Configuration data for the brew which is currently being logged. This contains some identifying information about the brew, such as the brew's name
 * and start date, which can be displayed by the logger. It also contains some fermentation parameters, such as desired fermentation temperature,
 * which will be used by the logger to maintain the brew (e.g. by modulating a heater to keep the fermenter at the correct temperature).
 *
 * \note This structure must not be modified incompatibly without bumping the version (BREW_CONFIG_VERSION).
 */
typedef struct {
	BrewConfigId brew_id; /**< Pseudo-random ID for this brew, where DEFAULT_BREW_ID is reserved for the default brew config. */

	char start_date[BREW_CONFIG_START_DATE_LENGTH]; /**< Start date/time for the brew in ISO 8601 format, e.g. "2012-11-27T03:31Z". */
	char brew_name[BREW_CONFIG_BREW_NAME_LENGTH]; /**< Human-readable name for the brew, e.g. "Geordie Bitter". */
	TemperatureReading fermentation_temperature; /**< Desired fermenter temperature in tenths of a degree Celsius, e.g. 215 for 21.5 degrees. */
} BrewConfig;

/**
 * \brief Current brew configuration.
 * \private
 *
 * The brew configuration which was loaded at initialisation time, and is currently being used for all log entries.
 */
BrewConfig global_brew_config;

/*
 * Various assertion macros for debugging.
 */

/**
 * \def DYNAMIC_ASSERT(E)
 * \brief Dynamic assertion.
 *
 * A dynamic assertion, evaluated at run time (if at all). If the condition, \a E, is false, abort() is called and execution halts. Dynamic assertions
 * may be compiled out if ASSERTS_ENABLED is not defined.
 *
 * \param E Expression which must evaluate to true.
 */

/**
 * \def STATIC_ASSERT(E)
 * \brief Static assertion.
 *
 * A static assertion, evaluated at compile time if the compiler supports it (otherwise not evaluated at all). If the condition, \a E, is false,
 * compilation fails. The condition must be constant — it may not depend on run time data. Static assertions will never appear in compiled code, and
 * will always be checked at compile time unless the compiler doesn't support them.
 *
 * \param E Expression which must evaluate to true.
 */

#if defined(ASSERTS_ENABLED) && ASSERTS_ENABLED
#include <assert.h>
#include <stdlib.h>
#define DYNAMIC_ASSERT(E) assert (E)
#else /* if !ASSERTS_ENABLED */
#define DYNAMIC_ASSERT(E) while (0)
#endif /* ASSERTS_ENABLED */

/* See: http://stackoverflow.com/questions/3385515/static-assert-in-c */
#if defined(__GNUC__) && (__GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 6))
#define STATIC_ASSERT(E) _Static_assert (E, "Static assertion failed: " #E)
#else /* if !(__GNUC__ && ...) */
#define STATIC_ASSERT(E)
#warning Static assertions disabled. Build with GCC >= 4.6.0 to enable them.
#endif /* !(__GNUC__ && ...) */

/**
 * \brief Dynamic reachability assertion.
 *
 * A dynamic assertion which calls abort() if it's ever reached at run time. This should typically be used for the default case in switch statements.
 */
#define ASSERT_NOT_REACHED() DYNAMIC_ASSERT(0)

/**
 * \brief Nullability function attribute.
 *
 * A function attribute to tell the compiler that the list of pointer parameters, \a ..., must all be non-null when this function is called. The list
 * of parameters is a list of 1-based parameter indices (e.g. “1, 3” refers to the first and third parameters of the function; ‘0’ is not a valid
 * parameter index). All call sites to the function will be checked at compile time to ensure null is not being passed inappropriately.
 *
 * \param ... List of 1-based parameter indices for parameters which must be non-null.
 */
#define NONNULL(...) __attribute__((nonnull (__VA_ARGS__)))

#endif /* !_COMMON_H */
